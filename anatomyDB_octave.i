// Copyright (C) 2017 INRIA
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 2.1 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details. You should have received a copy of the GNU Lesser
// General Public License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Thomas Lemaire (INRIA)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
%module anatomyDB

//%begin %{
//#ifdef _MSC_VER
//#define SWIG_PYTHON_INTERPRETER_NO_DEBUG
//#define _ALLOW_ITERATOR_DEBUG_LEVEL_MISMATCH
//#define _CRT_SECURE_INVALID_PARAMETER(expr)
//#endif
//%}

%include "std_string.i"
%include "std_vector.i"
%include "std_list.i"

%{
#include <algorithm>

#include <octave/oct-obj.h>
#include <octave/dColVector.h>

#include "anatomyDB/types.h"
#include "anatomyDB/query.h"
#include "anatomyDB/FrameFactory.h"
%}

%typemap(in) anatomydb::Vector3, anatomydb::Vector3 const& (anatomydb::Vector3 vec)
{
    if (!$input.is_matrix_type()) {
        octave_stdout << "Vector of size 3 is expected\n";
        SWIG_fail;
    }

    const Matrix matrix=$input.matrix_value();

    if (!matrix.dims().is_vector() || !(matrix.dims().numel()==3)) {
        octave_stdout << "Vector of size 3 is expected\n";
        SWIG_fail;
    }

    for (std::size_t i=0; i<3; ++i)
        vec[i] = matrix.elem(i);

    $1 = &vec;
}

%typemap(out) anatomydb::Vector3, anatomydb::Vector3 const&
{
    ColumnVector vec(3);
    for (std::size_t i=0; i<3; ++i)
        vec.elem(i) = (*$1)[i];
    $result = vec;
}

// TODO: write a generic typemap for eigen
%typemap(out) anatomydb::Frame, anatomydb::Frame const&
{
    ColumnVector vec(7);
    for (std::size_t i=0; i<3; ++i)
        vec.elem(i) = $1.translation()[i];
    anatomydb::Quaternion q($1.rotation());
    for (std::size_t i=0; i<4; ++i)
        vec.elem(i+3) = q.coeffs()[i];
    $result = vec;
}

%exception {
  try {
    $action
  } catch (const std::runtime_error& e) {
    SWIG_exception(SWIG_RuntimeError,const_cast<char*>(e.what()));
  }
}

namespace std {
%template(ListStr) std::list<std::string>;
%template(VecStr) std::vector<std::string>;
}

namespace anatomydb {

void init(std::string const& databaseFile="");

std::string getReferenceName(std::string const& name);
std::string getReferenceNameNoThrow(std::string const& name);
std::string getEntityDescription(std::string const& name);

bool exists(std::string const& name);
bool isBone(std::string const& name);
bool isLandmark(std::string const& name);
bool isEntitySubClassOf(std::string const& name, std::string const& className);

std::vector<std::string> getSubClassOfList(std::string const& className);
std::vector<std::string> getParentClassList(std::string const& name);
std::vector<std::string> getPartOfList(std::string const& name, std::string const& className);
std::vector<std::string> getSubPartOfList(std::string const& name, std::string const& className);
std::vector<std::string> getSynonymList(std::string const& name);
std::string getSynonymFromBibliography(std::string const& name, std::string const& fromBibliography);
std::vector<std::string> getEntityBibliographyList(std::string const& name);
std::vector<std::string> getLandmarkBoneList(std::string const& name);
std::vector<std::string> getSubClassOfFromBibliographyList(std::string const& className, std::string const& bibliography);

%nodefaultctor LandmarkCont;
%nodefaultdtor LandmarkCont;
class LandmarkCont {
public:
    void clear();
    void add(std::string const& name, anatomydb::Vector3 const& coord);
    anatomydb::Vector3 const& get(std::string const& name) const;
};

%nodefaultctor FrameFactory;
%nodefaultdtor FrameFactory;
class FrameFactory {
public:
    static FrameFactory &instance();

    LandmarkCont const& landmark() const;
    LandmarkCont & landmark();

    bool isFrameRegistered(std::string const& name);
    std::vector<std::string> registeredFrameList() const;
};
%extend FrameFactory {
    anatomydb::Frame computeFrame(std::string const& name)
    {
        anatomydb::Frame frame;
        self->computeFrame(name, frame);
        return frame;
    }
}

} // anatomydb


%inline %{
std::vector<std::string> getAnatomicalEntityList() {
    std::vector<std::string> entityList;
    anatomydb::getAnatomicalEntityList(entityList);
    return entityList;
}
std::vector<std::string> getLandmarkList() {
    std::vector<std::string> landmarkList;
    anatomydb::getLandmarkList(landmarkList);
    return landmarkList;
}
std::vector<std::string> getJointList() {
    std::vector<std::string> jointList;
    anatomydb::getJointList(jointList);
    return jointList;
}
%}

