/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_ANATOMYDB_CHECK_H
#define PIPER_ANATOMYDB_CHECK_H

#ifdef WIN32
#	ifdef anatomyDB_EXPORTS
#		define ANATOMYDB_EXPORT __declspec( dllexport )
#	else
#		define ANATOMYDB_EXPORT __declspec( dllimport )
#	endif
#else
#	define ANATOMYDB_EXPORT
#endif

namespace anatomydb {

/// TODO check that all entities in the synonym column are subclass of Synonym
ANATOMYDB_EXPORT bool checkSynonym();

/** This function check for synonym/entity name duplication.
 * There is no database lowlevel constraint mechanism to enforce the unicity of {enttity.name, synonym.synonym}
 * \todo Would it be possible to enforce such constraint using the CHECK clause in the synonym CREATE TABLE ?
 */
ANATOMYDB_EXPORT bool checkSynonymUnicity();

/// TODO
//ANATOMYDB_EXPORT bool checkFrameExistance();

/// TODO check that all entities in the fromBibliography column are subclass of Bibliographiy
ANATOMYDB_EXPORT bool checkBibliography();

/// TODO check bibliography refers to a single entity (the reference entity or its synonym)
//ANATOMYDB_EXPORT bool checkBibliographyUnicity();

///TODO check the partOf relation describes a tree for each class (except joint)
ANATOMYDB_EXPORT bool checkTreePartOf();

} // anatomydb

#endif // PIPER_ANATOMYDB_CHECK_H
