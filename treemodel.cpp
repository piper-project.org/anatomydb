/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

/*
    treemodel.cpp

    Provides a simple tree model to show how to create and use hierarchical
    models.
*/

#include "treeitem.h"
#include "treemodel.h"
#include <stdlib.h>
#include <iostream>

#include "query.h"

/**
 * @brief TreeModel::TreeModel the constructor to use
 * @param relations
 * @param data
 * @param rootData
 * @param parent
 */
TreeModel::TreeModel(std::map<QVariant, QList<QVariant>> &relations, std::map<QVariant, QList<QVariant>> &data, QList<QVariant> roles, QObject * parent)
    : QAbstractItemModel(parent)
{
//    std::cerr << "The model has " << roles.size() << " roles : ";
    for( int i = 0 ; i < roles.size() ; i++){
        m_roleNameMapping[i] = roles.at(i).toByteArray();
//        std::cerr << roles.at(i).toString().toStdString() << ", ";
    }
    if(roles.empty())
        roles << "";

    rootItem = new TreeItem(roles);
    rootItem = buildData(relations,data,roles.at(0),0);
}

TreeModel::~TreeModel()
{
    delete rootItem;
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
//    int returnVal;
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    //Get the item corresponding to the index
    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

    if(m_roleNameMapping.contains(role) && (role >= 0) && (role < item->columnCount()))
        return item->data(role);

    return QVariant();
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        return rootItem->data(section);
    }

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem *parentItem;

        //If the parent is not valid, the default parent is the root of the tree
    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());  //data pointer from the model index, refers a TreeItem object

    TreeItem *childItem = parentItem->child(row);       //Build the child item corresponding to the row
    if (childItem)
        return createIndex(row, column, childItem);     //Build the index and return it, with all the necessary data
    else
        return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parentItem();

    if (parentItem == rootItem)
        return QModelIndex();

//    return createIndex(parentItem->row(), parentItem->columnCount(), parentItem); // former error
    return createIndex(parentItem->row(), 0, parentItem);
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

//    if(parentItem != rootItem){
//        std::cerr << "The item "<< parentItem->data(0).toString().toStdString()  << " has " << parentItem->childCount() << " children. \n";
//    }

    return parentItem->childCount();
}

QHash<int, QByteArray> TreeModel::roleNames() const
{
    return m_roleNameMapping;
}

QModelIndex TreeModel::child(const QModelIndex &indexParent, int row) const
{
    if (!indexParent.isValid()){
//        std::cerr << "parent index not valid" ;
        return QModelIndex();
    }

    TreeItem *parentItem = static_cast<TreeItem*>(indexParent.internalPointer());

    if(parentItem->childCount()<1){
//        std::cerr << "parent item has no child" ;
        return QModelIndex();
    }

    TreeItem *childItem = parentItem->child(row); //childItem->parentItem();
//    std::cerr << "parent item : " << parentItem->data(0).toString().toStdString() <<
//                 " has child no " << row << " equal to " << childItem->data(0).toString().toStdString() << "\n";

//    if (parentItem == rootItem)

//    return createIndex(parentItem->row(), parentItem->columnCount(), parentItem); // former error
    return createIndex(childItem->row(), 0, childItem);
}

int TreeModel::rowInParent(const QModelIndex &index) const
{
    if(!index.isValid())
        return -1;

//    std::cerr << "index is valid\n";

    QModelIndex parent = this->parent(index);
    if(!parent.isValid())   //This is a root item
        return index.row();
//    std::cerr << "parent is valid\n";

    TreeItem * parentItem = static_cast<TreeItem*>(parent.internalPointer());

    int nbChild = parentItem->childCount();
//    std::cerr << "parent has " << nbChild <<" children\n";
    while(nbChild > 0){
        nbChild --;
        if(parent.child(nbChild,0) == index){
//            std::cerr << "the child no " << nbChild << "is ok !";
            return nbChild;
        }
//        std::cerr << "the child no " << nbChild << "is not ok !";
    }
    return -1;
}

QList<QVariant> TreeModel::getChildrenIndexes(const QModelIndex &parent, bool recursive) const
{
    TreeItem* current;
    QList<QVariant> indexes;

    if(!parent.isValid())
        current = this->rootItem;
    else
        current = static_cast<TreeItem*>(parent.internalPointer());

    int count = current->childCount();
//    std::cerr << "get index of " << current->data(0).toString().toStdString() << " has "<< count << " kids \n";

    for(int i = 0; i < count; ++i){
//        std::cerr << "add child " << i << std::endl;
        indexes.append(index(i,0,parent));
        if (recursive)
            indexes.append(getChildrenIndexes(index(i,0,parent),true));
    }
    return indexes;
}

QList<QVariant> TreeModel::getLeafIndexes(const QModelIndex &parent) const
{
    TreeItem* current;
    QList<QVariant> indexes;

    if (!parent.isValid())
        current = this->rootItem;
    else
        current = static_cast<TreeItem*>(parent.internalPointer());

    int count = current->childCount();
    for (int i = 0; i < count; ++i){
        QModelIndex childIndex = index(i, 0, parent);
        if (static_cast<TreeItem*>(childIndex.internalPointer())->childCount() == 0) // if it has no children, append it to the list
            indexes.append(childIndex);
        else // ...otherwise get its leafs
            indexes.append(getLeafIndexes(childIndex));
    }
    return indexes;
}

void TreeModel::_findIndex(const QModelIndex &parentItemIndex, TreeItem* parentItem, QVariant data, bool doCompareRefName, int role, QList<QVariant>& indexes)
{
    int count = parentItem->childCount();
    for (int i = 0; i < count; ++i) {
        QModelIndex childIndex = index(i, 0, parentItemIndex);
        TreeItem* childItem = parentItem->child(i);
        QVariant treeData = childItem->data(role);
        if (doCompareRefName) {
            std::string treeName = treeData.toString().toStdString();
            if (anatomydb::exists(treeName))
                treeData = QString::fromStdString(anatomydb::getReferenceName(treeName));
        }
        if (treeData == data)
            indexes.append(childIndex);
        _findIndex(childIndex, childItem, data, doCompareRefName, role, indexes);
    }
}

QList<QVariant> TreeModel::findIndex(QVariant data, bool doCompareRefName, int role, const QModelIndex &parent)
{
    QList<QVariant> indexes;
    TreeItem* rootItem;
    if (!parent.isValid())
        rootItem = this->rootItem;
    else
        rootItem = static_cast<TreeItem*>(parent.internalPointer());

    QVariant userData = data;
    if (doCompareRefName) {
        std::string treeName = data.toString().toStdString();
        if (anatomydb::exists(treeName))
            userData = QString::fromStdString(anatomydb::getReferenceName(treeName));
    }
    _findIndex(parent, rootItem, userData, doCompareRefName, role, indexes);
    return indexes;
}

QList<QVariant> TreeModel::getAllIndexes() const
{
    return getChildrenIndexes();
}

TreeItem* TreeModel::buildData(std::map<QVariant, QList<QVariant>> &relations, std::map<QVariant, QList<QVariant>> &data, QVariant current, TreeItem* parent){
    TreeItem* item;
    if(current==rootItem->data(0)){ // parent == 0){ //Current is rootItem
        item = rootItem;
//        std::cerr << "** " << item->data(0).toString().toStdString() << " is the root item\n";
    }else{
        QList<QVariant> item_data = {};

        if(data.find(current) != data.end())
            item_data = data.at(current);

        item_data.insert(0,current);
        item = new TreeItem(item_data,parent);
    }
//    std::cerr << "** " << current.toString().toStdString();
//    std::cerr << "** " << item->data(0).toString().toStdString();

    if(relations.find(current) != relations.end()){

        QList<QVariant> children = relations.at(current);
//        std::cerr << " is parent of " << children.size() <<" children : \n";
        for (int i = 0; i < children.size(); i++){
            TreeItem* child = buildData(relations,data,children.at(i),item);
            item->appendChild(child);
//            std::cerr << " ";
        }
    }

//    std::cerr << "****** End "<< current.toString().toStdString() <<" ****** \n";
    return item;
}
