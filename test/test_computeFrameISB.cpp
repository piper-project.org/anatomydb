/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "gtest/gtest.h"

#include "anatomyDB/FrameFactory.h"
#include "anatomyDB/query.h"

using namespace anatomydb;

TEST(computeFrameISB_test, init)
{
    FrameFactory & ff = FrameFactory::instance();
    EXPECT_TRUE(ff.isFrameRegistered("Pelvis_right_hip_frame"));
    EXPECT_TRUE(ff.isFrameRegistered("Pelvis_left_hip_frame"));
    EXPECT_TRUE(ff.isFrameRegistered("C3_frame"));
    EXPECT_EQ(3, ff.countFrameRegistered("C3_frame"));
    EXPECT_TRUE(ff.isFrameRegistered("T10_frame"));
    EXPECT_EQ(3, ff.countFrameRegistered("T10_frame"));
}

TEST(computeFrameISB_test, frameNames)
{
    for (std::string const& name : FrameFactory::instance().registeredFrameList()) {
        EXPECT_TRUE(anatomydb::exists(name)) << "frame: " << name;
        EXPECT_TRUE(anatomydb::isEntitySubClassOf(name, "Frame")) << "frame: " << name;
    }
}
