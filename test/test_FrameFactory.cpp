/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "gtest/gtest.h"

#include "anatomyDB/FrameFactory.h"

using namespace anatomydb;

void Center_of_AB(Frame& frame, LandmarkCont const& landmark)
{
    frame.translation() = (landmark.get("A")+landmark.get("B"))/2;
}

void Center_of_AB2(Frame& frame, LandmarkCont const& landmark)
{
    frame.translation() << 0,1,2;
}

TEST(FrameFactory_test, init)
{
    FrameFactory & ff = FrameFactory::instance();
    ff.registerFrame("Center_of_AB", Center_of_AB);
    EXPECT_EQ(1, ff.countFrameRegistered("Center_of_AB"));
    ff.landmark().add("A", Vector3(1,0,0));
    ff.landmark().add("B", Vector3(0,1,0));
    ASSERT_TRUE(ff.isFrameRegistered("Center_of_AB"));
    EXPECT_GT(ff.registeredFrameList().size(), 0);
}

TEST(FrameFactory_test, landmark)
{
    FrameFactory & ff = FrameFactory::instance();
    EXPECT_NO_THROW(ff.landmark().get("A"));
    EXPECT_THROW(ff.landmark().get("C"), std::runtime_error);
}

TEST(FrameFactory_test, frame)
{
    FrameFactory & ff = FrameFactory::instance();
    Frame I;
    EXPECT_NO_THROW(ff.computeFrame("Center_of_AB", I));
    EXPECT_TRUE(I.translation().isApprox(Vector3(0.5,0.5,0)));
    EXPECT_TRUE(I.rotation().isIdentity());
    EXPECT_THROW(ff.computeFrame("Center_of_CD", I), std::runtime_error);
}

TEST(FrameFactory_test, multiframe)
{
    FrameFactory & ff = FrameFactory::instance();
    std::size_t size = ff.registeredFrameList().size();
    EXPECT_NO_THROW(ff.registerFrame("Center_of_AB", Center_of_AB2));
    EXPECT_EQ(size, ff.registeredFrameList().size());
    EXPECT_EQ(2, ff.countFrameRegistered("Center_of_AB"));
}

TEST(FrameFactory_test, unregister)
{
    FrameFactory & ff = FrameFactory::instance();
    ff.unregisterFrame("Center_of_AB");
    EXPECT_FALSE(ff.isFrameRegistered("Center_of_AB"));
}
