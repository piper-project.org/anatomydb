/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <algorithm>
#include "gtest/gtest.h"

#include "anatomyDB/query.h"
#include "anatomyDB/check.h"

TEST(AnatomyDB_test, init) {
    anatomydb::init("./");
}

TEST(AnatomyDB_test, synonym) {
    EXPECT_TRUE( anatomydb::checkSynonym() );
}

TEST(AnatomyDB_test, synonymUnicity) {
    EXPECT_TRUE( anatomydb::checkSynonymUnicity() );
}

TEST(AnatomyDB_test, bibliography) {
    EXPECT_TRUE( anatomydb::checkBibliography() );
}

TEST(AnatomyDB_test, TreePartOf) {
    EXPECT_TRUE( anatomydb::checkTreePartOf() );
}

TEST(AnatomyDB_test, isSynonymOf) {
    EXPECT_TRUE( anatomydb::isSynonymOf("Fifth_cervical_vertebra", "C5") );
    EXPECT_FALSE( anatomydb::isSynonymOf("Fifth_cervical_vertebra", "T5") );
}

TEST(AnatomyDB_test, getEntityId) {
    EXPECT_NO_THROW( anatomydb::getEntityId("Skull") );
    EXPECT_NO_THROW( anatomydb::getEntityId("C5") );
    EXPECT_NO_THROW( anatomydb::getEntityId("C5_frame") );
    EXPECT_NO_THROW( anatomydb::getEntityId("T1") );
    EXPECT_NO_THROW( anatomydb::getEntityId("Bone") );
    EXPECT_NO_THROW( anatomydb::getEntityId("Left_femur") );
    EXPECT_THROW( anatomydb::getEntityId("blablabla"), std::runtime_error );
}

TEST(AnatomyDB_test, exists) {
    EXPECT_TRUE( anatomydb::exists("Bone") );
    EXPECT_TRUE( anatomydb::exists("Synonym") );
    EXPECT_TRUE( anatomydb::exists("Left_femur") );
    EXPECT_TRUE( anatomydb::exists("C5") );
    EXPECT_TRUE( anatomydb::exists("Left_anterior_cruciate_ligament") );
    EXPECT_FALSE( anatomydb::exists("blablabla") );
}

TEST(AnatomyDB_test, getReferenceName) {
    EXPECT_EQ("Left_femur", anatomydb::getReferenceName("Left_femur"));
    EXPECT_EQ("First_thoracic_vertebra", anatomydb::getReferenceName("T1"));
}

TEST(AnatomyDB_test, getEntityDescription) {
    EXPECT_GT(anatomydb::getEntityDescription("Bone").size(), 0);
}

TEST(AnatomyDB_test, isEntitySubClassOf) {
    EXPECT_TRUE( anatomydb::isEntitySubClassOf("C5","Synonym") );
    EXPECT_TRUE( anatomydb::isEntitySubClassOf("Skin","Anatomical_entity") );
    EXPECT_TRUE( anatomydb::isEntitySubClassOf("Left_femur","Anatomical_entity") );
    EXPECT_TRUE( anatomydb::isEntitySubClassOf("C5","Anatomical_entity") );
    EXPECT_FALSE( anatomydb::isEntitySubClassOf("Right_anterior_superior_iliac_spine","Anatomical_entity") );

    EXPECT_TRUE( anatomydb::isEntitySubClassOf("Left_femur","Bone") );
    EXPECT_TRUE( anatomydb::isEntitySubClassOf("Skull","Bone") );
    EXPECT_TRUE( anatomydb::isEntitySubClassOf("Skeleton_of_rib_cage","Bone") );
    EXPECT_TRUE( anatomydb::isEntitySubClassOf("Left_third_rib","Bone") );
    EXPECT_TRUE( anatomydb::isEntitySubClassOf("Fifth_cervical_vertebra","Bone") );
    EXPECT_TRUE( anatomydb::isEntitySubClassOf("C5","Bone") );
    EXPECT_FALSE( anatomydb::isEntitySubClassOf("Skin","Bone") );

    EXPECT_TRUE( anatomydb::isEntitySubClassOf("Articular_capsule_of_right_knee_joint","Articular_capsule") );
    EXPECT_TRUE( anatomydb::isEntitySubClassOf("Left_anterior_cruciate_ligament","Ligament") );

    EXPECT_TRUE( anatomydb::isEntitySubClassOf("Skin_of_neck","Skin") );

    EXPECT_TRUE( anatomydb::isEntitySubClassOf("Right_anterior_superior_iliac_spine","Landmark") );
    EXPECT_FALSE( anatomydb::isEntitySubClassOf("Left_femur","Landmark") );

}

TEST(AnatomyDB_test, isSynonym) {
    EXPECT_TRUE( anatomydb::isSynonym("C5") );
    EXPECT_FALSE( anatomydb::isSynonym("Left_femur") );
}

TEST(AnatomyDB_test, isAnatomicalEntity) {
    EXPECT_TRUE( anatomydb::isAnatomicalEntity("Skin") );
    EXPECT_TRUE( anatomydb::isAnatomicalEntity("Skin_of_head") );
    EXPECT_TRUE( anatomydb::isAnatomicalEntity("Left_femur") );
    EXPECT_TRUE( anatomydb::isAnatomicalEntity("C5") );
    EXPECT_FALSE( anatomydb::isAnatomicalEntity("Right_anterior_superior_iliac_spine") );
}

TEST(AnatomyDB_test, isBone) {
    EXPECT_TRUE( anatomydb::isBone("Left_femur") );
    EXPECT_TRUE( anatomydb::isBone("Skull") );
    EXPECT_TRUE( anatomydb::isBone("Skeleton_of_rib_cage") );
    EXPECT_TRUE( anatomydb::isBone("Left_third_rib") );
    EXPECT_TRUE( anatomydb::isBone("Fifth_cervical_vertebra") );
    EXPECT_TRUE( anatomydb::isBone("C5") );
    EXPECT_FALSE( anatomydb::isBone("Skin") );
}

TEST(AnatomyDB_test, isSkin) {
    EXPECT_TRUE( anatomydb::isSkin("Skin_of_right_free_lower_limb") );
}

TEST(AnatomyDB_test, isLandmark) {
    EXPECT_FALSE( anatomydb::isLandmark("Left_femur") );
    EXPECT_TRUE( anatomydb::isLandmark("Right_anterior_superior_iliac_spine") );
}

TEST(AnatomyDB_test, getSubClassOfList) {
    std::vector<std::string> landmarkList = anatomydb::getSubClassOfList("Landmark");
    EXPECT_TRUE( std::find(landmarkList.begin(), landmarkList.end(), "Right_anterior_superior_iliac_spine")!=landmarkList.end() );
    EXPECT_TRUE( std::find(landmarkList.begin(), landmarkList.end(), "Left_posterior_superior_iliac_spine")!=landmarkList.end() );
}

TEST(AnatomyDB_test, getSubClassOfFromBibliographyList) {
    std::vector<std::string> entityList;

    entityList = anatomydb::getSubClassOfFromBibliographyList("Frame","ISB_JCS");
    EXPECT_TRUE( std::find(entityList.begin(), entityList.end(), "Pelvis_right_hip_frame")!=entityList.end() );
    EXPECT_TRUE( std::find(entityList.begin(), entityList.end(), "Right_femur_hip_frame")!=entityList.end() );

    entityList = anatomydb::getSubClassOfFromBibliographyList("Frame","ISB_BCS");
    EXPECT_TRUE( std::find(entityList.begin(), entityList.end(), "T6_frame")!=entityList.end() );

    entityList = anatomydb::getSubClassOfFromBibliographyList("Bone","FMA");
    EXPECT_TRUE( std::find(entityList.begin(), entityList.end(), "Left_femur")!=entityList.end() );
    EXPECT_TRUE( std::find(entityList.begin(), entityList.end(), "Pelvic_skeleton")!=entityList.end() );

    entityList = anatomydb::getSubClassOfFromBibliographyList("Bone","Kepple1997");
    EXPECT_TRUE(entityList.empty());

    entityList = anatomydb::getSubClassOfFromBibliographyList("Landmark","Kepple1997");
    EXPECT_TRUE( std::find(entityList.begin(), entityList.end(), "RASIS")!=entityList.end() );
    EXPECT_TRUE( std::find(entityList.begin(), entityList.end(), "RAIIS")!=entityList.end() );
    EXPECT_TRUE( std::find(entityList.begin(), entityList.end(), "RMEDEPIC")!=entityList.end() );
}

TEST(AnatomyDB_test, isEntityPartOf) {
    EXPECT_TRUE(anatomydb::isEntityPartOf("Left_hip_bone","Pelvic_skeleton"));
    EXPECT_TRUE(anatomydb::isEntityPartOf("C4","Spine"));
    EXPECT_TRUE(anatomydb::isEntityPartOf("Axis","Spine"));
    EXPECT_TRUE(anatomydb::isEntityPartOf("Fifth_cervical_vertebra","Spine"));
    EXPECT_FALSE(anatomydb::isEntityPartOf("C4","Pelvic_skeleton"));
    EXPECT_TRUE(anatomydb::isEntityPartOf("C4_frame", "C4"));
    EXPECT_FALSE(anatomydb::isEntityPartOf("C4_frame", "Spine"));
    EXPECT_TRUE(anatomydb::isEntityPartOf("C4_frame", "Spine", true));
}

TEST(AnatomyDB_test, getParentClassList) {
    std::vector<std::string> parentClassList = anatomydb::getParentClassList("Left_hip_bone");
    EXPECT_TRUE( std::find(parentClassList.begin(), parentClassList.end(), "Bone")!=parentClassList.end() );
    EXPECT_TRUE( std::find(parentClassList.begin(), parentClassList.end(), "Anatomical_entity")!=parentClassList.end() );
}

TEST(AnatomyDB_test, getPartOfList) {
    std::vector<std::string> partOfList;
    // result of any class
    partOfList = anatomydb::getPartOfList("Atlas");
    EXPECT_EQ(4, partOfList.size());
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Neck")!=partOfList.end() ); // region
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Vertebral_column")!=partOfList.end() ); // bone
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Atlanto-axial_joint")!=partOfList.end() ); // joint
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Atlanto-occipital_joint")!=partOfList.end() ); // joint
}

TEST(AnatomyDB_test, getPartOfSubClassList) {
    std::vector<std::string> partOfList;
    // test with bone
    partOfList = anatomydb::getPartOfSubClassList("Right_hip_bone", "Bone");
    EXPECT_EQ(1, partOfList.size());
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Pelvic_skeleton")!=partOfList.end() );
    // test with frame/bone
    partOfList = anatomydb::getPartOfSubClassList("Pelvis_left_hip_frame", "Bone", true);
    EXPECT_EQ(2, partOfList.size());
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Left_hip_bone")!=partOfList.end() );
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Pelvic_skeleton")!=partOfList.end() );
    partOfList = anatomydb::getPartOfSubClassList("Left_femur_knee_frame", "Bone");
    EXPECT_EQ(1, partOfList.size());
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Left_femur")!=partOfList.end() );
    partOfList = anatomydb::getPartOfSubClassList("Left_tibia_knee_frame", "Bone");
    EXPECT_EQ(1, partOfList.size());
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Left_tibia")!=partOfList.end() );
    partOfList = anatomydb::getPartOfSubClassList("C4_frame", "Bone");
    EXPECT_EQ(1, partOfList.size());
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Fourth_cervical_vertebra")!=partOfList.end() );
    //test with region
    partOfList = anatomydb::getPartOfSubClassList("Right_talus", "Region");
    ASSERT_EQ(1, partOfList.size());
    EXPECT_EQ("Right_foot", partOfList[0] );
}



TEST(AnatomyDB_test, getPartOfSubClassListRecursive) {
    std::vector<std::string> partOfList;
    partOfList = anatomydb::getPartOfSubClassList("Left_humerus", "Bone", true);
    EXPECT_EQ(0, partOfList.size());
    partOfList = anatomydb::getPartOfSubClassList("Left_humerus", "Region", true);
    EXPECT_EQ(4, partOfList.size());
    EXPECT_EQ("Left_arm", partOfList[0]); // closest region
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Left_free_upper_limb")!=partOfList.end() );
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Left_upper_limb")!=partOfList.end() );
    EXPECT_TRUE( std::find(partOfList.begin(), partOfList.end(), "Body")!=partOfList.end() );
    std::vector<std::string> refPartOfList = anatomydb::getPartOfSubClassList("Pelvic_skeleton", "Region", true);
    partOfList = anatomydb::getPartOfSubClassList("Left_hip_bone", "Region", true);
    EXPECT_TRUE(refPartOfList == partOfList);
    partOfList = anatomydb::getPartOfSubClassList("Left_lateral_collateral_ligament", "Region", true);
    ASSERT_GE(partOfList.size(), 1);
    EXPECT_EQ("Left_free_lower_limb", partOfList[0] );
}

TEST(AnatomyDB_test, getSubPartOfList) {
    std::vector<std::string> subPartOfList = anatomydb::getSubPartOfList("Pelvic_skeleton", "Bone");
    EXPECT_EQ(4, subPartOfList.size());
    EXPECT_TRUE( std::find(subPartOfList.begin(), subPartOfList.end(), "Right_hip_bone")!=subPartOfList.end() );
    EXPECT_TRUE( std::find(subPartOfList.begin(), subPartOfList.end(), "Left_hip_bone")!=subPartOfList.end() );
    EXPECT_TRUE( std::find(subPartOfList.begin(), subPartOfList.end(), "Sacrum")!=subPartOfList.end() );
    EXPECT_TRUE( std::find(subPartOfList.begin(), subPartOfList.end(), "Coccyx")!=subPartOfList.end() );
    std::vector<std::string> boneList = anatomydb::getSubPartOfList("Left_hip_joint", "Bone");
    EXPECT_EQ(2, boneList.size());
    EXPECT_TRUE( std::find(boneList.begin(), boneList.end(), "Left_hip_bone")!=boneList.end() );
    EXPECT_TRUE( std::find(boneList.begin(), boneList.end(), "Left_femur")!=boneList.end() );
    std::vector<std::string> frameList = anatomydb::getSubPartOfList("Left_hip_joint","Frame");
    EXPECT_EQ(2, frameList.size());
    EXPECT_TRUE( std::find(frameList.begin(), frameList.end(), "Pelvis_left_hip_frame")!=frameList.end() );
    EXPECT_TRUE( std::find(frameList.begin(), frameList.end(), "Left_femur_hip_frame")!=frameList.end() );
    std::vector<std::string> regionList = anatomydb::getSubPartOfList("Left_lower_limb","Region");
    EXPECT_EQ(2, regionList.size());
    EXPECT_TRUE( std::find(regionList.begin(), regionList.end(), "Left_free_lower_limb")!=regionList.end() );
    EXPECT_TRUE( std::find(regionList.begin(), regionList.end(), "Left_pelvic_girdle")!=regionList.end() );
    EXPECT_FALSE( std::find(regionList.begin(), regionList.end(), "Left_foot")!=regionList.end() );
    std::vector<std::string> skinList = anatomydb::getSubPartOfList("Right_free_lower_limb", "Skin");
    EXPECT_EQ(1, skinList.size());
    EXPECT_TRUE( std::find(skinList.begin(), skinList.end(), "Skin_of_right_free_lower_limb")!=skinList.end() );
}

TEST(AnatomyDB_test, getSubPartOfListRecursive) {
    std::vector<std::string> subPartOfList = anatomydb::getSubPartOfList("Pelvic_skeleton", "Frame", true);
    EXPECT_EQ(4, subPartOfList.size());
    EXPECT_TRUE( std::find(subPartOfList.begin(), subPartOfList.end(), "Pelvic_frame")!=subPartOfList.end() );
    EXPECT_TRUE( std::find(subPartOfList.begin(), subPartOfList.end(), "Pelvis_left_hip_frame")!=subPartOfList.end() );
    EXPECT_TRUE( std::find(subPartOfList.begin(), subPartOfList.end(), "Pelvis_right_hip_frame")!=subPartOfList.end() );
    EXPECT_TRUE( std::find(subPartOfList.begin(), subPartOfList.end(), "L5_S1_arch_frame")!=subPartOfList.end() );
}

TEST(AnatomyDB_test, getSynonymList) {
    std::vector<std::string> synonymList;
    synonymList = anatomydb::getSynonymList("Fourth_cervical_vertebra");
    EXPECT_TRUE( std::find(synonymList.begin(), synonymList.end(), "C4")!=synonymList.end() );
    EXPECT_TRUE( std::find(synonymList.begin(), synonymList.end(), "Fourth_cervical_vertebra")!=synonymList.end() );

    synonymList = anatomydb::getSynonymList("T1");
    EXPECT_TRUE( std::find(synonymList.begin(), synonymList.end(), "T1")!=synonymList.end() );
    EXPECT_TRUE( std::find(synonymList.begin(), synonymList.end(), "T01")!=synonymList.end() );
    EXPECT_TRUE( std::find(synonymList.begin(), synonymList.end(), "First_thoracic_vertebra")!=synonymList.end() );

    synonymList = anatomydb::getSynonymList("T1_frame");
    EXPECT_TRUE( std::find(synonymList.begin(), synonymList.end(), "T1_frame")!=synonymList.end() );
    EXPECT_TRUE( std::find(synonymList.begin(), synonymList.end(), "T01_frame")!=synonymList.end() );
    EXPECT_TRUE( std::find(synonymList.begin(), synonymList.end(), "First_thoracic_vertebra_frame")!=synonymList.end() );

}

TEST(AnatomyDB_test, getSynonymFromBibliography) {
    EXPECT_EQ("Fourth_cervical_vertebra", anatomydb::getSynonymFromBibliography("Fourth_cervical_vertebra", "FMA"));
    EXPECT_EQ("Fifth_cervical_vertebra", anatomydb::getSynonymFromBibliography("C5", "FMA"));
    EXPECT_EQ("Right_anterior_superior_iliac_spine", anatomydb::getSynonymFromBibliography("RASIS", "FMA"));
    EXPECT_EQ("Right_anterior_superior_iliac_spine", anatomydb::getSynonymFromBibliography("Right_anterior_superior_iliac_spine", "FMA"));
    EXPECT_EQ("LHJCPEL", anatomydb::getSynonymFromBibliography("LHJCPEL", "Kepple1997"));
    EXPECT_EQ("LHJCPEL", anatomydb::getSynonymFromBibliography("Left_acetabular_center", "Kepple1997"));
    EXPECT_EQ("", anatomydb::getSynonymFromBibliography("LHJCPEL", "FMA"));
}

TEST(AnatomyDB_test, isEntityFromBibliography) {
    EXPECT_TRUE(anatomydb::isEntityFromBibliography("Left_femur", "FMA"));
    EXPECT_FALSE(anatomydb::isEntityFromBibliography("Pelvic_skeleton", "Kepple1997"));
    EXPECT_TRUE(anatomydb::isEntityFromBibliography("T9_frame", "ISB_BCS"));
    EXPECT_FALSE(anatomydb::isEntityFromBibliography("T9_frame", "ISB_JCS"));
    EXPECT_TRUE(anatomydb::isEntityFromBibliography("C1_C2_arch_frame", "ISB_JCS"));
    EXPECT_FALSE(anatomydb::isEntityFromBibliography("C1_C2_arch_frame", "ISB_BCS"));
}

TEST(AnatomyDB_test, getEntityBibliographyList) {
    std::vector<std::string> bibliographyList = anatomydb::getEntityBibliographyList("RASIS");
    ASSERT_GE(bibliographyList.size(), 2);
    EXPECT_TRUE( std::find(bibliographyList.begin(), bibliographyList.end(), "FMA")!=bibliographyList.end() );
    EXPECT_TRUE( std::find(bibliographyList.begin(), bibliographyList.end(), "Kepple1997")!=bibliographyList.end() );
}

TEST(AnatomyDB_test, getLandmarkBoneList) {
    std::vector<std::string> entityList;
    entityList = anatomydb::getLandmarkBoneList("RASIS");
    ASSERT_EQ(2, entityList.size());
    EXPECT_EQ("Right_hip_bone", entityList[0]);
    EXPECT_EQ("Pelvic_skeleton", entityList[1]);

    entityList = anatomydb::getLandmarkBoneList("Cotyle_L");
    ASSERT_EQ(2, entityList.size());
    EXPECT_EQ("Left_hip_bone", entityList[0]);
    EXPECT_EQ("Pelvic_skeleton", entityList[1]);


}

TEST(AnatomyDB_test, getInsertOnList) {
    std::vector< std::vector<std::string> > entityList;
    entityList = anatomydb::getInsertOnList("Articular_capsule_of_right_knee_joint", "Bone");
    ASSERT_EQ(2, entityList.size());
    std::vector<std::string> const& femurSide = entityList[0].size()==1 ? entityList[0] : entityList[1];
    std::vector<std::string> const& tibiaSide = entityList[0].size()==2 ? entityList[0] : entityList[1];
    ASSERT_EQ(1, femurSide.size());
    EXPECT_EQ("Right_femur", femurSide[0]);
    ASSERT_EQ(2, tibiaSide.size());
    EXPECT_EQ("Right_tibia", tibiaSide[0]);
    EXPECT_EQ("Skeleton_of_right_leg", tibiaSide[1]);
}

TEST(AnatomyDB_test, getAnatomicalEntityList) {
    std::vector<std::string> entityList;
    anatomydb::getAnatomicalEntityList(entityList);
    EXPECT_TRUE( std::find(entityList.begin(), entityList.end(), "Left_femur")!=entityList.end() );
}

TEST(AnatomyDB_test, getJointList) {
    std::vector<std::string> jointList;
    anatomydb::getJointList(jointList);
    EXPECT_TRUE( std::find(jointList.begin(), jointList.end(), "Left_hip_joint")!=jointList.end() );
}

TEST(AnatomyDB_test, getLandmarkList) {
    std::vector<std::string> landmarkList;
    anatomydb::getLandmarkList(landmarkList);
    EXPECT_TRUE( std::find(landmarkList.begin(), landmarkList.end(), "Right_anterior_superior_iliac_spine")!=landmarkList.end() );
}

TEST(AnatomyDB_test, search) {
    std::vector<std::string> searchList = anatomydb::search("Bo","Anatomical_entity",true);
    EXPECT_TRUE( std::find(searchList.begin(), searchList.end(), "Body")!=searchList.end() );
    EXPECT_TRUE( std::find(searchList.begin(), searchList.end(), "Vertebral_column")!=searchList.end() );
    EXPECT_FALSE( std::find(searchList.begin(), searchList.end(), "Atlas")!=searchList.end() );
}



