/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <stdexcept>

#include "anatomyDB/types.h"
#include "anatomyDB/FrameFactory.h"

/* This file defines anatomical frames (mainly) as defined by ISB publications.
 * Frames functions are sorted from head to toe.
 * Refer to publications for landmark
 */
namespace anatomydb {

// returns the Scapula JCS for Glenohumeral joint based on ISB recommendations
// inputs: humerus head center (HHC)
void _Scapula_GH(Frame &frame,  Vector3 const& HHC, Side /*side*/) // AI, AA, TS,
{
    frame.translation() = HHC;
    frame.linear().setIdentity();
}

REGISTER_FRAME(Left_scapula_glenohumeral_frame, [](Frame& frame, LandmarkCont const& l){_Scapula_GH(frame, l.get("Head_center_of_left_humerus"), Side::LEFT); });
REGISTER_FRAME(Right_scapula_glenohumeral_frame, [](Frame& frame, LandmarkCont const& l){_Scapula_GH(frame, l.get("Head_center_of_right_humerus"), Side::RIGHT); });

// Body segment CS: Humerus 1st option
void Humerus_1(Frame &frame, Vector3 const& GH, Vector3 const& EL, Vector3 const& EM, Side side)
{
    frame.translation() = GH;
    // v
    frame.linear().col(1)=GH-(EL+EM)*0.5; frame.linear().col(1).normalize();
    // u
    frame.linear().col(0)=(GH-EL).cross(GH-EM); frame.linear().col(0).normalize();
    if (side!=Side::RIGHT)
        frame.linear().col(0)*=-1.;
    // w
    frame.linear().col(2) = frame.linear().col(0).cross(frame.linear().col(1));

    frame.linear().transpose();
}

// returns the Scapula JCS for Glenohumeral joint based on ISB recommendations
// inputs: humerus head center (HHC)
void _Humerus_GH(Frame &frame,  Vector3 const& GH, Vector3 const& EL, Vector3 const& EM, Side side)
{
    Humerus_1(frame, GH, EL, EM, side);
}

REGISTER_FRAME(Left_humerus_glenohumeral_frame, [](Frame& frame, LandmarkCont const& l){_Humerus_GH(
                frame,
                l.get("Head_center_of_left_humerus"),
                l.get("Lateral_epicondyle_of_left_humerus"),
                l.get("Medial_epicondyle_of_left_humerus"),
                Side::LEFT); });
REGISTER_FRAME(Right_humerus_glenohumeral_frame, [](Frame& frame, LandmarkCont const& l){_Humerus_GH(
                frame,
                l.get("Head_center_of_right_humerus"),
                l.get("Lateral_epicondyle_of_right_humerus"),
                l.get("Medial_epicondyle_of_right_humerus"),
                Side::RIGHT); });

// compute the scapula JCS for acromioclavicular joint (no ISB recommendation)
// inputs: scapula and clavicle acromioclavicular surface
void _Scapula_AC(Frame &frame, Vector3 const& Scapula_AC, Vector3 const& Clavicle_AC, Side /*side*/)
{
    frame.translation() = (Scapula_AC+Clavicle_AC)*0.5;
    frame.linear().setIdentity();
}

REGISTER_FRAME(Left_scapula_acromioclavicular_frame, [](Frame& frame, LandmarkCont const& l){_Scapula_AC(frame, l.get("Left_scapula_acromioclavicular_joint"), l.get("Left_clavicle_acromioclavicular_joint"), Side::LEFT); });
REGISTER_FRAME(Right_scapula_acromioclavicular_frame, [](Frame& frame, LandmarkCont const& l){_Scapula_AC(frame, l.get("Right_scapula_acromioclavicular_joint"), l.get("Right_clavicle_acromioclavicular_joint"), Side::RIGHT); });


// compute the clavicle JCS for acromioclavicular joint (no ISB recommendation)
// inputs: scapula and clavicle acromioclavicular surface
void _Clavicle_AC(Frame &frame, Vector3 const& Scapula_AC, Vector3 const& Clavicle_AC, Side /*side*/)
{
    frame.translation() = (Scapula_AC+Clavicle_AC)*0.5;
    frame.linear().setIdentity();
}

REGISTER_FRAME(Left_clavicle_acromioclavicular_frame, [](Frame& frame, LandmarkCont const& l){_Clavicle_AC(frame, l.get("Left_scapula_acromioclavicular_joint"), l.get("Left_clavicle_acromioclavicular_joint"), Side::LEFT); });
REGISTER_FRAME(Right_clavicle_acromioclavicular_frame, [](Frame& frame, LandmarkCont const& l){_Clavicle_AC(frame, l.get("Right_scapula_acromioclavicular_joint"), l.get("Right_clavicle_acromioclavicular_joint"), Side::RIGHT); });

// compute the clavicle JCS for sternoclavicular joint (no ISB recommendation)
// inputs: clavicle and sternum sternoclavicular surface
void _Clavicle_SC(Frame &frame, Vector3 const& Clavicle_SC, Vector3 const& Sternum_SC, Side /*side*/)
{
    frame.translation() = (Clavicle_SC+Sternum_SC)*0.5;
    frame.linear().setIdentity();
}

REGISTER_FRAME(Left_clavicle_sternoclavicular_frame, [](Frame& frame, LandmarkCont const& l){_Clavicle_SC(frame, l.get("Left_clavicle_sternoclavicular_joint"), l.get("Left_sternum_sternoclavicular_joint"), Side::LEFT); });
REGISTER_FRAME(Right_clavicle_sternoclavicular_frame, [](Frame& frame, LandmarkCont const& l){_Clavicle_SC(frame, l.get("Right_clavicle_sternoclavicular_joint"), l.get("Right_sternum_sternoclavicular_joint"), Side::RIGHT); });


// compute the sternum JCS for sternoclavicular joint (no ISB recommendation)
// inputs: clavicle and sternum sternoclavicular surface
void _Sternum_SC(Frame &frame, Vector3 const& Clavicle_SC, Vector3 const& Sternum_SC, Side /*side*/)
{
    frame.translation() = (Clavicle_SC+Sternum_SC)*0.5;
    frame.linear().setIdentity();
}

REGISTER_FRAME(Sternum_left_sternoclavicular_frame, [](Frame& frame, LandmarkCont const& l){_Sternum_SC(frame, l.get("Left_clavicle_sternoclavicular_joint"), l.get("Left_sternum_sternoclavicular_joint"), Side::LEFT); });
REGISTER_FRAME(Sternum_right_sternoclavicular_frame, [](Frame& frame, LandmarkCont const& l){_Sternum_SC(frame, l.get("Right_clavicle_sternoclavicular_joint"), l.get("Right_sternum_sternoclavicular_joint"), Side::RIGHT); });

void Forearm(Frame &frame, Vector3 const& US, Vector3 const& RS, Vector3 const& EL, Vector3 const& EM, Side side)
{
    frame.translation() = US;
    Vector3 mid_EL_EM=(EL-EM)*0.5;
    // v
    frame.linear().col(1)=mid_EL_EM-US; frame.linear().col(1).normalize();
    // u
    frame.linear().col(0)=(RS-mid_EL_EM).cross(US-mid_EL_EM); frame.linear().col(0).normalize();
    if (side!=Side::RIGHT)
        frame.linear().col(0)*=-1.;
    // w
    frame.linear().col(2) = frame.linear().col(0).cross(frame.linear().col(1));

    frame.linear().transpose();
}

void Ulna(Frame &frame, Vector3 const& US, Vector3 const& EL, Vector3 const& EM, Side side)
{
    frame.translation() = US;
    Vector3 mid_EL_EM=(EL-EM)*0.5;
    // v
    frame.linear().col(1)=mid_EL_EM-US; frame.linear().col(1).normalize();
    // u
    frame.linear().col(0)=(US-EM).cross(US-EL); frame.linear().col(0).normalize();
    if (side!=Side::RIGHT)
        frame.linear().col(0)*=-1.;
    // w
    frame.linear().col(2) = frame.linear().col(0).cross(frame.linear().col(1));

    frame.linear().transpose();
}

void Radius(Frame &frame, Vector3 const& US, Vector3 const& RS, Vector3 const& EL, Side side)
{
    frame.translation() = RS;
    // v
    frame.linear().col(1)=EL-RS; frame.linear().col(1).normalize();
    // u
    frame.linear().col(0)=(RS-EL).cross(US-EL); frame.linear().col(0).normalize();
    if (side!=Side::RIGHT)
        frame.linear().col(0)*=-1.;
    // w
    frame.linear().col(2) = frame.linear().col(0).cross(frame.linear().col(1));

    frame.linear().transpose();
}

// compute the humerus JCS for humeroulnar joint (ISB recommendation)
void _Humerus_HU(Frame &frame, Vector3 const& GH, Vector3 const& EL, Vector3 const& EM, Side side)
{
    Humerus_1(frame, GH, EL, EM, side);
    frame.translation() = (EL+EM)*0.5;
}

REGISTER_FRAME(Left_humerus_humeroulnar_frame, [](Frame& frame, LandmarkCont const& l){_Humerus_HU(
                frame,
                l.get("Head_center_of_left_humerus"),
                l.get("Lateral_epicondyle_of_left_humerus"),
                l.get("Medial_epicondyle_of_left_humerus"),
                Side::LEFT); });
REGISTER_FRAME(Right_humerus_humeroulnar_frame, [](Frame& frame, LandmarkCont const& l){_Humerus_HU(
                frame,
                l.get("Head_center_of_right_humerus"),
                l.get("Lateral_epicondyle_of_right_humerus"),
                l.get("Medial_epicondyle_of_right_humerus"),
                Side::RIGHT); });

// compute the ulna JCS for humeroulnar joint (ISB recommendation)
void _Ulna_HU(Frame &frame, Vector3 const& US, Vector3 const& EL, Vector3 const& EM, Side side)
{
    Ulna(frame, US, EL, EM, side);
    frame.translation() = (EL+EM)*0.5;
}

REGISTER_FRAME(Left_ulna_humeroulnar_frame, [](Frame& frame, LandmarkCont const& l){_Ulna_HU(
                frame,
                l.get("Styloid_process_of_left_ulna"),
                l.get("Lateral_epicondyle_of_left_humerus"),
                l.get("Medial_epicondyle_of_left_humerus"),
                Side::LEFT); });
REGISTER_FRAME(Right_ulna_humeroulnar_frame, [](Frame& frame, LandmarkCont const& l){_Ulna_HU(
                frame,
                l.get("Styloid_process_of_right_ulna"),
                l.get("Lateral_epicondyle_of_right_humerus"),
                l.get("Medial_epicondyle_of_right_humerus"),
                Side::RIGHT); });

// compute the ulna JCS for radiocarpal joint (ISB recommendation not clear...)
void _Radius_RC(Frame &frame, Vector3 const& US, Vector3 const& RS, Vector3 const& EL, Side side)
{
    Radius(frame, US, RS, EL, side);
    frame.translation() = (US+RS)*0.5;
}

REGISTER_FRAME(Left_radius_radiocarpal_frame, [](Frame& frame, LandmarkCont const& l){_Radius_RC(
                frame,
                l.get("Styloid_process_of_left_ulna"),
                l.get("Styloid_process_of_left_radius"),
                l.get("Lateral_epicondyle_of_left_humerus"),
                Side::LEFT); });
REGISTER_FRAME(Right_radius_radiocarpal_frame, [](Frame& frame, LandmarkCont const& l){_Radius_RC(
                frame,
                l.get("Styloid_process_of_right_ulna"),
                l.get("Styloid_process_of_right_radius"),
                l.get("Lateral_epicondyle_of_right_humerus"),
                Side::RIGHT); });


// compute the carpal JCS for radiocarpal joint (ISB recommendation not clear...)
void _Carpal_RC(Frame &frame, Vector3 const& US, Vector3 const& RS, Vector3 const& EL, Side side)
{
    _Radius_RC(frame, US, RS, EL, side);
}

REGISTER_FRAME(Left_carpal_radiocarpal_frame, [](Frame& frame, LandmarkCont const& l){_Carpal_RC(
                frame,
                l.get("Styloid_process_of_left_ulna"),
                l.get("Styloid_process_of_left_radius"),
                l.get("Lateral_epicondyle_of_left_humerus"),
                Side::LEFT); });
REGISTER_FRAME(Right_carpal_radiocarpal_frame, [](Frame& frame, LandmarkCont const& l){_Carpal_RC(
                frame,
                l.get("Styloid_process_of_right_ulna"),
                l.get("Styloid_process_of_right_radius"),
                l.get("Lateral_epicondyle_of_right_humerus"),
                Side::RIGHT); });


//Lateral_epicondyle_of_left_humerus
//Medial_epicondyle_of_left_humerus
//void _Humerus_Elbow(Frame &frame,  Vector3 const& , Side /*side*/)
//{

//}


// only set the orientation of the frame
// CLP/CUP center of lower/upper plate
// LL/RL left/right landmark
void _VertebraOrientation(Frame &frame, Vector3 const& CLP, Vector3 const& CUP, Vector3 const& LL, Vector3 const& RL)
{
    // v
    frame.linear().col(1)=CUP-CLP; frame.linear().col(1).normalize();
    // w
    frame.linear().col(2)=RL-LL; frame.linear().col(2).normalize();
    // u
    frame.linear().col(0) = frame.linear().col(1).cross(frame.linear().col(2));

    frame.linear().transpose();
}

void _Vertebra(Frame &frame, Vector3 const& CLP, Vector3 const& CUP, Vector3 const& LL, Vector3 const& RL)
{
    frame.translation() = (CLP+CUP)*0.5;
    _VertebraOrientation(frame, CLP, CUP, LL, RL);
}

// special case for C1 for which no upper plate is defined
// LL/RL left/right landmarks
// PL/AL posterior/anterior landmarks
void _VertebraC1(Frame &frame, Vector3 const& LL, Vector3 const& RL, Vector3 const& PL, Vector3 const& AL )
{
    frame.translation() = (PL+AL)*0.5;
    // w
    frame.linear().col(2)=RL-LL; frame.linear().col(2).normalize();
    Vector3 pseudoU = AL-PL; pseudoU.normalize();
    // v
    frame.linear().col(1) = frame.linear().col(2).cross(pseudoU);
    // u
    frame.linear().col(0) = frame.linear().col(1).cross(frame.linear().col(2));
}

//compute vertebra coordinate system (not based on ISB)
//landmarks: Center Lower/Upper Plate
//L_ upper vertebra
void _VertebraLower(Frame &frame, Vector3 const& CLP, Vector3 const& L_CUP)
{
    frame.translation() = (CLP+L_CUP)*0.5;
    frame.linear().setIdentity();
}

//compute vertebra coordinate system (not based on ISB)
//landmarks: Posterior/Anterior Lower/Upper Plate
//U_ upper vertebra
//void _VertebraLower(Frame &frame, Vector3 const& CLP, Vector3 const& CUP, Vector3 const& L_CUP, Vector3 const& U_ALP)
//{
//    frame.translation() = (PUP+AUP+U_PLP+U_ALP)*0.25;
//    // u
//    frame.linear().col(0) = AUP-PUP; frame.linear().col(0).normalize();
//    // v
//    frame.linear().col(1) = U_PLP-PUP; frame.linear().col(1).normalize();
//    // w
//    frame.linear().col(2) = frame.linear().col(0).cross(frame.linear().col(1));

//    frame.linear().transpose();
//}

//compute vertebra coordinate system (not based on ISB)
//landmarks: Center Lower/Upper Plate
//L_ upper vertebra
void _VertebraUpper(Frame &frame, Vector3 const& CUP, Vector3 const& U_CLP)
{
    frame.translation() = (CUP+U_CLP)*0.5;
    frame.linear().setIdentity();
}

//compute vertebra coordinate system (not based on ISB)
//landmarks: Posterior/Anterior Lower/Upper Plate
//L_ lower vertebra
//void _VertebraUpper(Frame &frame, Vector3 const& PLP, Vector3 const& ALP, Vector3 const& L_PUP, Vector3 const& L_AUP)
//{
//    frame.translation() = (L_PUP+L_AUP+PLP+ALP)*0.25;
//    // u
//    frame.linear().col(0) = ALP-PLP; frame.linear().col(0).normalize();
//    // v
//    frame.linear().col(1) = PLP-L_PUP; frame.linear().col(1).normalize();
//    // w
//    frame.linear().col(2) = frame.linear().col(0).cross(frame.linear().col(1));

//    frame.linear().transpose();
//}

/*
 * register JCS for the spine
 */

// let's be lazy and define a few macros macros
#define REGISTER_FRAME_VERTEBRA_LOWER(V1,V2) REGISTER_FRAME(V1 ## _ ## V2 ## _arch_frame, [](Frame& frame, LandmarkCont const& l){_VertebraLower(frame, l.get("Center_lower_plate_of_"#V1), l.get("Center_upper_plate_of_"#V2)); })
#define REGISTER_FRAME_VERTEBRA_UPPER(V1,V2) REGISTER_FRAME(V1 ## _ ## V2 ## _arch_frame, [](Frame& frame, LandmarkCont const& l){_VertebraUpper(frame, l.get("Center_upper_plate_of_"#V1), l.get("Center_lower_plate_of_"#V2)); })

#define REGISTER_FRAME_VERTEBRA_BOTH(V1,V2) \
    REGISTER_FRAME_VERTEBRA_LOWER(V1,V2); \
    REGISTER_FRAME_VERTEBRA_UPPER(V2,V1) \

REGISTER_FRAME(Skull_C1_frame, [](Frame& frame, LandmarkCont const& l){_VertebraUpper(frame, l.get("Center_atlanto_occipital_joint"), l.get("Center_atlanto_occipital_joint")); });
REGISTER_FRAME(C1_Skull_frame, [](Frame& frame, LandmarkCont const& l){_VertebraLower(frame, l.get("Center_atlanto_occipital_joint"), l.get("Center_atlanto_occipital_joint")); });

REGISTER_FRAME_VERTEBRA_BOTH(C1,C2); // TODO not an "arch" joint in FMA
REGISTER_FRAME_VERTEBRA_BOTH(C2,C3);
REGISTER_FRAME_VERTEBRA_BOTH(C3,C4);
REGISTER_FRAME_VERTEBRA_BOTH(C4,C5);
REGISTER_FRAME_VERTEBRA_BOTH(C5,C6);
REGISTER_FRAME_VERTEBRA_BOTH(C6,C7);
REGISTER_FRAME_VERTEBRA_BOTH(C7,T1);
REGISTER_FRAME_VERTEBRA_BOTH(T1,T2);
REGISTER_FRAME_VERTEBRA_BOTH(T2,T3);
REGISTER_FRAME_VERTEBRA_BOTH(T3,T4);
REGISTER_FRAME_VERTEBRA_BOTH(T4,T5);
REGISTER_FRAME_VERTEBRA_BOTH(T5,T6);
REGISTER_FRAME_VERTEBRA_BOTH(T6,T7);
REGISTER_FRAME_VERTEBRA_BOTH(T7,T8);
REGISTER_FRAME_VERTEBRA_BOTH(T8,T9);
REGISTER_FRAME_VERTEBRA_BOTH(T9,T10);
REGISTER_FRAME_VERTEBRA_BOTH(T10,T11);
REGISTER_FRAME_VERTEBRA_BOTH(T11,T12);
REGISTER_FRAME_VERTEBRA_BOTH(T12,L1);
REGISTER_FRAME_VERTEBRA_BOTH(L1,L2);
REGISTER_FRAME_VERTEBRA_BOTH(L2,L3);
REGISTER_FRAME_VERTEBRA_BOTH(L3,L4);
REGISTER_FRAME_VERTEBRA_BOTH(L4,L5);

REGISTER_FRAME(L5_S1_arch_frame, [](Frame& frame, LandmarkCont const& l){_VertebraLower(frame, l.get("Center_lower_plate_of_L5"), l.get("Center_Sacral_Plate")); });
REGISTER_FRAME(S1_L5_arch_frame, [](Frame& frame, LandmarkCont const& l){_VertebraUpper(frame, l.get("Center_Sacral_Plate"), l.get("Center_lower_plate_of_L5")); });

/*
 * register the reference frames for the vertebrae
 */

// let's be lazy and define a few macros macros
// based on the available landmarks, choose the lateral landmarks for medio/lateral vertebra axis
#define REGISTER_FRAME_VERTEBRA(V) \
    REGISTER_FRAME_I(V ## _frame, 0, [](Frame& frame, LandmarkCont const& l){_Vertebra(frame, l.get("Center_lower_plate_of_"#V), l.get("Center_upper_plate_of_"#V), l.get("Lower_articular_facet_of_"#V"_L"), l.get("Lower_articular_facet_of_"#V"_R") ); }); \
    REGISTER_FRAME_I(V ## _frame, 1, [](Frame& frame, LandmarkCont const& l){_Vertebra(frame, l.get("Center_lower_plate_of_"#V), l.get("Center_upper_plate_of_"#V), l.get("Left_point_of_the_inferior_plate_of_"#V), l.get("Right_point_of_the_inferior_plate_of_"#V) ); }); \
    REGISTER_FRAME_I(V ## _frame, 2, [](Frame& frame, LandmarkCont const& l){_Vertebra(frame, l.get("Center_lower_plate_of_"#V), l.get("Center_upper_plate_of_"#V), l.get("Left_point_of_the_superior_plate_of_"#V), l.get("Right_point_of_the_superior_plate_of_"#V) ); })


REGISTER_FRAME(C1_frame, [](Frame& frame, LandmarkCont const& l){_VertebraC1(frame, l.get("Lower_articular_facet_of_C1_L"), l.get("Lower_articular_facet_of_C1_R"), l.get("Posterior_arch_of_C1"), l.get("Tuberosity_of_the_anterior_arch_of_C1")); });
REGISTER_FRAME_VERTEBRA(C2);
REGISTER_FRAME_VERTEBRA(C3);
REGISTER_FRAME_VERTEBRA(C4);
REGISTER_FRAME_VERTEBRA(C5);
REGISTER_FRAME_VERTEBRA(C6);
REGISTER_FRAME_VERTEBRA(C7);
REGISTER_FRAME_VERTEBRA(T1);
REGISTER_FRAME_VERTEBRA(T2);
REGISTER_FRAME_VERTEBRA(T3);
REGISTER_FRAME_VERTEBRA(T4);
REGISTER_FRAME_VERTEBRA(T5);
REGISTER_FRAME_VERTEBRA(T6);
REGISTER_FRAME_VERTEBRA(T7);
REGISTER_FRAME_VERTEBRA(T8);
REGISTER_FRAME_VERTEBRA(T9);
REGISTER_FRAME_VERTEBRA(T10);
REGISTER_FRAME_VERTEBRA(T11);
REGISTER_FRAME_VERTEBRA(T12);
REGISTER_FRAME_VERTEBRA(L1);
REGISTER_FRAME_VERTEBRA(L2);
REGISTER_FRAME_VERTEBRA(L3);
REGISTER_FRAME_VERTEBRA(L4);
REGISTER_FRAME_VERTEBRA(L5);


// returns the Pelvis JCS for Hip based on ISB recommendations
// inputs: hip joint center (HJC), left/right anterior superior iliac spine, left/right posterior superior iliac spine
void _Pelvis_Hip(Frame &frame, Vector3 const& HJC, Vector3 const& r_ASIS, Vector3 const& l_ASIS, Vector3 const& r_PSIS, Vector3 const& l_PSIS)
{
    frame.translation()=HJC;

    Vector3 m_PSIS = (r_PSIS+l_PSIS)*0.5;
    // w
    frame.linear().col(2) =r_ASIS-l_ASIS; frame.linear().col(2).normalize();
    // v
    frame.linear().col(1)=(r_ASIS-m_PSIS).cross(l_ASIS-m_PSIS); frame.linear().col(1).normalize();
    // u
    frame.linear().col(0) = frame.linear().col(1).cross(frame.linear().col(2));

    frame.linear().transpose();
}

REGISTER_FRAME(Pelvis_right_hip_frame, [](Frame& frame, LandmarkCont const& landmark){ _Pelvis_Hip(frame, landmark.get("Right_acetabular_center"), landmark.get("RASIS"), landmark.get("LASIS"), landmark.get("RPSIS"), landmark.get("LPSIS")); });
REGISTER_FRAME(Pelvis_left_hip_frame, [](Frame& frame, LandmarkCont const& landmark){ _Pelvis_Hip(frame, landmark.get("Left_acetabular_center"), landmark.get("RASIS"), landmark.get("LASIS"), landmark.get("RPSIS"), landmark.get("LPSIS")); });

REGISTER_FRAME(Pelvic_frame, [](Frame& frame, LandmarkCont const& landmark){
    _Pelvis_Hip(frame,
                (landmark.get("Left_acetabular_center") + landmark.get("Right_acetabular_center")) / 2.,
                landmark.get("RASIS"), landmark.get("LASIS"),
                landmark.get("RPSIS"), landmark.get("LPSIS")); });

void Femur(Frame& frame, Vector3 const& HJC, Vector3 const& FE_L, Vector3 const& FE_M, Side side)
{
    frame.translation()=HJC;

    // v
    frame.linear().col(1)=HJC-(FE_L+FE_M)*0.5; frame.linear().col(1).normalize();
    // w
    frame.linear().col(2)=((FE_L-HJC).cross(FE_M-HJC)).cross(frame.linear().col(1)); frame.linear().col(2).normalize();
    if (side!=Side::RIGHT)
        frame.linear().col(2)*=-1.;
    // u
    frame.linear().col(0)=frame.linear().col(1).cross(frame.linear().col(2));

    frame.linear().transpose();
}

//compute Femur JCS for Hip
void _Femur_Hip(Frame& frame, Vector3 const& HJC, Vector3 const& FE_L, Vector3 const& FE_M, Side side)
{
    Femur(frame, HJC, FE_L, FE_M, side);
}

REGISTER_FRAME(Left_femur_hip_frame, [](Frame& frame, LandmarkCont const& landmark){ _Femur_Hip(frame, landmark.get("Head_center_of_left_femur"), landmark.get("Lateral_epicondyle_of_left_femur"), landmark.get("Medial_epicondyle_of_left_femur"), Side::LEFT); });
REGISTER_FRAME(Right_femur_hip_frame, [](Frame& frame, LandmarkCont const& landmark){ _Femur_Hip(frame, landmark.get("Head_center_of_right_femur"), landmark.get("Lateral_epicondyle_of_right_femur"), landmark.get("Medial_epicondyle_of_right_femur"), Side::RIGHT); });

// returns the Femur JCS for Knee based on ISB recommendations
// inputs: hip joint center (HJC), lateral and medial femoral epicondyles (FE_L,FE_M), and side
void _Femur_Knee(Frame& frame, Vector3 const& HJC, Vector3 const& FE_L, Vector3 const& FE_M, Side side)
{

    Femur(frame, HJC, FE_L, FE_M, side);
    frame.translation()=(FE_L+FE_M)*0.5;

    // definition in 1983 paper not consistent with the one in the 2002 paper...
//    Vector3 MFE = (FE_L+FE_M)*0.5;
//    frame.translation()=MFE;
//    // v
//    frame.linear().col(1)=HJC-MFE; frame.linear().col(1).normalize();
//    // u
//    frame.linear().col(0) = (HJC-FE_L).cross(HJC-FE_M); frame.linear().col(0).normalize();
//    if (side!=Side::RIGHT)
//        frame.linear().col(0)*=-1;
//    // w
//    frame.linear().col(2) = frame.linear().col(0).cross(frame.linear().col(1));

//    frame.linear().transpose();
}

REGISTER_FRAME(Left_femur_knee_frame, [](Frame& frame, LandmarkCont const& landmark){ _Femur_Knee(frame, landmark.get("Head_center_of_left_femur"), landmark.get("Lateral_epicondyle_of_left_femur"), landmark.get("Medial_epicondyle_of_left_femur"), Side::LEFT); });
REGISTER_FRAME(Right_femur_knee_frame, [](Frame& frame, LandmarkCont const& landmark){ _Femur_Knee(frame, landmark.get("Head_center_of_right_femur"), landmark.get("Lateral_epicondyle_of_right_femur"), landmark.get("Medial_epicondyle_of_right_femur"), Side::RIGHT); });

// returns the Tibia JCS for Knee based on ISB recommendations
// inputs: lateral and medial femoral epicondyles (FE_L,FE_M), Tip of the medial and lateral malleolus (MM and LM), The most medial and lateral points on the border of the tibial condyle (MC and LC) and side
void _Tibia_Knee(Frame& frame, Vector3 const& FE_L, Vector3 const& FE_M, Vector3 const& LC, Vector3 const& MC, Vector3 const& LM, Vector3 const& MM, Side side)
{
    frame.translation() = (FE_L+FE_M)*0.5;
    Vector3 IC = (MC+LC)*0.5;
    Vector3 IM = (LM+MM)*0.5;
    // v
    frame.linear().col(1)=IC-IM; frame.linear().col(1).normalize();
    // u
    frame.linear().col(0) = (MC-IM).cross(LC-IM); frame.linear().col(0).normalize();
    if (side!=Side::RIGHT)
        frame.linear().col(0)*=-1;
    // w
    frame.linear().col(2) = frame.linear().col(0).cross(frame.linear().col(1));

    frame.linear().transpose();
}

REGISTER_FRAME(Left_tibia_knee_frame, [](Frame& frame, LandmarkCont const& l){ _Tibia_Knee(
                frame,
                l.get("Lateral_epicondyle_of_left_femur"),
                l.get("Medial_epicondyle_of_left_femur"),
                l.get("Lateral_point_lateral_condyle_of_left_tibia"),
                l.get("Medial_point_medial_condyle_of_left_tibia"),
                l.get("Tip_lateral_malleolus_of_left_fibula"),
                l.get("Tip_medial_malleolus_of_left_tibia"),
                Side::LEFT); });
REGISTER_FRAME(Right_tibia_knee_frame, [](Frame& frame, LandmarkCont const& l){ _Tibia_Knee(
                frame,
                l.get("Lateral_epicondyle_of_right_femur"),
                l.get("Medial_epicondyle_of_right_femur"),
                l.get("Lateral_point_lateral_condyle_of_right_tibia"),
                l.get("Medial_point_medial_condyle_of_right_tibia"),
                l.get("Tip_lateral_malleolus_of_right_fibula"),
                l.get("Tip_medial_malleolus_of_right_tibia"),
                Side::RIGHT); });

// returns the Tibia JCS for Ankle based on ISB recommendations
// inputs: Tip of the medial and lateral malleolus (MM and LM), The most medial and lateral points on the border of the tibial condyle (MC and LC) and side (eg. "right")

void _Tibia_Ankle(Frame& frame, Vector3 const& LC, Vector3 const& MC, Vector3 const& LM, Vector3 const& MM, Side side)
{
    frame.translation() = (LM+MM)*0.5;
    Vector3 IC = (MC+LC)*0.5;
    // w
    frame.linear().col(2) = LM-MM; frame.linear().col(2).normalize();
    // u
    frame.linear().col(0) = (IC-LM).cross(IC-MM); frame.linear().col(0).normalize();
    if (side!=Side::RIGHT) {
        frame.linear().col(2)*=-1.;
        frame.linear().col(0)*=-1.;
    }
    // v
    frame.linear().col(1) = frame.linear().col(2).cross(frame.linear().col(0));

    frame.linear().transpose();
}

REGISTER_FRAME(Left_tibia_ankle_frame, [](Frame& frame, LandmarkCont const& l){ _Tibia_Ankle(
                frame,
                l.get("Lateral_point_lateral_condyle_of_left_tibia"),
                l.get("Medial_point_medial_condyle_of_left_tibia"),
                l.get("Tip_lateral_malleolus_of_left_fibula"),
                l.get("Tip_medial_malleolus_of_left_tibia"),
                Side::LEFT); });
REGISTER_FRAME(Right_tibia_ankle_frame, [](Frame& frame, LandmarkCont const& l){ _Tibia_Ankle(
                frame,
                l.get("Lateral_point_lateral_condyle_of_right_tibia"),
                l.get("Medial_point_medial_condyle_of_right_tibia"),
                l.get("Tip_lateral_malleolus_of_right_fibula"),
                l.get("Tip_medial_malleolus_of_right_tibia"),
                Side::RIGHT); });

void Calcaneus(Frame& frame, Vector3 const& LC, Vector3 const& MC, Vector3 const& LM, Vector3 const& MM, Side side)
{
    Vector3 IM = (LM+MM)*0.5;
    frame.translation() = IM;
    // v
    frame.linear().col(1) = (LC+MC)*0.5-frame.translation(); frame.linear().col(1).normalize();
    // u
    frame.linear().col(0) = (MC-IM).cross(LC-IM); frame.linear().col(0).normalize();
    if (side!=Side::RIGHT)
        frame.linear().col(0)*=-1.;
    // w
    frame.linear().col(2) = frame.linear().col(0).cross(frame.linear().col(1));

    frame.linear().transpose();
}

void _Calcaneus_Ankle(Frame& frame, Vector3 const& LC, Vector3 const& MC, Vector3 const& LM, Vector3 const& MM, Side side)
{
    Calcaneus(frame, LC, MC, LM, MM, side);
}

REGISTER_FRAME(Left_calcaneus_ankle_frame, [](Frame& frame, LandmarkCont const& l){ _Calcaneus_Ankle(
                frame,
                l.get("Lateral_point_lateral_condyle_of_left_tibia"),
                l.get("Medial_point_medial_condyle_of_left_tibia"),
                l.get("Tip_lateral_malleolus_of_left_fibula"),
                l.get("Tip_medial_malleolus_of_left_tibia"),
                Side::LEFT); });
REGISTER_FRAME(Right_calcaneus_ankle_frame, [](Frame& frame, LandmarkCont const& l){ _Calcaneus_Ankle(
                frame,
                l.get("Lateral_point_lateral_condyle_of_right_tibia"),
                l.get("Medial_point_medial_condyle_of_right_tibia"),
                l.get("Tip_lateral_malleolus_of_right_fibula"),
                l.get("Tip_medial_malleolus_of_right_tibia"),
                Side::RIGHT); });


}





