/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef ANATOMYDB_FRAMEFACTORY_H
#define ANATOMYDB_FRAMEFACTORY_H

#ifdef WIN32
#	ifdef anatomyDB_EXPORTS
#		define ANATOMYDB_EXPORT __declspec( dllexport )
#	else
#		define ANATOMYDB_EXPORT __declspec( dllimport )
#	endif
#else
#	define ANATOMYDB_EXPORT
#endif

#include <string>
#include <vector>
#include <map>
#include <functional>

#include "anatomyDB/types.h"

namespace anatomydb {

class FrameFactory;

/** A simple map container for landmarks, the internal key to index landmarks is obtained from the db, it is the landmark reference name.
 * It is currently the referenceName. Set useNameAsKey to true to bypass this mechanism and only use given names.
 * \todo improve link between the container and the db, add should check that the landmark is known
 * \todo use landmark id instead of reference name (for performance...)
 */
class ANATOMYDB_EXPORT LandmarkCont : public std::map<std::string, Vector3> {
public:
    friend class FrameFactory;

    typedef std::map<std::string, Vector3> ContType;

    void add(std::string const& name, Vector3 const& coord);
    Vector3 const& get(std::string const& name) const;
    bool contains(std::string const& name) const;

private:
    /// \{
    /// attributes dedicated to inspection, mainly used to generate documentation
    bool m_doInspect;
    mutable std::vector<std::string> m_inspectList;
    /// \}
};

/**
 * The FrameFactory handles anatomical frames computation. It uses the singleton and factory design pattern.
 * For each frame several functions can be registered. When a frame is computed,
 * the different functions are tried in the order in which they were registered.
 *
 * \todo template over LandmarkCont ? make it possible to use externally defined LandmarkCont ? to decide with usage...
 * \todo add a "namespace" for compute methods name (for instance bibliography source) to prevent name clashes.
 * \author Thomas Lemaire
 * \date 2016
 * \addtogroup anatomyDB
 */
class ANATOMYDB_EXPORT FrameFactory {

public:

    /// function signature a ComputeFrame function must follow
    typedef std::function<void (Frame&, LandmarkCont const&)> ComputeFrame;
//    typedef void (*ComputeFrame)(Frame&);

    static FrameFactory &instance();

    LandmarkCont const& landmark() const {return m_landmark;}
    LandmarkCont & landmark() {return m_landmark;}

    /// register the function \a computeFrame to compute frame \a name
    bool registerFrame(std::string const& name, ComputeFrame computeFrame);
    /// unregister the frame \a name
    /// @return true if succesfully removed
    bool unregisterFrame(std::string const& name);
    /// @return true if the frame \a name has been registered
    bool isFrameRegistered(std::string const& name);
    /// @return the number of function registered to compute frame \a name
    std::size_t countFrameRegistered(std::string const& name);
    /// @return the list of registered frames name
    std::vector<std::string> registeredFrameList() const;
    /// Check whether frame \a name can be computed, if not the reson is given in \a message
    bool canComputeFrame(std::string const& name, std::string & message);
    /// compute the frame \a name, store the result in \a frame
    void computeFrame(std::string const& name, Frame& frame) const;
    /// compute the frame \a name, store the result in \a frame
    bool computeFrameNoThrow(std::string const& name, Frame& frame) const;

    /// \returns the list of required landmarks to compute the \a frame, one list per compute function
    std::vector< std::vector<std::string> > getFrameRequiredLandmarks(std::string const& name);

private:
    typedef std::multimap<std::string, ComputeFrame> ComputeFrameCont;

    FrameFactory();
    FrameFactory(FrameFactory const&) { }

    ComputeFrameCont m_computeFrame;
    LandmarkCont m_landmark;

};

// a handy macro to register frame
#define REGISTER_FRAME(name, function) bool register_##name = FrameFactory::instance().registerFrame(#name, function)
// a handy macro to register several frames having the same name
#define REGISTER_FRAME_I(name, i, function) bool register_##name##_##i = FrameFactory::instance().registerFrame(#name, function)

}

#endif // ANATOMYDB_FRAMEFACTORY_H
