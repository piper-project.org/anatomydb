/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include <QList>
#include <QObject>
#include <QHash>
#include <QByteArray>
#include <QtQml>

//#include "treeitem.h"

class TreeItem;

/**
 * @brief The TreeModel class, from Qt SimpleTreeModel example. It implements QAstractItemModel and uses
 * TreeItem as items. Some modifications has been added to fit our needs.
 */
class TreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:

    /**
     * @brief TreeModel
     * @param relations : the map that associates an item's name with its parent's name. The relations that doesn't match with a tree structure (no parents nor children) will be ignored.
     * @param data : the map that associates an item's name with the rest of its data (this map can be empty).
     * @param roles : The different roles to display on the tree. This data is also used to build the root item.
     * @param parent
     */
    explicit TreeModel(std::map<QVariant, QList<QVariant> > &relations, std::map<QVariant, QList<QVariant> > &data, QList<QVariant> roles, QObject * parent = 0);

    ~TreeModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    /**
     * @brief roleNames
     * @return the hashmap that contains the roles of the tree
     */
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE QModelIndex child(const QModelIndex &indexParent, int row) const;
    Q_INVOKABLE int rowInParent(const QModelIndex &index) const;
    /// \return indexes of all children of \a parent if \a recursive is false return only direct children indexes
    Q_INVOKABLE QList<QVariant> getChildrenIndexes(const QModelIndex &parent = QModelIndex(), bool recursive=true) const;
    /// \return indexes of all elements in the model
    Q_INVOKABLE QList<QVariant> getAllIndexes() const;
    /// \return indexes of all leaf (have no children) elements of the specified parent element (specify none to get all leafs in model)
    Q_INVOKABLE QList<QVariant> getLeafIndexes(const QModelIndex &parent = QModelIndex()) const;
    /**
     * \return a list a indexes of (data,role) in the tree starting at \a parent (starting at the tree root if \a parent is not specified, if \a doCompareRefName id true, the values in the tree are converted to their reference name when possible
     */
    Q_INVOKABLE QList<QVariant> findIndex(QVariant data, bool doCompareRefName = false, int role = Qt::DisplayRole, const QModelIndex &parent = QModelIndex());

private:    
    /**
     * @brief buildData
     * @param relations : all the known relations between the items
     * @param data : the data known for each item
     * @param current : the name of the item to build
     * @param parent : the parent item of current
     * @return the TreeItem build from current, knowing the relations and the data, with all its descendants corrects
     */
    TreeItem* buildData(std::map<QVariant, QList<QVariant> > &relations, std::map<QVariant, QList<QVariant> > &data, QVariant current, TreeItem* parent);

    void _findIndex(const QModelIndex &parentItemIndex, TreeItem* parentItem, QVariant data, bool doCompareRefName, int role, QList<QVariant>& indexes);

    TreeItem *rootItem;
    QHash<int, QByteArray> m_roleNameMapping;
};

#endif // TREEMODEL_H
