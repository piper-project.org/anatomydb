## Data

Description: the data contained in the anatomyDB database

Authors: UCBL-Ifsttar, CEESAR, INRIA, U Southampton 

Version: 1.0.0

License: Creative Commons Attribution 4.0 International License.
(https://creativecommons.org/licenses/by/4.0/)

This work has received funding from the European Union Seventh Framework 
Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).

Contributors include Thomas Lemaire (INRIA), Christophe Lecomte (U Southampton), 
Erwan Jolivet (CEESAR), Philippe Beillas (UCBL-Ifsttar)


## Software

Copyright (C) 2017 INRIA

AnatomyDB is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as published 
by the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

The anatomyDB is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PIPER Framework.  If not, see <http://www.gnu.org/licenses/>.

Contributors: Thomas Lemaire (INRIA)

## Acknowledgement

This work has received funding from the European Union Seventh Framework 
Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).