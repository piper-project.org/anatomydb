/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "anatomictreemodel.h"

#include <stdlib.h>
#include <iostream>

#include <QStringList>
#include <QString>

#include "query.h"

AnatomicTreeModel::AnatomicTreeModel()
{
//    std::cerr << "\n***************Default anatomic tree model****************\n";
	m_model = nullptr;
    this->setRoles();
    this->setClasses();
    this->setDataList(&std::vector<std::string>());
//    std::cerr << "\n******************************************************************\n\n";
}

AnatomicTreeModel::AnatomicTreeModel(std::vector<std::string> valList)
{
//    std::cerr << "\n***************Initialisation with List of values****************\n";
	m_model = nullptr;
    this->setRoles();
    this->setClasses();
    this->setDataList(&valList);
//    std::cerr << "\n******************************************************************\n\n";
}

AnatomicTreeModel::~AnatomicTreeModel()
{
	if (m_model)
		delete m_model;
}

/**
 * @brief AnatomicTreeModel::getRoles
 * @return  the roles that are normally displayed on the screen.
 */
QList<QVariant> AnatomicTreeModel::getRoles(){
    QList<QVariant> roles = {};
    for (int i = 0; i< m_rolesList.size(); i++){
        roles << roleToQVariant(m_rolesList.at(i));
    }
    return roles;
}


QList<QVariant> AnatomicTreeModel::getQDataList(){
    QList<QVariant> list = {};
    for(int i = 0; i < getDataList().size(); i++ ){
        list.append(QString::fromStdString(getDataList().at(i)));
    }
    return list;
}

void AnatomicTreeModel::setQDataList(QList<QVariant> qlist){
    std::vector<std::string> list = {};
    for(int i = 0; i < qlist.size(); i++ ){
        list.push_back(qlist.at(i).toString().toStdString());
    }
    setDataList(&list);
//    std::cerr << "\n----------------list changed-----------------\n";
}

void AnatomicTreeModel::dataListFromClasses(QStringList classes){
    std::vector<std::string> dataToAdd = {};

    for(int i = 0; i < classes.size(); i ++){
        std::string currentClass = classes.at(i).toStdString();
        if(anatomydb::exists(currentClass)){
//            std::cerr << "The class " << currentClass << " exists \n";
            std::vector<std::string> classList = anatomydb::getSubClassOfList(currentClass);
            dataToAdd.insert(dataToAdd.end(),classList.begin(),classList.end());
        }
    }
    setDataList(&dataToAdd);
}

QVariant AnatomicTreeModel::getDataRole(QVariant role, QString qelement){
    std::vector<std::string>  valuesList = {};
    std::string  valuesString = "";
    std::string element = qelement.toStdString();
    int start =0;

    switch(roleFromQVariant(role)){
    case Roles::Name :
        valuesList.push_back(element);
        break;
    case Roles::Synonyms :
//        start = 1;
        if(anatomydb::exists(element))
            valuesList = anatomydb::getSynonymList(element);
        break;
    case Roles::Class :
        if(anatomydb::exists(element))
            valuesList = anatomydb::getParentClassList(element);
        break;
    case Roles::PartOf :
        if(anatomydb::exists(element))
            valuesList = this->getPartOf(element,false);
        start = 1;
        break;
    case Roles::Bibliography :
        if(anatomydb::exists(element))
            valuesList = anatomydb::getEntityBibliographyList(element);
        break;
//    default :
//        break;
    }

    for(size_t k = start ; k < valuesList.size(); k++){
        if( k != start) valuesString.append(", ");
        valuesString.append(valuesList.at(k));
    }
    return QString::fromStdString(valuesString);
}

/**
 * @brief AnatomicTreeModel::searchTree
 * @param word
 * @param doSynonyms
 * @return the list of elements of anatomydb that are matching the word. Can search also in the synonyms.
 */
//QStringList AnatomicTreeModel::searchTree(QString const& word, bool doSynonyms){

//    std::string stdWord = word.toStdString();
//    QStringList retVal = {};
//    std::vector<std::string> result = {};

//    QStringList classes = getClasses(true);
//    for(int i =0; i< classes.size(); i++){
// //        std::cerr << "search tree\n";
//        std::vector<std::string> temp = anatomydb::search(stdWord,classes.at(i).toStdString(),doSynonyms);

//        result.insert( result.end(), temp.begin(), temp.end() );
//    }
//    for(int j = 0; j<result.size(); j++){
//        retVal << QString::fromStdString(result[j]);
//    }

//    return retVal;
//}

std::string AnatomicTreeModel::getMatchingClass(std::string elem){
    std::string retClass = "";

    if(anatomydb::exists(elem)){
        std::vector<std::string> classList= anatomydb::getParentClassList(elem);

        for(size_t i = 0; i < classList.size(); i++){
            for(size_t j = 0; j< m_classes.size(); j++){
                if(classList.at(i).compare(m_classes.at(j)) == 0){
                    retClass = classList.at(i);
                    break;
                }
            }
        }
    }
    return retClass;
}

std::vector<std::string> AnatomicTreeModel::getElemClasses(std::string element){
    std::string elemClass = this->getMatchingClass(element);
    if(!anatomydb::exists(elemClass)) return {};
    std::vector<std::string> finalClasses = {};
    std::string region = "Region";
    finalClasses.push_back(elemClass);
    finalClasses.push_back(region);
    for(size_t i = 0; i < m_classes.size(); i ++){
        if((m_classes.at(i)!= elemClass)&&(m_classes.at(i) != region)){
            finalClasses.push_back(m_classes.at(i));
        }
    }
    return finalClasses;
}

std::vector<std::string> AnatomicTreeModel::getClasses(){
    return m_classes;
}

void AnatomicTreeModel::setClasses(std::vector<std::string> classList){
    m_classes.clear();

    if(classList.empty()){
        m_classes = {"Region","Bone","Joint","Landmark","Bibliography"};
    }else{
        for (int i = 0; i< classList.size(); i++){
            m_classes.push_back(classList.at(i));
        }
    }
//     std::cerr << "\n----------------classes set-----------------\n";
//    emit classesChanged();
}

TreeModel* AnatomicTreeModel::getModel(){
    return m_model;
}

void AnatomicTreeModel::setModel(TreeModel* model){
    m_model = model;
//    std::cerr << "\n----------------model changed-----------------\n";
    emit modelChanged();
}

std::vector<std::string> AnatomicTreeModel::getDataList(){
    return this->dataList;
}

void AnatomicTreeModel::setDataList(std::vector<std::string> *list){
    this->dataList.clear();
	this->dataList.insert(dataList.begin(), list->begin(), list->end());
    emit listChanged();

    std::map<QVariant,QList<QVariant> > relations_model = {};
    std::map<QVariant,QList<QVariant> > data_model = {};

    if (list->size()>0){
        buildModelTreeFromList(&relations_model,&data_model);
    }
	if (m_model != nullptr)
		delete m_model;
    setModel(new TreeModel(relations_model,data_model,this->getRoles()));
}

void AnatomicTreeModel::setRoles(QList<QVariant> roles){
    m_rolesList.clear();
    if(roles.empty()){
        for ( const auto r : Roles::All )
            m_rolesList << r;
    }else{
        for (int i = 0; i< roles.size(); i++){
            m_rolesList << roleFromQVariant(roles.at(i));
        }
    }
//    std::cerr << "\n----------------role changed-----------------\n";
}

QVariant AnatomicTreeModel::roleToQVariant(Roles::Role r){
    QVariant ret;
    switch (r) {
    case Roles::Name:
        ret = QString::fromStdString("Name");
        break;
    case Roles::Synonyms:
        ret = QString::fromStdString("Synonyms");
        break;
    case Roles::Class:
        ret = QString::fromStdString("Class");
        break;
    case Roles::PartOf:
        ret = QString::fromStdString("PartOf");
        break;
    case Roles::Bibliography:
        ret = QString::fromStdString("Bibliography");
        break;
    default:
        ret = QString::fromStdString("");
        break;
    }
    return ret;
}

Roles::Role AnatomicTreeModel::roleFromQVariant(QVariant val){
    std::string testVal = val.toString().toStdString();
    if(testVal == "Name") return Roles::Name;
    if(testVal == "Synonyms") return Roles::Synonyms;
    if(testVal == "Class") return Roles::Class;
    if(testVal == "PartOf") return Roles::PartOf;
    if(testVal == "Bibliography") return Roles::Bibliography;
    return Roles::Other;
}

Roles::Role AnatomicTreeModel::getRole(int i){
    return this->m_rolesList.at(i);
}

std::vector<std::string> AnatomicTreeModel::getPartOf(std::string const& element, bool allAncestors){
    std::vector<std::string> partOf = {};
    std::vector<std::string> classes = this->getElemClasses(element);



    for(size_t i = 0; i < classes.size(); i ++){
            std::vector<std::string> parents = {};

            if(allAncestors)
                parents = this->getParentsAmong(element, classes.at(i), dataList);
            else{
                parents = anatomydb::getPartOfSubClassList(element,classes.at(i));
            }
            partOf.insert(partOf.end(),parents.begin(),parents.end());


//            std::cerr << "parents of class " << classes.at(i) << " of element " << element << " : \n";

//            for(int j = 0; j < parents.size(); j++)
//                std::cerr << "     " << parents.at(j) << " \n";

    }
    return partOf;
}

std::vector<std::string> AnatomicTreeModel::getParentsAmong(std::string element, std::string classElement,
	std::vector<std::string> const &valList){
    std::vector<std::string> ret = {};
    if(!anatomydb::exists(classElement)){
//        std::cerr << " The class " << classElement << " is not in anatomy DB ! The parents of " << element << " are not accessible. \n";
    }
    else{
        ret = anatomydb::getAllParents(element,classElement);
        size_t i;
        for(i=0 ; i<ret.size() ; i++){     //For each parent of the element
            std::string parent = ret.at(i);                                                 //Get the current parent

            size_t j;
            for(j=0 ; j<valList.size() ; j++){                                              //For each element of the list
                if(valList.at(j)==parent){                                                      //If the parent is in it we remember the position
                    break;                                                                      //Search its position in the list
                }
            }
            if(j == valList.size()){                                                        //If the whole list has been checked and the parent hasn't been found
                ret.erase(ret.begin() + i);                                                    //We remove it from the parent's list
                i--;                                                                           //And we decrement the count
            }
        }
    }
    return ret;
}



std::string AnatomicTreeModel::getFirstParentInList(std::string element, std::vector<std::string> const& classList){

    std::vector<std::string> parentsInList = {};
    std::vector<std::string> directParents = {};
    std::vector<std::string> tempParents = {};

    if(!anatomydb::exists(element)) return "";


    for(size_t i = 0; i < classList.size(); i ++){
        std::string c = classList.at(i);
        if(anatomydb::exists(c)){
            tempParents = anatomydb::getPartOfSubClassList(element,c);              //Get the parents of class c
            directParents.insert(directParents.end(),
                                 tempParents.begin(), tempParents.end());   //add them to all the parents of the element
            for(size_t j = 0; j < tempParents.size(); j ++){                   //For each of the current parent
                std::string parent = tempParents.at(j);
                if(this->isInList(parent)){                                  //Check if it is in the list
                    parentsInList.push_back(parent);                        //and memorize it if doing so
                }
            }
        }
    }

    if(parentsInList.empty()){                                            //If there is no match at this level
        for(size_t i = 0; i < directParents.size(); i ++){                   //For each of the element's parent
            parentsInList.push_back(                                      //Get recursively its closestparent in the datalist
                        getFirstParentInList(directParents.at(i),classList));
        }
    }
    if(parentsInList.empty()) return "";
    return parentsInList.at(0);                                             //Return the first parent
}


bool AnatomicTreeModel::isInList(std::string element){
    for(auto const& dlEle : dataList){
        if(dlEle ==element){                                                      //If the parent is in it we remember the position
            return true;                                                                      //Search its position in the list
        }
    }
    return false;
}


void AnatomicTreeModel::buildModelTreeFromList(std::map<QVariant,QList<QVariant>> *model_relations, std::map<QVariant,QList<QVariant>> *model_data){

    int outOfBounds = 0;
    std::vector<std::string>  parents = {};

    int nbRootItems =0;
    int nbDisplayedElements =0;

    for(size_t i=0 ; i< dataList.size() ; i++){
        std::string elem = dataList.at(i);
        QString qElem = QString::fromStdString(elem);   //Get the element of the list to add

        nbDisplayedElements++;

//        std::cerr << elem << " is computed " << std::endl;

        parents.clear();
        parents.push_back(getFirstParentInList(elem, m_classes));   //Get its closest parent in the list

//        std::cerr << elem << " has " << parents.size() << "parent : " << parents.at(0) << std::endl;

        QVariant parent;
		auto roles = getRoles();
        if(parents.at(0) == ""){                        //This is a root item (ex : Body, Landmark, Skin etc.)
            parent = roles.at(0);                      //So its the child of the root item
            nbRootItems++;
        }else{
            parent = QString::fromStdString(parents[0]);
    //        if(parents.size()>1) outOfBounds++;
        }
        (*model_relations)[parent] <<  qElem;

                //Now we add the other interesting data (the ones that will be displayed in the table) to model_data
        for(int k = 1; k<roles.size(); k++){
           (*model_data)[qElem].insert(k,getDataRole(roles.at(k),QString::fromStdString(elem)));
        }
    }

    //Recap of the results, with interesting data
//    std::cerr // << "Results for " << className().toStdString()
//                  << " : \n Number out of bounds " << outOfBounds
//              << " \n Number of elements : " << i
//              << " \n Number of displayed elements : " << nbDisplayedElements
//              << " \n Number of root items : " << nbRootItems << "\n";
}
