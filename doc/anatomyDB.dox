/*! \defgroup anatomyDB Anatomy Database

\brief The anatomyDB library contains knowledge on the human body anatomy.

\author Thomas Lemaire, Christophe Lecomte \date 2015

\section secAnatomyDBData How the data is managed

The following picture presents how the data is organized in the library :
  -# The MyCF database is query to get the basis of the database (anatomical entities such as bones, group of bones, joints,...). This process is done by the \c make_entity_mycf.py script. The obtain data is stored in the \c entity_mycf_doNotEdit.csv file.
  -# A set of editable \c .csv files contains information that cannot be retrieved from MyCF. This includes synonyms for entity names, anatomical landmarks, anatomical frames,...
  -# The \c create.sql script is used at compile time to produce the sqlite database file.

\image html anatomyDB/doc/dataflow.png "AnatomyDB dataflow"
  
\section secAnatomyDBQuery How to query the database

The core of the anatomyDB library is a set of functions which wrap SQL queries. In addition to the standard C++ library, several wrappers for other target languages are provided.

\image html anatomyDB/doc/usage.png "AnatomyDB usage"

\subsection secAnatomyDBPython In Python

\subsection secAnatomyDBOcatve In Octave

Octave (http://www.octave.org) is an open source alternative to Matlab, and is compatible with it up to a certain level. AnatomyDB functions can be used in octave as shown below :

\include octave_demo.m

output :
\verbatim
C1 synonyms:
ans = 
{
  [1,1] = Atlas
  [2,1] = C1
}
Bony parts of pelvis:
ans = 
{
  [1,1] = Coccyx
  [2,1] = Left_hip_bone
  [3,1] = Right_hip_bone
  [4,1] = Sacrum
}
Right_anterior_superior_iliac_spine in Kepple1997:
ans = RASIS
Landmarks defined in Kepple1997:
ans = 
{
  [1,1] = RASIS
  [2,1] = LASIS
  [3,1] = RPSIS
  [4,1] = LPSIS
  [5,1] = RHJCPEL
  [6,1] = LHJCPEL
  [7,1] = RAIIS
  [8,1] = LAIIS
  ...
}
Fill landmarks coordinates:
ff =

{
  FrameFactory, ptr = 0x7f3d62ffbf40
  computeFrame (method)
  instance (static method)
  isFrameRegistered (method)
  landmark (method)
  registeredFrameList (method)
}

Available frames in factory:
ans = 
{
  [1,1] = C1_C2_arch
  [2,1] = C1_Skull
  ...
  [25,1] = Left_calcaneus_ankle
  [26,1] = Left_carpal_radiocarpal
  [27,1] = Left_clavicle_acromioclavicular
  [28,1] = Left_clavicle_sternoclavicular
  ...
  [38,1] = Left_ulna_humeroulnar
  [39,1] = Pelvis_left_hip
  [40,1] = Pelvis_right_hip
  [41,1] = Right_calcaneus_ankle
  [42,1] = Right_carpal_radiocarpal
  [43,1] = Right_clavicle_acromioclavicular
  [44,1] = Right_clavicle_sternoclavicular
  [45,1] = Right_femur_hip
  [46,1] = Right_femur_knee
  [47,1] = Right_humerus_glenohumeral
  [48,1] = Right_humerus_humeroulnar
  [49,1] = Right_radius_radiocarpal
  ...
}
Left_scapula_glenohumeral frame:
ans =

   1
   2
   3
   0
   0
   0
   1

Left_femur_hip frame cannot be computed (missing landmarks):
error: Error while computating frame Left_femur_hip
Unknown landmark: Medial_epicondyle_of_left_femur (Medial_epicondyle_of_left_femur) (SWIG_RuntimeError)
\endverbatim

*/