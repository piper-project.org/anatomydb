# Copyright (C) 2017 INRIA
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 2.1 of the License, or (at your
# option) any later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
# for more details. You should have received a copy of the GNU Lesser General
# Public License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Thomas Lemaire (INRIA)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 
import os
import shutil
import csv
import myCF

print "Query..."
boneList=myCF.listBone()
setOfBoneList=myCF.listSetOfBone()
jointList=myCF.listSubClassOf("Synovial_joint")

## FIX ##
boneList+=["Skeleton_of_left_hand", "Skeleton_of_right_hand", "Skeleton_of_left_forearm", "Skeleton_of_right_forearm", "Skeleton_of_rib_cage", "Skeleton_of_left_leg", "Skeleton_of_right_leg"]

class Entity:
    def __init__(self, name):
        self.name=name
        self.subClassOf=list()
        self.partOf=""

entityDict = dict()

for e in boneList:
    entityDict[e]=Entity(e)
    entityDict[e].subClassOf.append("Bone")
    entityDict[e].subClassOf.append("Anatomical_entity")
for e in setOfBoneList:
    entityDict[e]=Entity(e)
    entityDict[e].subClassOf.append("Bone")
    entityDict[e].subClassOf.append("Anatomical_entity")
for e in jointList:
    entityDict[e]=Entity(e)
    entityDict[e].subClassOf.append("Joint")
    entityDict[e].subClassOf.append("Anatomical_entity")

for setOfBone in setOfBoneList:
    for bone in myCF.listBonePartOf(setOfBone):
        entityDict[bone].partOf = setOfBone

## FIX ##
for vertebra in myCF.listSubClassOf("Vertebra"):
    entityDict[vertebra].partOf="Vertebral_column"
for rib in myCF.listSubClassOf("Rib"):
    entityDict[rib].partOf="Skeleton_of_rib_cage"
entityDict["Sternum"].partOf="Skeleton_of_rib_cage"
entityDict["Left_ulna"].partOf="Skeleton_of_left_forearm"
entityDict["Left_radius"].partOf="Skeleton_of_left_forearm"
entityDict["Right_ulna"].partOf="Skeleton_of_right_forearm"
entityDict["Right_radius"].partOf="Skeleton_of_right_forearm"
entityDict["Left_hip_bone"].partOf="Pelvic_skeleton"
entityDict["Right_hip_bone"].partOf="Pelvic_skeleton"
entityDict["Left_tibia"].partOf="Skeleton_of_left_leg"
entityDict["Left_fibula"].partOf="Skeleton_of_left_leg"
entityDict["Right_tibia"].partOf="Skeleton_of_right_leg"
entityDict["Right_fibula"].partOf="Skeleton_of_right_leg"
entityDict["Right_knee_joints"].name = "Right_knee_joint"
entityDict["Left_knee_joints"].name = "Left_knee_joint"
entityDict["Left_calcaneus"].partOf="Skeleton_of_left_foot"
entityDict["Right_calcaneus"].partOf="Skeleton_of_right_foot"


def writeCsvLine(csvWriter, e):
    csvWriter.writerow([e.name,"",e.subClassOf[0] if len(e.subClassOf) > 0 else "",e.subClassOf[1] if len(e.subClassOf) > 1 else "",e.partOf])
    if len(e.subClassOf)>2:
            print "[WARNING] ignored extra subClassOf for entity", e.name
    
## write .csv
print "Write data/entity_mycf_doNotEdit.csv..."
with open('../data/entity_mycf_doNotEdit.csv', 'wb') as csvfile:
    csvWriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
    csvWriter.writerow(["name", "description", "subClassOf_01", "subClassOf_02", "partOf"])
    entityList = entityDict.keys()
    entityList.sort()
    for name in entityList:
        if "Skin" in entityDict[name].name: writeCsvLine(csvWriter, entityDict[name])
    for name in entityList:
        if "Bone" in entityDict[name].subClassOf: writeCsvLine(csvWriter, entityDict[name])
    for name in entityList:
        if "Joint" in entityDict[name].subClassOf: writeCsvLine(csvWriter, entityDict[name])
    

        
        
