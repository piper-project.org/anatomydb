# Copyright (C) 2017 INRIA
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 2.1 of the License, or (at your
# option) any later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
# for more details. You should have received a copy of the GNU Lesser General
# Public License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Thomas Lemaire (INRIA)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 
import sys
import urllib, urllib2
from urlparse import urlparse
import json


headers = {}
headers_json = {'Accept': 'application/rdf+json'}
headers_sparql_xml = {'Accept': 'application/sparql-results+xml'}
headers_sparql_json = {'Accept': 'application/sparql-results+json'}

base_url = "https://mybody.inrialpes.fr/openrdf-sesame/"
repository = "mycf"

query_prefix = """PREFIX owl:<http://www.w3.org/2002/07/owl#>
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX mcf:<http://www.mycorporisfabrica.org/ontology/mcf.owl#>
"""

def printAnswer(answer, varNames=["s"]):
    for i in answer["results"]["bindings"]:
        for v in varNames:
            print urlparse(i[v]["value"]).fragment

def answerToList(answer, varNames=["s"]):
    l=list()
    for i in answer["results"]["bindings"]:
        for v in varNames:
            l.append(urlparse(i[v]["value"]).fragment)
    return l

def processQuery(query):
    #print query_prefix+query
    url = base_url + "/repositories/mycf?query=" + urllib.quote(query_prefix+query)
    request = urllib2.Request(url, None, headers_sparql_json)
    answer = json.loads(urllib2.urlopen(request).read())
    return answerToList(answer)

def search(arg):
    return processQuery(
    """
    select distinct ?s ?o
    where {{
    FILTER (contains(str(?s), '{0}')) }}""".format(arg) )

def searchAnatomicalEntity(arg):
    return processQuery(
    """
    select distinct ?s ?o
    where {{
    ?s rdfs:subClassOf mcf:Anatomical_entity .
    FILTER (contains(str(?s), '{0}')) }}""".format(arg) )
    
def searchBoneEntity(arg):
    return processQuery(
    """
    select distinct ?s ?o
    where {{
    {{
        ?s rdfs:subClassOf mcf:Bone .
        FILTER (contains(str(?s), '{0}')) 
    }}
    UNION {{
        ?s rdfs:subClassOf mcf:Set_of_bone_organ .
        FILTER (contains(str(?s), '{0}'))
    }}
    }}
    """.format(arg) )

def listBone():
    return listSubClassOf("Bone")

def listSetOfBone():
    return listSubClassOf("Set_of_bone_organ")
    
def listPartOf(entity):
    return processQuery(
    """
    select distinct ?s ?o
    where {{
    ?s mcf:PartOf mcf:{0} }}
    """.format(entity))

def listSubClassOf(entity):
    return processQuery(
    """
    select distinct ?s ?o
    where {{
    ?s rdfs:subClassOf mcf:{0} }}
    """.format(entity))
    
def listBonePartOf(entity):
    return processQuery(
    """
    select distinct ?s ?o
    where {{
    ?s mcf:PartOf mcf:{0} .
    ?s rdfs:subClassOf mcf:Bone
    }}
    """.format(entity))
    
    
