This folder contains the data included with the release 1.0.0 of the anatomyDB database

Authors: UCBL-Ifsttar, CEESAR, INRIA, U Southampton 

Version: 1.0.0

License: Creative Commons Attribution 4.0 International License.
(https://creativecommons.org/licenses/by/4.0/)

Contributors include Thomas Lemaire (INRIA), Christophe Lecomte (U Southampton), 
Erwan Jolivet (CEESAR), Philippe Beillas (UCBL-Ifsttar)

This work has received funding from the European Union Seventh Framework 
Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).

