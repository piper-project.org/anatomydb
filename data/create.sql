--.trace "create.log"

PRAGMA foreign_keys = ON;

-- create all tables
CREATE TABLE entity (
id INTEGER PRIMARY KEY,
name TEXT NOT NULL UNIQUE,
description TEXT
);

CREATE TABLE partOf (
entity INTEGER NOT NULL,
partOf INTEGER NOT NULL,
FOREIGN KEY(entity) REFERENCES entity(id),
FOREIGN KEY(partOf) REFERENCES entity(id),
PRIMARY KEY(entity, partOf)
);

CREATE TABLE hasSynonym (
entity INTEGER NOT NULL,
synonym INTEGER NOT NULL,
FOREIGN KEY(entity) REFERENCES entity(id),
FOREIGN KEY(synonym) REFERENCES entity(id),
PRIMARY KEY(entity, synonym)
);

CREATE TABLE subClassOf (
entity INTEGER NOT NULL,
subClassOf INTEGER NOT NULL,
FOREIGN KEY(entity) REFERENCES entity(id),
FOREIGN KEY(subClassOf) REFERENCES entity(id),
PRIMARY KEY(entity, subClassOf)
);
CREATE TABLE insertOn (
entity INTEGER NOT NULL,
insertOn INTEGER NOT NULL,
FOREIGN KEY(entity) REFERENCES entity(id),
FOREIGN KEY(insertOn) REFERENCES entity(id),
PRIMARY KEY(entity, insertOn)
);

--CREATE TABLE definedBy (
--entity TEXT NOT NULL,
--definedBy TEXT NOT NULL,
--FOREIGN KEY(entity) REFERENCES entity(name),
--FOREIGN KEY(definedBy) REFERENCES entity(name),
--PRIMARY KEY(entity, definedBy)
--);

CREATE TABLE fromBibliography (
entity INTEGER NOT NULL,
fromBibliography INTEGER NOT NULL,
FOREIGN KEY(entity) REFERENCES entity(id),
FOREIGN KEY(fromBibliography) REFERENCES entity(id),
PRIMARY KEY(entity, fromBibliography)
);

.mode csv
.bail ON

-- populate database - class.csv
.import class.csv class_tmp
INSERT INTO entity(name,description) SELECT name,description FROM class_tmp;
DROP TABLE class_tmp;

-- populate database - bibliography.csv
.import bibliography.csv bibliography_tmp
INSERT INTO entity(name,description) SELECT name,description FROM bibliography_tmp;
INSERT INTO subClassOf(entity,subClassOf)
    SELECT id,(SELECT id FROM entity WHERE name='Bibliography')
    FROM entity,bibliography_tmp WHERE NOT bibliography_tmp.name="" AND entity.name=bibliography_tmp.name;
DROP TABLE bibliography_tmp;

-- populate database - entity_mycf_doNotEdit.csv
.import entity_mycf_doNotEdit.csv entity_mycf
INSERT INTO entity(name,description) SELECT name,description FROM entity_mycf;
INSERT INTO subClassOf(entity,subClassOf)
    SELECT entity.id,subClassOf.id FROM entity_mycf, entity, entity AS subClassOf
    WHERE NOT entity_mycf.subClassOf_01=""
        AND entity_mycf.name=entity.name AND entity_mycf.subClassOf_01=subClassOf.name;
INSERT INTO subClassOf(entity,subClassOf)
    SELECT entity.id,subClassOf.id FROM entity_mycf, entity, entity AS subClassOf
    WHERE NOT entity_mycf.subClassOf_02=""
        AND entity_mycf.name=entity.name AND entity_mycf.subClassOf_02=subClassOf.name;
INSERT INTO partOf(entity,partOf) SELECT entity.id,partOf.id FROM entity_mycf, entity, entity AS partOf
    WHERE NOT partOf=""
        AND entity_mycf.name=entity.name AND entity_mycf.partOf=partOf.name;
INSERT INTO fromBibliography(entity, fromBibliography) SELECT entity.id,(SELECT id FROM entity WHERE name='FMA')
    FROM entity_mycf, entity WHERE entity_mycf.name=entity.name;
DROP TABLE entity_mycf;



-- populate database - entity.csv
.import entity_user.csv entity_user
-- entity
INSERT INTO entity(name,description) SELECT name,description FROM entity_user;
INSERT INTO fromBibliography(entity,fromBibliography) SELECT entity.id, bibliography.id
    FROM entity_user, entity, entity AS bibliography
    WHERE NOT entity_user.name="" AND NOT entity_user.bibliography_00=""
        AND entity.name=entity_user.name AND bibliography.name=entity_user.bibliography_00;
-- synonym_01
INSERT INTO entity(name) SELECT synonym_01 FROM entity_user
    WHERE NOT entity_user.name="" AND NOT synonym_01="";
INSERT INTO subClassOf(entity,subClassOf) SELECT id,(SELECT id FROM entity WHERE name='Synonym')
    FROM entity_user,entity
    WHERE NOT entity_user.name="" AND NOT synonym_01="" AND synonym_01=entity.name;
INSERT INTO hasSynonym(entity,synonym) SELECT entity.id,synonym.id FROM entity_user, entity, entity AS synonym
    WHERE NOT synonym_01="" AND entity_user.name=entity.name AND synonym_01=synonym.name;
INSERT INTO fromBibliography(entity,fromBibliography) SELECT entity.id, bibliography.id
    FROM entity_user, entity, entity AS bibliography
    WHERE NOT entity_user.name="" AND NOT entity_user.synonym_01="" AND NOT entity_user.bibliography_01=""
        AND entity.name=entity_user.synonym_01 AND bibliography.name=entity_user.bibliography_01;
-- subClassOf, partOf
INSERT INTO subClassOf(entity,subClassOf)
    SELECT entity.id,subClassOf.id FROM entity_user, entity, entity AS subClassOf
    WHERE NOT entity_user.subClassOf_01=""
        AND entity_user.name=entity.name AND entity_user.subClassOf_01=subClassOf.name;
INSERT INTO subClassOf(entity,subClassOf)
    SELECT entity.id,subClassOf.id FROM entity_user, entity, entity AS subClassOf
    WHERE NOT entity_user.subClassOf_02=""
        AND entity_user.name=entity.name AND entity_user.subClassOf_02=subClassOf.name;
INSERT INTO partOf(entity,partOf) SELECT entity.id,partOf.id FROM entity_user, entity, entity AS partOf
    WHERE NOT partOf=""
        AND entity_user.name=entity.name AND entity_user.partOf=partOf.name;
-- insertOn
INSERT INTO insertOn(entity,insertOn)
    SELECT entity.id,insertOn.id FROM entity_user, entity, entity AS insertOn
    WHERE NOT entity_user.insertOn_01=""
        AND entity_user.name=entity.name AND entity_user.insertOn_01=insertOn.name;
INSERT INTO insertOn(entity,insertOn)
    SELECT entity.id,insertOn.id FROM entity_user, entity, entity AS insertOn
    WHERE NOT entity_user.insertOn_02=""
        AND entity_user.name=entity.name AND entity_user.insertOn_02=insertOn.name;
INSERT INTO insertOn(entity,insertOn)
    SELECT entity.id,insertOn.id FROM entity_user, entity, entity AS insertOn
    WHERE NOT entity_user.insertOn_03=""
        AND entity_user.name=entity.name AND entity_user.insertOn_03=insertOn.name;
DROP TABLE entity_user;

-- populate database - regions.csv  -- Addition of the anatomical regions
.import regions.csv regions
INSERT INTO entity(name,description) SELECT name,description FROM regions;
INSERT INTO subClassOf(entity,subClassOf) SELECT entity.id,(SELECT id FROM entity WHERE name='Region')
    FROM regions,entity
    WHERE NOT regions.name="" AND regions.name=entity.name;
INSERT INTO subClassOf(entity,subClassOf) SELECT entity.id,(SELECT id FROM entity WHERE name='Anatomical_entity')
    FROM regions,entity
    WHERE NOT regions.name="" AND regions.name=entity.name;
INSERT INTO partOf(entity,partOf) SELECT entity.id,partOf.id FROM regions, entity, entity AS partOf
    WHERE NOT partOf=""
        AND regions.name=entity.name AND regions.partOf=partOf.name;
DROP TABLE regions;

-- populate database - synonym.csv
.import synonym.csv synonym_tmp
INSERT INTO entity(name) SELECT synonym FROM synonym_tmp
    WHERE NOT synonym_tmp.synonym="";
INSERT INTO subClassOf(entity,subClassOf) SELECT id,(SELECT id FROM entity WHERE name='Synonym')
    FROM synonym_tmp,entity
    WHERE NOT synonym_tmp.synonym="" AND synonym_tmp.synonym=entity.name;
INSERT INTO hasSynonym(entity,synonym) SELECT entity.id,synonym.id FROM synonym_tmp, entity, entity AS synonym
    WHERE NOT synonym_tmp.synonym="" AND synonym_tmp.entity=entity.name AND synonym_tmp.synonym=synonym.name;
DROP TABLE synonym_tmp;

-- populate database - landmark.csv
.import landmark.csv landmark_tmp
-- landmark
INSERT INTO entity(name,description) SELECT name,description FROM landmark_tmp WHERE NOT name="";
INSERT INTO subClassOf(entity,subClassOf) SELECT id,(SELECT id FROM entity WHERE name='Landmark')
    FROM landmark_tmp,entity
    WHERE NOT landmark_tmp.name="" AND landmark_tmp.name=entity.name;
INSERT INTO fromBibliography(entity,fromBibliography) SELECT entity.id, bibliography.id
    FROM landmark_tmp, entity, entity AS bibliography
    WHERE NOT landmark_tmp.name="" AND NOT landmark_tmp.bibliography_00=""
        AND entity.name=landmark_tmp.name AND bibliography.name=landmark_tmp.bibliography_00;
-- synonym_01
INSERT INTO entity(name) SELECT synonym_01 FROM landmark_tmp
    WHERE NOT landmark_tmp.name="" AND NOT synonym_01="";
INSERT INTO subClassOf(entity,subClassOf) SELECT id,(SELECT id FROM entity WHERE name='Synonym')
    FROM landmark_tmp,entity
    WHERE NOT landmark_tmp.name="" AND NOT synonym_01="" AND synonym_01=entity.name;
INSERT INTO hasSynonym(entity,synonym) SELECT entity.id,synonym.id FROM landmark_tmp, entity, entity AS synonym
    WHERE NOT synonym_01="" AND landmark_tmp.name=entity.name AND synonym_01=synonym.name;
INSERT INTO fromBibliography(entity,fromBibliography) SELECT entity.id, bibliography.id
    FROM landmark_tmp, entity, entity AS bibliography
    WHERE NOT landmark_tmp.name="" AND NOT landmark_tmp.synonym_01="" AND NOT landmark_tmp.bibliography_01=""
        AND entity.name=landmark_tmp.synonym_01 AND bibliography.name=landmark_tmp.bibliography_01;
-- synonym_02
INSERT INTO entity(name) SELECT synonym_02 FROM landmark_tmp
    WHERE NOT landmark_tmp.name="" AND NOT synonym_02="";
INSERT INTO subClassOf(entity,subClassOf) SELECT id,(SELECT id FROM entity WHERE name='Synonym')
    FROM landmark_tmp,entity
    WHERE NOT landmark_tmp.name="" AND NOT synonym_02="" AND synonym_02=entity.name;
INSERT INTO hasSynonym(entity,synonym) SELECT entity.id,synonym.id FROM landmark_tmp, entity, entity AS synonym
    WHERE NOT landmark_tmp.name="" AND NOT synonym_02="" AND landmark_tmp.name=entity.name AND synonym_02=synonym.name;
-- todo: biblio
-- synonym_03
INSERT INTO entity(name) SELECT synonym_03 FROM landmark_tmp
    WHERE NOT landmark_tmp.name="" AND NOT synonym_03="";
INSERT INTO subClassOf(entity,subClassOf) SELECT id,(SELECT id FROM entity WHERE name='Synonym')
    FROM landmark_tmp,entity
    WHERE NOT landmark_tmp.name="" AND NOT synonym_03="" AND synonym_03=entity.name;
INSERT INTO hasSynonym(entity,synonym) SELECT entity.id,synonym.id FROM landmark_tmp, entity, entity AS synonym
    WHERE NOT landmark_tmp.name="" AND NOT synonym_03="" AND landmark_tmp.name=entity.name AND synonym_03=synonym.name;
-- todo: biblio
-- synonym_04
INSERT INTO entity(name) SELECT synonym_04 FROM landmark_tmp
    WHERE NOT landmark_tmp.name="" AND NOT synonym_04="";
INSERT INTO subClassOf(entity,subClassOf) SELECT id,(SELECT id FROM entity WHERE name='Synonym')
    FROM landmark_tmp,entity
    WHERE NOT landmark_tmp.name="" AND NOT synonym_04="" AND synonym_04=entity.name;
INSERT INTO hasSynonym(entity,synonym) SELECT entity.id,synonym.id FROM landmark_tmp, entity, entity AS synonym
    WHERE NOT landmark_tmp.name="" AND NOT synonym_04="" AND landmark_tmp.name=entity.name AND synonym_04=synonym.name;
-- todo: biblio
-- partOf
INSERT INTO partOf(entity,partOf) SELECT entity.id,partOf.id FROM landmark_tmp,entity,entity AS partOf
    WHERE NOT landmark_tmp.name="" AND NOT landmark_tmp.partOf=""
        AND landmark_tmp.name=entity.name
        AND landmark_tmp.partOf=partOf.name;
DROP TABLE landmark_tmp;

-- populate database - frames.csv
.import frames.csv frames_tmp
INSERT INTO entity(name,description) SELECT name,description FROM frames_tmp WHERE NOT name="";
INSERT INTO subClassOf(entity,subClassOf) SELECT id,(SELECT id FROM entity WHERE name='Frame')
    FROM frames_tmp,entity
    WHERE NOT frames_tmp.name="" AND frames_tmp.name=entity.name;
INSERT INTO partOf(entity,partOf)  SELECT entity.id,partOf.id FROM frames_tmp,entity,entity AS partOf
WHERE NOT frames_tmp.name="" AND NOT frames_tmp.partOf=""
    AND frames_tmp.name=entity.name
    AND frames_tmp.partOf=partOf.name;
INSERT INTO fromBibliography(entity,fromBibliography) SELECT entity.id, bibliography.id
    FROM frames_tmp, entity, entity AS bibliography
    WHERE NOT frames_tmp.name="" AND NOT frames_tmp.bibliography_00=""
        AND entity.name=frames_tmp.name AND bibliography.name=frames_tmp.bibliography_00;
-- synonym_01
INSERT INTO entity(name) SELECT synonym_01 FROM frames_tmp
    WHERE NOT frames_tmp.name="" AND NOT synonym_01="";
INSERT INTO subClassOf(entity,subClassOf) SELECT id,(SELECT id FROM entity WHERE name='Synonym')
    FROM frames_tmp,entity
    WHERE NOT frames_tmp.name="" AND NOT synonym_01="" AND synonym_01=entity.name;
INSERT INTO hasSynonym(entity,synonym) SELECT entity.id,synonym.id FROM frames_tmp, entity, entity AS synonym
    WHERE NOT frames_tmp.name="" AND NOT synonym_01="" AND frames_tmp.name=entity.name AND synonym_01=synonym.name;
INSERT INTO fromBibliography(entity,fromBibliography) SELECT entity.id, bibliography.id
    FROM frames_tmp, entity, entity AS bibliography
    WHERE NOT frames_tmp.name="" AND NOT frames_tmp.synonym_01="" AND NOT frames_tmp.bibliography_01=""
        AND entity.name=frames_tmp.synonym_01 AND bibliography.name=frames_tmp.bibliography_01;
-- synonym_02
INSERT INTO entity(name) SELECT synonym_02 FROM frames_tmp
    WHERE NOT frames_tmp.name="" AND NOT synonym_02="";
INSERT INTO subClassOf(entity,subClassOf) SELECT id,(SELECT id FROM entity WHERE name='Synonym')
    FROM frames_tmp,entity
    WHERE NOT frames_tmp.name="" AND NOT synonym_02="" AND synonym_02=entity.name;
INSERT INTO hasSynonym(entity,synonym) SELECT entity.id,synonym.id FROM frames_tmp, entity, entity AS synonym
    WHERE NOT frames_tmp.name="" AND NOT synonym_02="" AND frames_tmp.name=entity.name AND synonym_02=synonym.name;

--INSERT INTO fromBibliography(entity, fromBibliography) SELECT name, bibliography_00 FROM frames_tmp WHERE NOT name="" AND NOT bibliography_00="";
--INSERT INTO fromBibliography(entity, fromBibliography) SELECT name, bibliography_01 FROM frames_tmp WHERE NOT name="" AND NOT bibliography_01="";
--INSERT INTO definedBy(entity,definedBy) SELECT name, definedBy_01 FROM frames_tmp WHERE NOT name="" AND NOT definedBy_01="";
--INSERT INTO definedBy(entity,definedBy) SELECT name, definedBy_02 FROM frames_tmp WHERE NOT name="" AND NOT definedBy_02="";
--INSERT INTO definedBy(entity,definedBy) SELECT name, definedBy_03 FROM frames_tmp WHERE NOT name="" AND NOT definedBy_03="";
--INSERT INTO definedBy(entity,definedBy) SELECT name, definedBy_04 FROM frames_tmp WHERE NOT name="" AND NOT definedBy_04="";
--INSERT INTO definedBy(entity,definedBy) SELECT name, definedBy_05 FROM frames_tmp WHERE NOT name="" AND NOT definedBy_05="";
DROP TABLE frames_tmp;

-- populate database - joint_extra_info.csv
.import joint_extra_info.csv joint_extra_info_tmp
-- joint
INSERT INTO partOf(entity,partof) SELECT entity.id, joint.id
    FROM joint_extra_info_tmp, entity, entity AS joint
    WHERE NOT entityPartOfJoint_01=""
        AND entityPartOfJoint_01=entity.name AND joint_extra_info_tmp.joint=joint.name;
INSERT INTO partOf(entity,partof) SELECT entity.id, joint.id
    FROM joint_extra_info_tmp, entity, entity AS joint
    WHERE NOT entityPartOfJoint_02=""
        AND entityPartOfJoint_02=entity.name AND joint_extra_info_tmp.joint=joint.name;
INSERT INTO partOf(entity,partof) SELECT entity.id, joint.id
    FROM joint_extra_info_tmp, entity, entity AS joint
    WHERE NOT entityPartOfJoint_03=""
        AND entityPartOfJoint_03=entity.name AND joint_extra_info_tmp.joint=joint.name;
INSERT INTO partOf(entity,partof) SELECT frame.id,joint.id
    FROM joint_extra_info_tmp, entity AS frame, entity AS joint
    WHERE NOT framePartOfJoint_01=""
        AND framePartOfJoint_01=frame.name AND joint_extra_info_tmp.joint=joint.name;
INSERT INTO partOf(entity,partof) SELECT frame.id,joint.id
    FROM joint_extra_info_tmp, entity AS frame, entity AS joint
    WHERE NOT framePartOfJoint_02=""
        AND framePartOfJoint_02=frame.name AND joint_extra_info_tmp.joint=joint.name;
INSERT INTO fromBibliography(entity,fromBibliography) SELECT joint.id, bibliography.id
    FROM joint_extra_info_tmp,entity AS joint, entity AS bibliography
    WHERE NOT joint_extra_info_tmp.joint="" AND NOT bibliography_00=""
        AND joint_extra_info_tmp.joint=joint.name AND bibliography.name=bibliography_00;
-- frame 01
INSERT INTO entity(name,description) SELECT framePartOfJoint_01,frameDescription_01 FROM joint_extra_info_tmp WHERE NOT framePartOfJoint_01="";
INSERT INTO subClassOf(entity,subClassOf) SELECT id,(SELECT id FROM entity WHERE name='Frame')
    FROM joint_extra_info_tmp,entity
    WHERE NOT framePartOfJoint_01=""
        AND framePartOfJoint_01=entity.name;
INSERT INTO partOf(entity,partOf)  SELECT frame.id,joint.id
    FROM joint_extra_info_tmp, entity AS frame, entity AS joint
    WHERE NOT framePartOfJoint_01=""
        AND frame.name=framePartOfJoint_01 AND joint.name=joint_extra_info_tmp.joint;
INSERT INTO partOf(entity,partOf)  SELECT frame.id,bone.id
    FROM joint_extra_info_tmp, entity AS frame, entity AS bone
    WHERE NOT framePartOfJoint_01="" AND NOT entityPartOfJoint_01=""
        AND frame.name=framePartOfJoint_01 AND bone.name=entityPartOfJoint_01;
INSERT INTO fromBibliography(entity, fromBibliography) SELECT frame.id, bibliography.id
    FROM joint_extra_info_tmp, entity AS frame, entity AS bibliography
    WHERE NOT framePartOfJoint_01="" AND NOT bibliography_00=""
        AND frame.name=framePartOfJoint_01 AND bibliography.name=bibliography_00;
-- frame 02
INSERT INTO entity(name,description) SELECT framePartOfJoint_02,frameDescription_02 FROM joint_extra_info_tmp WHERE NOT framePartOfJoint_02="";
INSERT INTO subClassOf(entity,subClassOf) SELECT id,(SELECT id FROM entity WHERE name='Frame')
    FROM joint_extra_info_tmp,entity
    WHERE NOT framePartOfJoint_02=""
        AND framePartOfJoint_02=entity.name;
INSERT INTO partOf(entity,partOf)  SELECT frame.id,joint.id
    FROM joint_extra_info_tmp, entity AS frame, entity AS joint
    WHERE NOT framePartOfJoint_02=""
        AND frame.name=framePartOfJoint_02 AND joint.name=joint_extra_info_tmp.joint;
INSERT INTO partOf(entity,partOf)  SELECT frame.id,bone.id
    FROM joint_extra_info_tmp, entity AS frame, entity AS bone
    WHERE NOT framePartOfJoint_02="" AND NOT entityPartOfJoint_02=""
        AND frame.name=framePartOfJoint_02 AND bone.name=entityPartOfJoint_02;
INSERT INTO fromBibliography(entity, fromBibliography) SELECT frame.id, bibliography.id
    FROM joint_extra_info_tmp, entity AS frame, entity AS bibliography
    WHERE NOT framePartOfJoint_02="" AND NOT bibliography_00=""
        AND frame.name=framePartOfJoint_02 AND bibliography.name=bibliography_00;
-- Need for frame synonyms ?

DROP TABLE joint_extra_info_tmp;

-- populate database - entity_partOf_region.csv -- add the region relations
.import entity_partOf_region.csv entity_partOf_region_tmp
INSERT INTO partOf(entity,partOf) SELECT entity.id,partOf.id FROM entity_partOf_region_tmp, entity, entity AS partOf
    WHERE entity_partOf_region_tmp.entity = entity.name AND entity_partOf_region_tmp.partOf = partOf.name;
DROP TABLE entity_partOf_region_tmp;

-- populate database - skin.csv
.import skin.csv skin_tmp
INSERT INTO entity(name,description) SELECT name,description FROM skin_tmp WHERE NOT name="";
INSERT INTO subClassOf(entity,subClassOf) SELECT entity.id,(SELECT id FROM entity WHERE name='Skin')
    FROM skin_tmp,entity
    WHERE NOT skin_tmp.name="" AND skin_tmp.name=entity.name;
INSERT INTO subClassOf(entity,subClassOf) SELECT entity.id,(SELECT id FROM entity WHERE name='Anatomical_entity')
    FROM skin_tmp,entity
    WHERE NOT skin_tmp.name="" AND skin_tmp.name=entity.name;
-- partOf skin entities
INSERT INTO partOf(entity,partOf) SELECT entity.id,partOf.id FROM skin_tmp, entity, entity AS partOf
    WHERE NOT skin_tmp.name="" AND NOT skin_tmp.partOf=""
        AND skin_tmp.name=entity.name AND skin_tmp.partOf=partOf.name;
-- partOf region
INSERT INTO partOf(entity,partOf) SELECT entity.id,partOf.id FROM skin_tmp, entity, entity AS partOf
    WHERE NOT skin_tmp.name="" AND NOT skin_tmp.region_partOf=""
        AND skin_tmp.name=entity.name AND skin_tmp.region_partOf=partOf.name;
DROP TABLE skin_tmp;



.tables
.schema
