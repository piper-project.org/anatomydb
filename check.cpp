/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "check.h"
#include "query.h"

#include <iostream>

#include <QVariant>
#include <QSqlQuery>

namespace anatomydb {

bool checkSynonym()
{
    QSqlQuery query;
    query.prepare("SELECT DISTINCT entity.name FROM subClassOf,entity,hasSynonym "
                  "WHERE subClassOf.subClassOf = entity.id AND subClassOf.entity = hasSynonym.synonym;");
    query.exec();

    if (query.next()) //If there is one class
        if( query.value(0).toString().toStdString() == "Synonym")
            if (!query.next()) //And there is no more classes
                return true;
    do {
        std::cerr << "class: " << query.value(0).toString().toStdString() << std::endl;
    } while (query.next());
    return false;
}

bool checkSynonymUnicity()
{
    bool ret = true;

    QSqlQuery query;
    query.prepare("SELECT entity.name,COUNT(hasSynonym.synonym) FROM entity,hasSynonym WHERE entity.id = hasSynonym.synonym GROUP BY hasSynonym.synonym");
    query.exec();

    while(query.next()){
        if(query.value(1).toInt() > 1){
            std::cerr << "Synonym " << query.value(0).toString().toStdString() << " has " << query.value(1).toString().toStdString() << " matching entities. \n";
            ret = false;
        }
    }
    return ret;
}

bool checkBibliography()
{
    QSqlQuery query;
    query.prepare("SELECT DISTINCT entity.name FROM subClassOf,entity,fromBibliography "
                  "WHERE subClassOf.subClassOf = entity.id AND subClassOf.entity = fromBibliography.fromBibliography;");
    query.exec();

    if (query.next()) //If there is one class
        if( query.value(0).toString().toStdString() == "Bibliography")
            if (!query.next()) //And there is no more classes
                return true;

    std::cerr << "The bibliographies are of classes ";
    do {
         std::cerr  << query.value(0).toString().toStdString() << ", ";
    } while (query.next());
    std::cerr << "\n";

    return false;
}

bool checkTreePartOf()
{
    //Get all the classes, except Anatomical_entity
    std::vector<std::string> classesToTest;
    QSqlQuery queryClass;
    queryClass.prepare("SELECT DISTINCT entity.name FROM entity,subClassOf WHERE subClassOf.subClassOf=entity.id");
    queryClass.exec();

    while (queryClass.next())
        if(queryClass.value(0).toString().toStdString() != "Anatomical_entity")     //Fails the test (contains classes)
            classesToTest.push_back(queryClass.value(0).toString().toStdString());

    for(std::size_t i = 0; i < classesToTest.size(); i++){

        //get all the elements of current class to test
        std::string currentClass = classesToTest.at(i);
        std::vector<std::string> entityList = getSubClassOfList(currentClass);

        //test if each element has no more than 1 parent(it can be 0)
        for(std::size_t j = 0; j < entityList.size(); j++){
            //Get the current id parents of class currentClass
            std::string currentEntity = entityList.at(j);
            std::vector<std::string> parentList = getPartOfSubClassList(currentEntity,currentClass);

            //If the results are more than 1, error
            if (parentList.size() > 1){
                std::cerr << "The class " << currentClass << " doesn't describes a tree. Ex : the entity "
                          << currentEntity << " has " << parentList.size() << " parents. \n";
                return false;
            }
        }
    }
    return true;
}

}
