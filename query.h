/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_ANATOMYDB_QUERY_H
#define PIPER_ANATOMYDB_QUERY_H

#ifdef WIN32
#	ifdef anatomyDB_EXPORTS
#		define ANATOMYDB_EXPORT __declspec( dllexport )
#	else
#		define ANATOMYDB_EXPORT __declspec( dllimport )
#	endif
#else
#	define ANATOMYDB_EXPORT
#endif

#include <string>
#include <vector>

namespace anatomydb {

/*! \addtogroup anatomyDB Anatomy Data Base
 * \note
 * The queries look for entity name in synonyms, but the result contains only reference name(s).
 * This is the responsability of the user to get the synonyms of the result if needed using getSynonymList(std::string const& name).
 * \todo add an option to make the entity names in request case insensitive ?
 * \todo make it possible to have several entities, of different class, have the same reference name (class needs to be added to some queries)
 * @{
 */

/** Initialize the database.
 * @param rootDir optionnal dir where the sqlite3 database file is located (used for testing and bindings)
 */
ANATOMYDB_EXPORT void init(std::string const& rootDir = "");

/** @return the unique id of entity \a name
* \throw runtime_error on unknown entity
*/
ANATOMYDB_EXPORT unsigned int getEntityId(std::string const& name);

/// @return true if \a name exists in the database (either as a reference name or a synonym)
ANATOMYDB_EXPORT bool exists(std::string const& name);

/** @return get the reference name for \a name
 * \throw runtime_error on unknown name
 */
ANATOMYDB_EXPORT std::string getReferenceName(std::string const& name);

/** @return true if \a name1 and \a name2 are synonyms
 * \throw runtime_error on unknown name
 */
ANATOMYDB_EXPORT bool isSynonymOf(std::string const& name1, std::string const& name2);

/// @return get the reference name for \a name, return an empty string on failure
ANATOMYDB_EXPORT std::string getReferenceNameNoThrow(std::string const& name);

/// @return reference name and synonyms for \a name, \a name is searched in synonyms too, reference name is returned first and is omitted when 
ANATOMYDB_EXPORT std::vector<std::string> getSynonymList(std::string const& name, bool omitReferenceName=false);

/// @return synonym for \a name given by bibliography \a fromBibliography
ANATOMYDB_EXPORT std::string getSynonymFromBibliography(std::string const& name, std::string const& fromBibliography);

/// @returns description for \a name
ANATOMYDB_EXPORT std::string getEntityDescription(std::string const& name);

/// Is \a name subclass of \a className ?
ANATOMYDB_EXPORT bool isEntitySubClassOf(std::string const& name, std::string const& className);

/// Is \a name a synonym ?
ANATOMYDB_EXPORT bool isSynonym(std::string const& name);

/// Is \a name an anatomical entity ?
ANATOMYDB_EXPORT bool isAnatomicalEntity(std::string const& name);

/// Is \a name a bone ?
ANATOMYDB_EXPORT bool isBone(std::string const& name);

/// Is \a name skin ?
ANATOMYDB_EXPORT bool isSkin(std::string const& name);

/// Is \a name a landmark ?
ANATOMYDB_EXPORT bool isLandmark(std::string const& name);

/// @return list parent classes of \a name
ANATOMYDB_EXPORT std::vector<std::string> getParentClassList(std::string const& name);

/// @return entities sub class of \a className
ANATOMYDB_EXPORT std::vector<std::string> getSubClassOfList(std::string const& className);

/// @return true if \a entity is partOf \a parent, will search recursively if \param recursive is true
ANATOMYDB_EXPORT bool isEntityPartOf(std::string const& entity, std::string const& parent, bool recursive=false);

/** @return entities which \a name is part of
 * \sa getPartOfList(std::string const& name, std::string const& className)
 */
ANATOMYDB_EXPORT std::vector<std::string> getPartOfList(std::string const& name);

/** @return entities which \a name is part of and are of class \a className, when \a recursive is true, all results are in the output list, starting from the "partOf" closest enities.
 */
ANATOMYDB_EXPORT std::vector<std::string> getPartOfSubClassList(std::string const& name, std::string const& className, bool recursive=false);

/// @return list of entities sub part of \a name and are of class \a className
ANATOMYDB_EXPORT std::vector<std::string> getSubPartOfList(std::string const& name, std::string const& className, bool recursive=false);

/// @return bibliography entries for entity \a name
ANATOMYDB_EXPORT std::vector<std::string> getEntityBibliographyList(std::string const& name);

/// @return true if \a entity is from \a bibliography
ANATOMYDB_EXPORT bool isEntityFromBibliography(std::string const& entity, std::string const& bibliography);

/// @return the list of entities to which landmark \a name belongs (partOf relation)
ANATOMYDB_EXPORT std::vector<std::string> getLandmarkBoneList(std::string const& name);

/// @return the list of entities on which \a name inserts, only entities of class \a className are returned, and each entity ascendants (partOf relation) are returned
ANATOMYDB_EXPORT std::vector< std::vector<std::string> > getInsertOnList(std::string const& name, std::string const& className);

/** get entities that are subClass of \a className
 * result is output in \a entityList
 */
ANATOMYDB_EXPORT void getSubClassOfList(std::string const& className, std::vector<std::string> &entityList);

/// get all anatomical entities in \a entityList
ANATOMYDB_EXPORT void getAnatomicalEntityList(std::vector<std::string> &entityList);

/// get all joints in \a jointList
ANATOMYDB_EXPORT void getJointList(std::vector<std::string> &jointList);

/// get all landmarks in \a landmarkList
ANATOMYDB_EXPORT void getLandmarkList(std::vector<std::string> &landmarkList);

/** get entities that are subClass of \a className and from \a bibliography
 * \todo subClassOf relation could be defined between classes, either update subClass queries or implement a database "saturation" to add implicit relations
 */
ANATOMYDB_EXPORT std::vector<std::string> getSubClassOfFromBibliographyList(std::string const& className, std::string const& bibliography);

/** get all the ancestors of an element of class \a className
 * @return all the element's parents/ancestors, ordering them by distance, from closer to farther
 */
ANATOMYDB_EXPORT std::vector<std::string> getAllParents(std::string const& element, std::string const& className);

/** get all the names that matches the \a word, with an option to take into account the synonyms
 */
ANATOMYDB_EXPORT std::vector<std::string> search(std::string const& word, const std::string &className, bool doSynonyms = false);

/*! @} */

} // anatomydb

#endif // PIPER_ANATOMYDB_QUERY_H
