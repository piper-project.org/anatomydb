/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "query.h"

#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <vector>
#include <list>


#include <QDebug>
#include <QFileInfo>
#include <QDir>
#include <QCoreApplication>
#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>

namespace anatomydb {

void init(std::string const& rootDir)
{
	if (QCoreApplication::instance()==nullptr)
		if (rootDir.empty())
			throw std::runtime_error("anatomyDB: anatomyDB installation root directory is required");
		else
			QCoreApplication::addLibraryPath(QString::fromStdString(rootDir));
		
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");

    QString _databaseFile;
    if (rootDir.empty())
        if (QCoreApplication::instance()!=nullptr)
            _databaseFile = QDir(QCoreApplication::applicationDirPath()).filePath("anatomyDB.sqlite");
        else
            _databaseFile = "anatomyDB.sqlite";
    else
        _databaseFile = QDir(QString::fromStdString(rootDir)).filePath("anatomyDB.sqlite");

    if (!QFileInfo().exists(_databaseFile))
        throw std::runtime_error("AnatomyDB: database not found - " + _databaseFile.toStdString());

    db.setDatabaseName(_databaseFile);
    if (!db.open())
        throw std::runtime_error(db.lastError().text().toStdString());
}

unsigned int getEntityId(std::string const& name)
{
    QSqlQuery query;

    // look if name exists
    query.prepare("SELECT id FROM entity WHERE entity.name=:name");
    query.bindValue(":name", QString::fromStdString(name));
    query.exec();
    if (!query.next())
        throw std::runtime_error("anatomyDB: unknown name "+name);

    unsigned int id = query.value(0).toUInt();

    if (query.next())
        throw std::runtime_error("anatomyDB: several entities named "+name);

    // is it a synonym ?
    query.prepare("SELECT * FROM subClassOf WHERE entity=:id AND subClassOf=(SELECT id FROM entity WHERE name=\"Synonym\")");
    query.bindValue(":id", id);
    query.exec();
    if (!query.next())
        return id;

    // If it is a synonym, look for the entity id
    query.prepare("SELECT hasSynonym.entity FROM hasSynonym WHERE hasSynonym.synonym=:id");
    query.bindValue(":id", id);
    query.exec();
    if (query.next())
        id = query.value(0).toUInt();
    else
        throw std::runtime_error("anatomyDB: internal error looking for "+name);
    if (query.next())
        throw std::runtime_error("anatomyDB: several entities named "+name);
    return id;
}

bool exists(std::string const& name)
{
    QSqlQuery query;
    query.prepare("SELECT name FROM entity WHERE entity.name=:name");
    query.bindValue(":name", QString::fromStdString(name));
    query.exec();
    return query.next();
}

std::string getReferenceName(std::string const& name)
{
    QSqlQuery query;
    // is name already an entity name ?
    if (isSynonym(name)) {
        query.prepare("SELECT entity.name FROM entity, hasSynonym, entity AS synonym "
                      "WHERE entity.id=hasSynonym.entity AND synonym.id=hasSynonym.synonym AND synonym.name=:name");
        query.bindValue(":name", QString::fromStdString(name));
        query.exec();
        if (query.next()) {
            std::string res = query.value(0).toString().toStdString();
            if (query.next())
                throw std::runtime_error("anatomyDB: several entities named "+name);
            return res;
        }
    }
    else {
        query.prepare("SELECT name FROM entity WHERE entity.name=:name");
        query.bindValue(":name", QString::fromStdString(name));
        query.exec();
        if (query.next()) {
            if (query.next())
                throw std::runtime_error("anatomyDB: several entities named "+name);
            return name;
        }
    }
    throw std::runtime_error("anatomyDB: unknown name "+name);
}

bool isSynonymOf(std::string const& name1, std::string const& name2)
{
    return getEntityId(name1) == getEntityId(name2);
}

std::string getReferenceNameNoThrow(std::string const& name)
{
    try {
        return getReferenceName(name);
    }
    catch(...) {
        return "";
    }
}

std::vector<std::string> getSynonymList(std::string const& name, bool omitReferenceName)
{
    std::vector<std::string> synonymList;
    if (!omitReferenceName)
        synonymList.push_back(getReferenceName(name));

    QSqlQuery query;
    query.prepare("SELECT synonym.name FROM hasSynonym, entity AS synonym WHERE hasSynonym.synonym=synonym.id AND hasSynonym.entity=:id");
    query.bindValue(":id", getEntityId(name));
    query.exec();
    while (query.next())
        synonymList.push_back(query.value(0).toString().toStdString());

    return synonymList;
}

std::string getSynonymFromBibliography(std::string const& name, std::string const& fromBibliography)
{
    QSqlQuery query;

    unsigned int entityId = getEntityId(name);
    unsigned int bibliographyId = getEntityId(fromBibliography);

    // try if the reference entity is from bibliography
    query.prepare("SELECT synonym.name FROM entity AS synonym, fromBibliography "
                  "WHERE synonym.id = :entityId "
                      "AND synonym.id=fromBibliography.entity "
                      "AND fromBibliography.fromBibliography=:fromBibliographyId");
    query.bindValue(":entityId", entityId);
    query.bindValue(":fromBibliographyId", bibliographyId);
    query.exec();
    if (query.next())
        return query.value(0).toString().toStdString();

    // if not look for correct bibliography in synonyms
    query.prepare("SELECT synonym.name FROM entity AS synonym, hasSynonym, fromBibliography "
                  "WHERE synonym.id = hasSynonym.synonym "
                      "AND synonym.id=fromBibliography.entity "
                      "AND hasSynonym.entity=:entityId "
                      "AND hasSynonym.synonym = fromBibliography.entity "
                      "AND fromBibliography.fromBibliography=:fromBibliographyId");
    query.bindValue(":entityId", entityId);
    query.bindValue(":fromBibliographyId", bibliographyId);
    query.exec();
    if (query.next())
        return query.value(0).toString().toStdString();
    return "";
}

std::string getEntityDescription(std::string const& name)
{
    QSqlQuery query;
    query.prepare("SELECT entity.description FROM entity WHERE entity.id=:id");
    query.bindValue(":id", getEntityId(name));
    query.exec();
    if (query.next())
        return query.value(0).toString().toStdString();
    return "";
}

bool isEntitySubClassOf(std::string const& name, std::string const& className)
{
    if (!exists(name))
        return false;
    if (!exists(className))
        return false;
    // special case for synonym, because below reference name is used
    if (className=="Synonym") return isSynonym(name);

    QSqlQuery query;
    query.prepare("SELECT * FROM subClassOf "
                  "WHERE subClassOf.entity=(SELECT id FROM entity WHERE name=:name) "
                  "AND subClassOf.subClassOf=(SELECT id FROM entity WHERE name=:className)");
    query.bindValue(":name", QString::fromStdString(getReferenceName(name)));
    query.bindValue(":className", QString::fromStdString(className));
    query.exec();
    return query.next();
}

bool isSynonym(std::string const& name)
{
    if (!exists(name))
        return false;
    QSqlQuery query;

    query.prepare("SELECT * FROM subClassOf "
                  "WHERE subClassOf.entity=(SELECT id FROM entity WHERE name=:name) "
                  "AND subClassOf.subClassOf=(SELECT id FROM entity WHERE name=\"Synonym\")");
    query.bindValue(":name", QString::fromStdString(name));
    query.exec();
    return query.next();
}

bool isAnatomicalEntity(std::string const& name)
{
    return isEntitySubClassOf(name, "Anatomical_entity");
}

bool isBone(std::string const& name)
{
    return isEntitySubClassOf(name, "Bone");
}

bool isSkin(std::string const& name)
{
    return isEntitySubClassOf(name, "Skin");
}

bool isLandmark(std::string const& name)
{
    return isEntitySubClassOf(name, "Landmark");
}

std::vector<std::string> getSubClassOfList(std::string const& className)
{
    std::vector<std::string> entityList;
    QSqlQuery query;
    query.prepare("SELECT name FROM entity,subClassOf WHERE subClassOf.subClassOf=:classId AND entity.id=subclassOf.entity");
    query.bindValue(":classId", getEntityId(className));
    query.exec();
    while (query.next())
        entityList.push_back(query.value(0).toString().toStdString());
    return entityList;
}

std::vector<std::string> getParentClassList(std::string const& name)
{
    std::vector<std::string> parentClassList;

    QSqlQuery query;
    query.prepare("SELECT entity.name FROM entity,subClassOf "
                  "WHERE subClassOf.entity=:id AND entity.id=subClassOf.subClassOf");
    query.bindValue(":id", getEntityId(name));
    query.exec();
    while (query.next())
        parentClassList.push_back(query.value(0).toString().toStdString());

    return parentClassList;
}

// internal non recursive version
bool _isEntityPartOf(std::string const& entity, std::string const& parent)
{
    QSqlQuery query;
    query.prepare("SELECT entity FROM partOf "
                  "WHERE partOf.entity=:entityId "
                  "AND partOf.partOf=:partId");
    query.bindValue(":entityId", getEntityId(entity));
    query.bindValue(":partId", getEntityId(parent));
    query.exec();
    return query.next();
}

bool isEntityPartOf(std::string const& entity, std::string const& parent, bool recursive)
{
    if (!recursive)
        return _isEntityPartOf(entity, parent);
    else {
        std::list<std::string> entities;
        std::vector<std::string> tmpEntities;
        entities.push_back(entity);
        bool result = false;
        while (!entities.empty()) {
            result = _isEntityPartOf(entities.front(), parent);
            if (result) break;
            tmpEntities = getPartOfList(entities.front());
            entities.pop_front();
            entities.insert(entities.end(), tmpEntities.begin(), tmpEntities.end());
        }
        return result;
    }

}

std::vector<std::string> getPartOfList(std::string const& name)
{
    std::vector<std::string> partOfList;

    QSqlQuery query;
    query.prepare("SELECT entity.name FROM entity,partOf "
                  "WHERE partOf.entity=:id "
                  "AND partOf.partOf=entity.id");
    query.bindValue(":id", getEntityId(name));
    query.exec();
    while (query.next())
        partOfList.push_back(query.value(0).toString().toStdString());

    return partOfList;
}

// internal non recursive version
std::vector<std::string> _getPartOfSubClassList(std::string const& name, const std::string &className)
{
    std::vector<std::string> partOfList;

    QSqlQuery query;
    query.prepare("SELECT entity.name FROM entity,partOf,subClassOf "
                  "WHERE partOf.entity=:id "
                  "AND subClassOf.entity=partOf.partOf AND subClassOf.subClassOf=:classId "
                  "AND partOf.partOf=entity.id");
    query.bindValue(":id", getEntityId(name));
    query.bindValue(":classId", getEntityId(className));
    query.exec();
    while (query.next())
        partOfList.push_back(query.value(0).toString().toStdString());

    return partOfList;
}

std::vector<std::string> getPartOfSubClassList(std::string const& name, const std::string &className, bool recursive)
{
    if (!recursive)
        return _getPartOfSubClassList(name, className);
    else {
        std::vector<std::string> partOfList;
        std::list<std::string> partOfListToBeRecursed;
        partOfListToBeRecursed.push_back(name);
        while (!partOfListToBeRecursed.empty()) {
            std::vector<std::string> l = getPartOfList(partOfListToBeRecursed.front());
            partOfListToBeRecursed.pop_front();
            partOfListToBeRecursed.insert(partOfListToBeRecursed.end(), l.begin(), l.end());
            for (std::string entity : l)
                if (isEntitySubClassOf(entity, className)) {
                    // prevent duplicated insertion
                    if (std::find(partOfList.rbegin(), partOfList.rend(), entity) == partOfList.rend()) partOfList.push_back(entity);
                }
        }
        return partOfList;
    }
}

std::vector<std::string> _getSubPartOfList(std::string const& name, const std::string &className)
{
    std::vector<std::string> subPartOfList;

    QSqlQuery query;
    query.prepare("SELECT entity.name FROM entity,partOf,subClassOf "
                  "WHERE partOf.partOf=:id "
                  "AND subClassOf.entity=partOf.entity AND subClassOf.subClassOf=:classId "
                  "AND partOf.entity=entity.id");
    query.bindValue(":id", getEntityId(name));
    query.bindValue(":classId", getEntityId(className));
    query.exec();
    while (query.next())
        subPartOfList.push_back(query.value(0).toString().toStdString());

    return subPartOfList;
}

std::vector<std::string> _getSubPartOfList(std::string const& name)
{
    std::vector<std::string> subPartOfList;

    QSqlQuery query;
    query.prepare("SELECT entity.name FROM entity,partOf "
                  "WHERE partOf.partOf=:id "
                  "AND partOf.entity=entity.id");
    query.bindValue(":id", getEntityId(name));
    query.exec();
    while (query.next())
        subPartOfList.push_back(query.value(0).toString().toStdString());

    return subPartOfList;
}

std::vector<std::string> getSubPartOfList(std::string const& name, const std::string &className, bool recursive)
{
    if (!recursive)
        return _getSubPartOfList(name, className);
    else {
        std::vector<std::string> subPartOfList;
        std::list<std::string> subPartOfListToBeRecursed;
        subPartOfListToBeRecursed.push_back(name);
        while (!subPartOfListToBeRecursed.empty()) {
            std::vector<std::string> l = _getSubPartOfList(subPartOfListToBeRecursed.front());
            subPartOfListToBeRecursed.pop_front();
            subPartOfListToBeRecursed.insert(subPartOfListToBeRecursed.end(), l.begin(), l.end());
            for (std::string entity : l)
                if (isEntitySubClassOf(entity, className)) {
                    // prevent duplicated insertion
                    if (std::find(subPartOfList.rbegin(), subPartOfList.rend(), entity) == subPartOfList.rend()) subPartOfList.push_back(entity);
                }
        }
        return subPartOfList;
    }
}

std::vector<std::string> getEntityBibliographyList(std::string const& name)
{
    std::vector<std::string> bibliographyList;

    std::vector<std::string> synonymList = getSynonymList(name);

    QSqlQuery query;
    query.prepare("SELECT entity.name FROM entity,fromBibliography "
                  "WHERE fromBibliography.entity=(SELECT id FROM entity WHERE entity.name=:name) AND entity.id=fromBibliography.fromBibliography");
    for (std::string const& synonym : synonymList) {
        query.bindValue(":name", QString::fromStdString(synonym));
        query.exec();
        if (query.next())
            bibliographyList.push_back(query.value(0).toString().toStdString());
    }
    return bibliographyList;
}

bool isEntityFromBibliography(std::string const& entity, std::string const& bibliography)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM fromBibliography "
                  "WHERE fromBibliography.entity=:entityId AND fromBibliography.fromBibliography=:bibliographyId");
    query.bindValue(":entityId", getEntityId(entity));
    query.bindValue(":bibliographyId", getEntityId(bibliography));
    query.exec();
    return query.next();
}

std::vector<std::string> getLandmarkBoneList(std::string const& name)
{
    std::vector<std::string> entityList;
    QSqlQuery query;
    query.prepare("SELECT entity.name FROM entity,partOf,subClassOf "
                  "WHERE partOf.entity=:id AND entity.id=partOf.partOf "
                  "AND subClassOf.entity=partOf.partOf AND subClassOf.subClassOf=:boneClassId" );
    query.bindValue(":id", getEntityId(name));
    query.bindValue(":boneClassId", getEntityId("Bone"));
    query.exec();
    while (query.next()) {
        entityList.push_back(query.value(0).toString().toStdString());
        query.bindValue(":id", getEntityId(entityList.back()));
        query.exec();
    }
    return entityList;
}

std::vector< std::vector<std::string> > getInsertOnList(std::string const& name, std::string const& className)
{
    std::vector< std::vector<std::string> > entityList;
    QSqlQuery query;
    query.prepare("SELECT entity.name FROM entity,insertOn "
                  "WHERE insertOn.entity=:id AND insertOn.insertOn=entity.id" );
    query.bindValue(":id", getEntityId(name));
    query.exec();
    while (query.next()) {
        entityList.push_back(std::vector<std::string>());
        std::string insertOn = query.value(0).toString().toStdString();
        if (isEntitySubClassOf(insertOn, className) )
            entityList.back().push_back(insertOn);
        std::vector<std::string> partOfList = getPartOfSubClassList(insertOn, className, true);
        std::move(partOfList.begin(), partOfList.end(), std::back_inserter(entityList.back()));
    }
    return entityList;
}

void getSubClassOfList(std::string const& className, std::vector<std::string> &entityList)
{
    entityList.clear();
    QSqlQuery query;
    query.prepare("SELECT entity.name FROM entity,subClassOf "
                  "WHERE subClassOf.subClassOf=:classId AND subClassOf.entity=entity.id");
    query.bindValue(":classId", getEntityId(className));
    query.exec();
    while (query.next())
        entityList.push_back(query.value(0).toString().toStdString());
}

void getAnatomicalEntityList(std::vector<std::string> &entityList)
{
    getSubClassOfList("Anatomical_entity", entityList);
}

void getLandmarkList(std::vector<std::string> &landmarkList)
{
    getSubClassOfList("Landmark", landmarkList);
}

void getJointList(std::vector<std::string> &jointList)
{
    getSubClassOfList("Joint", jointList);
}

std::vector<std::string> getSubClassOfFromBibliographyList(std::string const& className, std::string const& bibliography)
{
    std::vector<std::string> entityList;
    QSqlQuery query;
    query.prepare("SELECT entity.name FROM entity,fromBibliography "
                  "WHERE fromBibliography.entity=entity.id AND fromBibliography.fromBibliography=:bibliographyId");
    query.bindValue(":bibliographyId", getEntityId(bibliography));
    query.exec();
    while (query.next()) {
        std::string entity = query.value(0).toString().toStdString();
        if (isEntitySubClassOf(entity, className))
            entityList.push_back(entity);
    }
    return entityList;
}

std::vector<std::string> getAllParents(std::string const& element, std::string const& className){
    std::vector<std::string> parents = getPartOfSubClassList(element,className);
    if(parents.empty()) return {};

    std::vector<std::string> ret = getAllParents(parents[0], className);
    if(ret.empty()) return std::vector<std::string>(1,parents[0]);
    ret.insert(ret.begin(),parents[0]); //The first parent of the list is the closest one to the element
    return ret;
}

std::vector<std::string> search(std::string const& word, const std::string &className, bool doSynonyms){
    std::vector<std::string> names = getSubClassOfList(className);
    std::vector<std::string> results = {};

    std::string wordLower = word;
    // TODO better alternative for changing case - QString ?
    for (int i=0; i<wordLower.size(); i++) wordLower[i] = tolower(wordLower[i]);

    if(names.empty()) std::cerr << "No elements in class " << className << "\n";

    for(int i = 0; i < names.size(); i++){
        std::string currentName = names.at(i);
        std::string currentNameLower = currentName;
        // TODO better alternative for changing case - QString ?
        for (int i=0; i<currentNameLower.size(); i++) currentNameLower[i] = tolower(currentNameLower[i]);
        std::string::size_type fit = currentNameLower.find(wordLower);
        if(fit != std::string::npos){
            results.push_back(currentName);
        }else{
            if(doSynonyms){
                std::vector<std::string> synonyms = getSynonymList(currentName);
                for(int j = 0; j < synonyms.size(); j++){
                    std::string syn = synonyms.at(j);
                    for (int i=0; i<syn.size(); i++) syn[i] = tolower(syn[i]);
                    if(syn.find(wordLower) != std::string::npos){
                        results.push_back(currentName);
                        break;
                    }
                }
            }
        }
    }
    return results;

}

} // anatomydb
