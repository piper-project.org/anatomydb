// Copyright (C) 2017 INRIA
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 2.1 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details. You should have received a copy of the GNU Lesser
// General Public License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Thomas Lemaire (INRIA)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.2
import QtQuick.Window 2.2
//import QtQuick.Layouts 1.3

import AnatomyDB 1.0
import AnatomicTreeModel 1.0

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 800
    height: 700
    title: qsTr("Anatomy DB Navigation")
    property color mainColor : "#d8dbdb"
    property color darkColor : "#b2b8be"
    property color pastelColor : "#f4f9fd"
    property color flashColor : "#63dce0"

//    Rectangle{
//        color: "#f4f9fd"
//    }


    /** Function used to insert the list of values in the model with the provided roles.
            This function is based on AnatomicTreeModel getDataRole function. The list must be an anatomydb entity.**/

     function insertList(model,list,roles){ //List is a QStringList
         model.clear();

         var list_size = list.length;
         for( var i=0; i < list_size ; ++i ) {
             if(model === modelSearch){
                 model.append({"name" : list[i],"synonym" : modelTree.getDataRole("Synonyms",list[i])});
             }else{

                 if(model !== modelClass){
                     model.append({"name" : list[i]});
                     for(var j = 0; j < roles.length; j++){
                         var data = modelTree.getDataRole(roles[j],list[i]);
                         model.setProperty(i,roles[j],data);
//                         console.log(list[i], " has ", roles[j], " : ", model.get(i).roles[j]);
                     }
                 }else{
                     model.append({"name" : list[i]});
                 }
             }
         }
         return list_size;
     }


     /********************* Models definition *********************/
    ListModel {
         id: modelList
        objectName: "modelList"

        function getListValue(index){
            return this.get(index) ? this.get(index).name : "";
        }

        function getMatches(word){
            var returnList = []
            if(word !== ""){
                for(var i = 0; i < this.rowCount(); i ++){
                    var data = this.getListValue(i)
                    var match = data.toLowerCase().indexOf(word.toLowerCase());
                    var matchSyn = match
                    var syn =""
                    if(synSearch.checked){
//                        console.log("syn search activated")
                        syn = modelTree.getDataRole("Synonyms",data)
                        matchSyn = syn.toLowerCase().indexOf(word.toLowerCase())
//                        if(matchSyn >= 0) console.log("syn search, matchsyn is positive")
                    }
                    if((match>=0)||(matchSyn >= 0)) {
                        var index = i
//                        console.log("index ", index, "; data : ", data, "; synonym : ",syn)
                        var pair = {"index" : index, "name" : data, "synonym" : syn}
                        returnList.push(pair);
                    }
                }
            }
            return returnList;
        }
    }

    ListModel {
         id: modelSearch
        objectName: "modelSearch"

        function getListValue(index){
            return this.get(index) ? this.get(index).name : "";
        }

        function insertList(list){
            this.clear()
            for( var i=0; i < list.length ; ++i ) {
                this.append({"name" : list[i].name,"synonym" : list[i].synonym, "refIndex" : list[i].index})
                        }
            results.setHeight()
        }
    }

    ListModel {
        id: modelClass
        objectName: "modelClass"

        ListElement{
            name: "Region"
        }
        ListElement{
            name: "Bone"
        }
        ListElement{
            name: "Joint"
        }
        ListElement{
            name: "Landmark"
        }
        ListElement{
            name: "Bibliography"
        }
    }

    AnatomicTreeModel {
        id: modelTree
        objectName: "modelTree"


        qdataList: ["Body_proper", "Left_arm", "Atlas", "Trunk", "Right_upper_limb",
                        "Left_upper_limb", "Right_lower_limb", "Left_lower_limb",
                        "Left_femur", "Left_tibia","Test", "Fifth_cervical_vertebra", "Neck"]
        onListChanged: {
            insertList(modelList,this.qdataList,this.roles) //reloads the modelList according to modelTree list changes
        }

        function getListValue(index){
            return this.model.data(index,0) ? this.model.data(index,0) : ""
        }

        function searchIndexIn(value,parent){

            var parentValue = this.getListValue(parent)
            var result
            if(parentValue !== ""){

                if(this.model.data(parent,0) === value){
                    return parent;
                }

                var rowCount = this.model.rowCount(parent)            //get the amount of rows in the model

                for(var i = 0; i < rowCount; i ++){
                    var index = this.model.child(parent,i)
                    result = this.searchIndexIn(value,index)
                    if(result) return result // If result is not undefined
                }
            }

    //        return result
        }
        function searchIndex(value){
            var rowCount = this.model.rowCount()            //get the amount of rows in the model

            var result = 0
            for(var i = 0; i < rowCount; i ++){
                var index = this.model.index(i,0)
                result = this.searchIndexIn(value,index)
                if(result) return result // If result is not undefined
            }
            return result
        }

        function getMatches(word){
            if(word === "") return []
            var indexes = this.model.getAllIndexes()
            var returnList = []
            for(var i = 0; i < indexes.length; i ++){
                var index = indexes[i]
                var data = this.getListValue(index)

                var match = data.toLowerCase().indexOf(word.toLowerCase())
                var matchSyn = match
                var syn =""
                if(synSearch.checked){
//                    console.log("syn search activated")
                    syn = modelTree.getDataRole("Synonyms",data)
                    matchSyn = syn.toLowerCase().indexOf(word.toLowerCase())
//                    if(matchSyn >= 0) console.log("syn search, matchsyn is positive")
                }
                if((match>=0)||(matchSyn >= 0)) {
//                    console.log("index ", index, "; data : ", data, "; synonym : ",syn)
                    var values = {"index" : index, "name" : data, "synonym" : syn}
                    returnList.push(values);
                }
            }
            return returnList;
        }
    }


    SplitView{
        id: splitTabsTool
        anchors.top: searchBar.bottom
        anchors.bottom: parent.bottom
        anchors.margins: 5

        width: parent.width
//        height: 100

        Rectangle {
            id: views
//            width: 600
            width: mainWindow.width - 150
            color: "transparent"
            anchors.margins: 3
            property var modelTab : modelTree  // This is used by the SearchComponent
            property var currentView : treeView
            property int currentIndex : -1

            onCurrentViewChanged: {
                var listVisible = (currentView === listView)
                listView.visible = listVisible
                treeView.visible = !listVisible
                modelTab = listVisible ? listView.model : modelTree
            }

            MouseArea {
                    anchors.fill: parent
                    onClicked: {
//                        searchComponent.focus = false
                        views.currentView.forceActiveFocus()
                    }
                }

            TreeView {
                id: treeView
    //                title: "treeView"

                anchors.fill : parent

                TableViewColumn {
                     role: "Name"
                     title: "Name"
                     width: 300
                 }
                TableViewColumn {
                     role: "Synonyms"
                     title: "Synonyms"
                     width: 300
                     visible: synCheck.checked
                 }
                TableViewColumn {
                     role: "Class"
                     title: "Class"
                     width: 300
                     visible: classCheck.checked
                 }
                TableViewColumn {
                     role: "PartOf"
                     title: "Other parents"
                     visible: jointCheck.checked
                 }
                TableViewColumn {
                     role: "Bibliography"
                     title: "From Bibliography"
                     visible: bibliCheck.checked
                 }

                model: modelTree.model

                //replace with a list of indexes that match the search component
                itemDelegate: Component {
                                 Text{
                                      id: delText
                                      text: styleData.value
                                      verticalAlignment: Text.AlignVCenter
                                      font.pointSize: 10
                                      //We choosed to bolden the words matching the searchComponent text
                                      font.bold: searchComponent.matchIndex(styleData.index)
                                     }
                             }

                function expandAncestors(index) {
                    var parent = this.model.parent(index)
//                    console.log("item ", anatomicTree.getListValue(index), " has parent ", anatomicTree.getListValue(parent) )
                    if((parent) && (modelTree.getListValue(parent) !== "")){ //if index has a parent
                        this.expandAncestors(parent) //expansion of the parent
                        this.expand(parent) //expansion of the parent
                    }
                }

                function scrollTo(index){
                    expandAncestors(index)

                    var child = index
                    var parent = model.parent(child)
                    var i = 0

                    while((parent) && (modelTree.getListValue(parent) !== "")){
                        child = parent
                        parent = model.parent(child)
                        i ++
                    }
                    var row = model.rowInParent(child)
                    this.positionViewAtRow(row,ListView.Beginning)
                }

                function positionViewAtRow(row, mode) {
                    __listView.positionViewAtIndex(row, mode)
                }
            }

            TableView { //Same behavior than treeView
                     id:listView

                     anchors.fill : parent

                     TableViewColumn {
                          role: "Name"
                          title: "Name"
                          width: 300
                      }
                     TableViewColumn {
                          role: "Synonyms"
                          title: "Synonyms"
                          width: 300
                          visible: synCheck.checked
                      }
                     TableViewColumn {
                          role: "Class"
                          title: "Class"
                          width: 300
                          visible: classCheck.checked
                      }
                     TableViewColumn {
                          role: "PartOf"
                          title: "Other parents"
                          visible: jointCheck.checked
                      }
                     TableViewColumn {
                          role: "Bibliography"
                          title: "From Bibliography"
                          visible: bibliCheck.checked
                      }

                     anchors.bottomMargin: -2
                     clip: true

                     model: modelList

                     visible: false

                     itemDelegate: Item{
                        Text{
                             id: delText
                             text: styleData.value
                             verticalAlignment: Text.AlignVCenter
                             font.pointSize: 10
                             font.bold: searchComponent.matchIndex(styleData.row)
                            }
                    }

                     function scrollTo(index){
                         this.positionViewAtRow(index,ListView.Contain );
                     }
            }

            Keys.onPressed: {

                if(event.key === Qt.Key_Left){
                        //Go to previous matching index
//                    console.log("Left key !")
                    var item = searchComponent.previous()
                    if(item !== 0){
                        views.currentView.scrollTo(item.index)
                        searchComponent.text = item.name
                    }
                }

                if(event.key === Qt.Key_Right){
                        //Go to next matching index
//                    console.log("Right key !")
                    var item = searchComponent.next()
                    if(item !== 0){
                        views.currentView.scrollTo(item.index)
                        searchComponent.text = item.name
                    }
                }
            }
        }
        Rectangle {
            id: tools
            objectName: "tools"

            anchors.margins: 5

            width: 150
            radius: 5
            color: mainColor

            Rectangle {
                id: switchItem

                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.margins: 5

                height: 30
                color: "transparent"
                border.color: darkColor
                border.width: 1
                opacity: 0.8
                radius: parent.radius

                ExclusiveGroup { id: tabPositionGroup }

                Button {
                    id: treeButton
                    anchors.right: listButton.left
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.margins: parent.border.width

                    text: "Tree"
                    checked: true
                    checkable: true
                    exclusiveGroup: tabPositionGroup
                    onCheckedChanged: {
                        if(this.checked){
                            views.currentView = treeView
                        }
                    }
                    style: ButtonStyle {
                        background: Rectangle{
                            height: parent.height
                            width: parent.width
                            color: "transparent"
                            Rectangle{
                                id: roundedRect
                                anchors.fill: parent
                                color: mainColor
                                radius: switchItem.radius
                                gradient: Gradient {
                                    GradientStop { position: 0 ; color: control.checked ? darkColor : mainColor }
                                    GradientStop { position: 1 ; color: control.checked ? pastelColor : darkColor }
                                }
                            }
                            Rectangle{
                                anchors.top: parent.top
                                anchors.bottom: parent.bottom
                                anchors.right: parent.right
                                width: roundedRect.radius
                                color: roundedRect.color
                                gradient: Gradient {
                                    GradientStop { position: 0 ; color: control.checked ? darkColor : mainColor }
                                    GradientStop { position: 1 ; color: control.checked ? pastelColor : darkColor }
                                }
                            }
                        }
                    }
                }

                Button {
                    id: listButton
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.margins: parent.border.width
                            width: parent.width / 2.0

                    text: "List"
                    checked: false
                    checkable: true
                    exclusiveGroup: tabPositionGroup
                    onCheckedChanged: {
                        if(this.checked){
                            views.currentView = listView
                        }
                    }
                    style: ButtonStyle {
                        background: Rectangle{
                            height: parent.height
                            width: parent.width
                            color: "transparent"
                            Rectangle{
                                id: roundedRectList
                                anchors.fill: parent
                                color: mainColor
                                radius: switchItem.radius
                                gradient: Gradient {
                                    GradientStop { position: 0 ; color: control.checked ? darkColor : mainColor }
                                    GradientStop { position: 1 ; color: control.checked ? pastelColor : darkColor }
                                }
                            }
                            Rectangle{
                                anchors.top: parent.top
                                anchors.bottom: parent.bottom
                                anchors.left: parent.left
                                width: roundedRectList.radius
                                color: roundedRectList.color
                                gradient: Gradient {
                                    GradientStop { position: 0 ; color: control.checked ? darkColor : mainColor }
                                    GradientStop { position: 1 ; color: control.checked ? pastelColor : darkColor }
                                }
                            }
                        }
                    }
                }
            }


            SplitView{
                anchors.top: switchItem.bottom
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: 5

                orientation: Qt.Vertical
                width: parent.width

                handleDelegate: Component {
                    Rectangle {
                        height: 2
                        width: parent.width
                        color: darkColor
                        radius: 3
                    }
                }

                Rectangle {
                    id: classItem

                    anchors.margins: 5

                    height: childrenRect.height
//                    color: "transparent"
                    opacity: 0.7
                    radius: 5
//                    visible: (views.currentView !== listView)

                    Label {
                        id: classTitle

                        anchors.top: parent.top

                        text: "Classes selection"
                        font.pixelSize: 16
                        font.italic: true
                    }

                    TableView {
                        id: classesChoice

                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: classTitle.bottom
//                        anchors.bottom: allClasses.top

                        headerVisible: false

                        TableViewColumn{
                            role: "name"
                        }

                        height: 100

                        onClicked: {
                            var selectedClasses = []
                            selection.forEach( function(rowIndex) {
//                                console.log(rowIndex)
                                selectedClasses.push(model.get(rowIndex).name)
                            } )
                            modelTree.dataListFromClasses(selectedClasses)
                        }

                        itemDelegate: Item{
                            height: 20
                           Text{
                                text: styleData.value
                                verticalAlignment: Text.AlignVCenter
                                font.pointSize: 10
                                font.bold: styleData.selected
                               }
                        }

                        selectionMode: SelectionMode.MultiSelection

                        model: modelClass
                    }
                }

                Rectangle {
                    id: columnsItem

                    anchors.margins: 5

                    height: 120
        //            color: parent.color
                    opacity: 0.7
                    radius: 5

                    Label {
                        id: columnsTitle

                        anchors.top: parent.top
                        height: 20

                        text: "Columns selection"
                        font.pixelSize: 16
                        font.italic: true
                    }


                    // Selection tools for the roles to display on the views - not available for the search Tab
                    Column {
                        id: checks

                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: columnsTitle.bottom

                        CheckBox {
                            id: synCheck
                            text: qsTr("Syn")
                            checked: true
                        }
                        CheckBox {
                            id: classCheck
                            text: qsTr("Class")
                            checked: false
                        }
                        CheckBox {
                            id: jointCheck
                            text: qsTr("Other parents")
                            checked: false
                        }
                        CheckBox {
                            id: bibliCheck
                            text: qsTr("From Bibliography")
                            checked: false
                        }
                    }
                }

                Rectangle {
                    id: otherTool

                    color: "transparent"
                }
            }

        }

    }



    Rectangle {
        id: searchBar
        objectName: "searchBar"

        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 5

        height: 30
        radius: 5
        color: mainColor

        Keys.onPressed: {
            if(results.focus){

                if(event.key === Qt.Key_Down){
                    if(listSuggest.currentIndex !== listSuggest.model.rowCount()-1){
                        listSuggest.incrementCurrentIndex()
                    }else{
                        searchComponent.forceActiveFocus()
                        listSuggest.currentIndex = -1
                    }
                }

                if(event.key === Qt.Key_Up){
                    if(listSuggest.currentIndex !== 0){
                        listSuggest.decrementCurrentIndex()
                    }else{
                        searchComponent.forceActiveFocus()
                        listSuggest.currentIndex = -1
                    }
                }

                if((event.key === Qt.Key_Left)||(event.key === Qt.Key_Right)){
                        searchComponent.forceActiveFocus()
                        listSuggest.currentIndex = -1
                }


                if(event.key === Qt.Key_Return){
//                    console.log("Return key pressed on results! ")
                    searchComponent.text = listSuggest.model.get(listSuggest.currentIndex).name
                    listSuggest.currentIndex = -1
                    searchComponent.forceActiveFocus()
                }

            }
            else{
                if(event.key === Qt.Key_Down){
                    results.forceActiveFocus()
                    listSuggest.currentIndex = 0;
//                    console.log("Down key pressed on searchField! current index is", listSuggest.currentIndex)
                }

                if(event.key === Qt.Key_Up){
                    results.forceActiveFocus()
                    listSuggest.positionViewAtEnd()
                    listSuggest.currentIndex = listSuggest.model.rowCount() -1
//                    console.log("Down key pressed on searchField! current index is", listSuggest.currentIndex)
                }

                if(event.key === Qt.Key_Return){
                    views.forceActiveFocus()
//                    console.log(" index 0 : ", searchComponent.current().index)
                    views.currentView.scrollTo(searchComponent.current().index)

                }
            }
        }

        Rectangle {
            id: searchField
            color: "white"
            opacity: 0.9

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.margins: 3
            radius: 4
            width: parent.width - 20 - navigation.width - searchOptions.width

            Component.onCompleted: searchComponent.forceActiveFocus()

            SearchComponent {
                id: searchComponent
                anchors.fill : parent

                refModel: views.modelTab
                defaultText: qsTr("Search for an entity")

                onTextChanged: {
                    listSuggest.positionViewAtBeginning()
                }
                onIndexListChanged: {
                    modelSearch.insertList(indexList)
//                    console.log("list inserted in modelsearch, size :", this.indexList.length, modelSearch.rowCount(), results.visible, results.height)
                }
                onFocusChanged: {
                    listSuggest.positionViewAtBeginning()
                }
                onEditingFinished: {
                    this.focus = false
                    views.forceActiveFocus()
                }
            }
        }

        Rectangle {
            id: results

            anchors.top : searchField.bottom
            anchors.left: searchField.left
            width: searchField.width
            z: splitTabsTool.z + 100

            visible: (this.focus || searchComponent.focus || searchField.focus) && (searchComponent.text !== "")

            height: 0
            onVisibleChanged: this.setHeight()

            function setHeight(){
                var nbRow = 0;
                var nbMax = 5;
                if(searchComponent.text !== ""){
                    nbRow = listSuggest.model.rowCount();
//                    console.log("nb rows :", nbRow)
                    if(nbRow>nbMax) nbRow = nbMax;

                    nbRow = nbRow*itemHeight;
                }
                this.height = nbRow
            }

            property int itemHeight : 20

            border.width: 1
            border.color: mainColor
            color: "white"
            opacity: 0.9

            Component {
                id: suggestDelegate
                Rectangle {
                    id: rectDelegate
                    height: 20
                    width: listSuggest.width
                    focus: false
                    color: ((index === listSuggest.currentIndex)&&this.focus) ? pastelColor : "transparent"

                    Row{
                        spacing: 10
                        Text{
                            text: name
                            verticalAlignment: Text.AlignVCenter
                            font.pointSize: 10
                           }
                        Text{
                            text: synonym
                            verticalAlignment: Text.AlignVCenter
                            font.pointSize: 10
                            visible: synSearch.checked
                            font.italic: true
                           }
                        }
                    MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                results.focus = true
                                if(listSuggest) listSuggest.currentIndex = index
                            }
                        }
                }
            }

            ListView {
                id: listSuggest

                anchors.fill: parent
                anchors.margins: 1

                currentIndex: -1

                model: modelSearch

                delegate: suggestDelegate

                clip : true

                onModelChanged: {
                    parent.setHeight()
                }
            }
        }

        Rectangle {
            id: navigation

            anchors.left: searchField.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 5
            width: 50

            height: parent.height
            color: darkColor
            border.color: darkColor
            border.width: 1
            radius: 5

            Button {
                id: previousArrow

                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.margins: parent.border.width
                width: (parent.width / 2.0) - parent.border.width - 1

                text: "<"

                onClicked: {
                    searchComponent.focus = false
                    views.forceActiveFocus()
                    var item = searchComponent.previous()
                    if(item !== 0){
                        views.currentView.scrollTo(item.index)
                        searchComponent.text = item.name
                    }
                }

                style: ButtonStyle {
                    background: Rectangle{
                        height: parent.height
                        width: parent.width
                        color: "transparent"
                        Rectangle{
                            id: roundedRectPrev
                            anchors.fill: parent
                            color: mainColor
                            radius: navigation.radius
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: control.pressed ? darkColor : mainColor }
                                GradientStop { position: 1 ; color: control.pressed ? pastelColor : darkColor }
                            }
                        }
                        Rectangle{
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom
                            anchors.right: parent.right
                            width: roundedRectPrev.radius
                            color: roundedRectPrev.color
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: control.pressed ? darkColor : mainColor }
                                GradientStop { position: 1 ; color: control.pressed ? pastelColor : darkColor }
                            }
                        }
                    }
                }
            }

            Button {
                id: nextArrow
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.margins: parent.border.width
                width: (parent.width / 2.0) - parent.border.width - 1

                text: ">"

                onClicked: {
                    searchComponent.focus = false
                    views.forceActiveFocus()
                    var item = searchComponent.next()
                    if(item !== 0){
                        views.currentView.scrollTo(item.index)
                        searchComponent.text = item.name
                    }
                }

                style: ButtonStyle {
                    background: Rectangle{
                        height: parent.height
                        width: parent.width
                        color: "transparent"
                        Rectangle{
                            id: roundedRectNext
                            anchors.fill: parent
                            color: mainColor
                            radius: navigation.radius
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: control.pressed ? darkColor : mainColor }
                                GradientStop { position: 1 ; color: control.pressed ? pastelColor : darkColor }
                            }
                        }
                        Rectangle{
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom
                            anchors.left: parent.left
                            width: roundedRectNext.radius
                            color: roundedRectNext.color
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: control.pressed ? darkColor : mainColor }
                                GradientStop { position: 1 ; color: control.pressed ? pastelColor : darkColor }
                            }
                        }
                    }
                }
            }
        }

        Rectangle {
            id: searchOptions

            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 5

            height: parent.height
            width: synSearch.width
            color: "transparent"

            CheckBox {
                id: synSearch

                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Check syn.")
                checked: true
                visible: true
                onCheckedChanged: {
                    searchComponent.indexList = searchComponent.refModel.getMatches(searchComponent.text)
                }
            }
        }

     }


}

