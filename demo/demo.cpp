/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlProperty>
#include <QVariant>
#include <QString>
#include <QtQml>

#include <stdlib.h>
#include <iostream>

#include "anatomyDB/query.h"
#include "anatomyDB/anatomictreemodel.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);                            //Create the qml environment
    QQmlApplicationEngine engine;

    anatomydb::init();

/************************************ Tree Model ************************************/
//    QList<QVariant> rootData;
//    QStringList classes = {};

//    std::vector<std::string> listData ;
//    anatomydb::getAnatomicalEntityList(listData);

//    std::vector<std::string> otherData = anatomydb::getSubClassOfList("Landmark");
//    listData.insert( listData.end(), otherData.begin(), otherData.end() );

//    otherData.clear();
//    otherData = anatomydb::getSubClassOfList("Bibliography");
//    listData.insert( listData.end(), otherData.begin(), otherData.end() );

    //This data has to match with the qml views' roles that we want to display
//    rootData << "Name" << "Synonyms" << "Class" << "PartOf" << "Bibliography";
//    classes << "Landmark" << "Region" << "Bibliography" << "Bone" << "Joint";

//    AnatomicTreeModel* tree = new AnatomicTreeModel(listData,classes,rootData);
//    AnatomicTreeModel* tree = new AnatomicTreeModel(listData);

/************************************ settle QML environment ************************************/

    qmlRegisterType<AnatomicTreeModel>("AnatomicTreeModel", 1, 0, "AnatomicTreeModel");



    engine.addImportPath("qrc:/qml");
//    engine.addImportPath("qrc:/");
//    engine.rootContext()->setContextProperty("initAnatomicTree", tree);   //Set the tree as modelTree

    engine.load(QUrl("qrc:/demo.qml"));


    return app.exec();

}

