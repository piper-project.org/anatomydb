// Copyright (C) 2017 INRIA
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 2.1 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details. You should have received a copy of the GNU Lesser
// General Public License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Thomas Lemaire (INRIA)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQml.Models 2.2

import AnatomicTreeModel 1.0
import AnatomyDB 1.0


ApplicationWindow {
    id: mainWindow
    visible: true
    width: 640
    height: 480

    AnatomicTreeModel {
        id: entityTreeModel

        function  updateEntityList() {
            var regions = ["Head","Body_proper", "Neck", "Trunk", "Right_upper_limb", "Left_upper_limb", "Right_lower_limb", "Left_lower_limb"]
//            var entities = ["Left_femur", "Left_tibia", "Pelvis"] // crash on unknown entity
            var entities = ["Skull", "Atlas","Left_fibula","Right_fibula", "Left_clavicle","Left_femur", "Left_tibia","Right_femur", "Right_tibia",
                            "Pelvic_skeleton","C2","C3","C4","C5","C6","T6","L3"];
            qdataList = regions.concat(entities);
        }

        Component.onCompleted: { updateEntityList(); }
    }

    ColumnLayout {
        anchors.fill: parent

//        spacing: 120

        SearchTree {
            id: searchTree

            Layout.fillWidth: true

            treeModel: entityTreeModel
            treeView: treeView

            Component.onCompleted: this.forceActiveFocus()

            onGotoCurrent: console.log(this.getCurrent().name , " is current index")
        }

        TreeView {
            id: treeView

            Layout.fillWidth: true
            Layout.fillHeight: true

            z:-1

            model: entityTreeModel.model

            selectionMode: SelectionMode.MultiSelection
            selection: ItemSelectionModel {
                model: entityTreeModel.model

                function selectAll() {
                    var allIndexes=model.getAllIndexes();
                    for (var i=0; i<allIndexes.length; ++i)
                        select(allIndexes[i], ItemSelectionModel.Select);
                }

                onSelectionChanged: {
                    for (var i=0; i<selected.length; ++i) {
                        // select children if any
                        var childrenIndexes = model.getChildrenIndexes(selected[i].topLeft, false);
                        for (var j=0; j<childrenIndexes.length; ++j)
                            select(childrenIndexes[j], ItemSelectionModel.Select);
                    }

                    for (var i=0; i<deselected.length; ++i) {
                        // deselect children if any
                        var childrenIndexes = model.getChildrenIndexes(deselected[i].topLeft, false);
                        for (var j=0; j<childrenIndexes.length; ++j)
                            select(childrenIndexes[j], ItemSelectionModel.Deselect);
                    }
                }
            }

            //        rowDelegate: Rectangle {
            // how to get row index without the parent ??
            // same pb as in https://forum.qt.io/topic/60609/get-qmodelindex-from-treeview-rowdelegate/2
            // no solution...
            //            id: highlightMatch
            //            width: parent.width
            //            height: parent.height
            //            border.width: styleData.selected && searchTree.matchIndex(styleData.row) ? 2 : 0
            //            border.color: styleData.selected && searchTree.matchIndex(styleData.row) ? "yellow" : "transparent"
            //            color: styleData.selected ? "steelblue" : (searchTree.matchIndex(styleData.row) ? "yellow" : "transparent")
            //        }

            itemDelegate: Label {
                font.bold: searchTree.matchIndex(styleData.index)
                text: styleData.value
            }

            TableViewColumn {
                role: "Name"
                title: "Name"
            }
        }

    }

    Component.onCompleted: {
        treeView.selection.selectAll();
    }

}
