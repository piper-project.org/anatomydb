/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef ANATOMICTREEMODEL_H
#define ANATOMICTREEMODEL_H

#include "treemodel.h"

#ifdef WIN32
#	ifdef anatomyDB_EXPORTS
#		define ANATOMYDB_EXPORT __declspec( dllexport )
#	else
#		define ANATOMYDB_EXPORT __declspec( dllimport )
#	endif
#else
#	define ANATOMYDB_EXPORT
#endif

class TreeModel;

namespace Roles
{
    /**
     * @brief The Role enum, that refers all the values the AnatomicTreeModel's roles can have.
     */
  enum Role
  {
      Name,
      Synonyms,
      Class,
      PartOf,
      Landmark,
      Bibliography,
      Other
  };

  static const Role All[] = { Name, Synonyms, Class, PartOf, Landmark, Bibliography};
}

/**
 * \todo make the type available in a qt plugin http://doc.qt.io/qt-5/qqmlextensionplugin.html
 * \todo move classes in an anatomydb namespace
 */
class ANATOMYDB_EXPORT AnatomicTreeModel : public QObject
{
    Q_OBJECT
    //is build from className, rolesList and with buildModelTree
    Q_PROPERTY(TreeModel* model READ getModel NOTIFY modelChanged) // WRITE setModel NOTIFY modelChanged)
    //There must be at least one role, to display the name of the data in the tree
    Q_PROPERTY(QList<QVariant> roles READ getRoles )//WRITE setRoles NOTIFY roleChanged)
    Q_PROPERTY(QList<QVariant> qdataList READ getQDataList WRITE setQDataList NOTIFY listChanged)
public:
    /**
     * @brief AnatomicTreeModel : basic constructor, builds an empty tree.
     */
    AnatomicTreeModel();
	~AnatomicTreeModel();

    /**
     * @brief AnatomicTreeModel : creates a tree model from a list of values.
     * @param valList : a list of strings from which the tree will be built
     */
    AnatomicTreeModel(std::vector<std::string> valList);

    QList<QVariant> getRoles();

    QList<QVariant> getQDataList();

    /** Set the list of entities to be inserted in the tree
     */
    void setQDataList(QList<QVariant> list);

    /**
     * @brief dataListFromClasses : depends on anatomy db, fills the dataList with the elements of class in classes
     * @param classes
      */
    Q_INVOKABLE void dataListFromClasses(QStringList classes);

    /**
     * @brief getDataRole
     * @param role
     * @param qelement
     * @return the value to set at the element's role
     */
    Q_INVOKABLE QVariant getDataRole(QVariant role, QString qelement);

signals:
    void modelChanged();
    void listChanged();

private:

    /**
     * @brief getMatchingClass
     * @param name
     * @return the first class of the element elem that is in the list of classes
     */
    std::string getMatchingClass(std::string elem = "");
    /**
     * @brief getElemClasses
     * @param element
     * @return All the classes, ordered by the importance for the element (first its class, then the regions, then the other classes)
     * Empty if not an anatomydb element.
     */
    std::vector<std::string> getElemClasses(std::string element);
    std::vector<std::string> getClasses();
    void setClasses(std::vector<std::string> classList = {});

    TreeModel* getModel();
    void setModel(TreeModel* model);

    std::vector<std::string> getDataList();
    /// \todo merge with setqDataList()
    void setDataList(std::vector<std::string>* list);

    void setRoles(QList<QVariant> roles = {});
    /**
     * @brief roleToQVariant
     * @param r
     * @return the QVariant value matching the Role r
     */
    QVariant roleToQVariant(Roles::Role r);
    /**
     * @brief roleFromQVariant
     * @param val
     * @return the Role matching the QVariant value
     */
    Roles::Role roleFromQVariant(QVariant val);
    Roles::Role getRole(int i);

    /**
     * @brief getPartOf
     * @param element
     * @param allAncestors : if false, only the direct parents are returned; if true, all the ancestors among the list are returned
     * @return the list of element's parents of all classes, an empty list if element is not an anatomydb element
     */
    std::vector<std::string> getPartOf(std::string const& element, bool allAncestors);
    /**
     * @brief getParentsAmong
     * @param element : an anatomydb element
     * @param valList
     * @return the element's parents of class classElement that are among valList, from the closest to the further;
     * an empty list if there is no parents or if the element is not an anatomydb element
     */
    std::vector<std::string> getParentsAmong(std::string element, std::string classElement, std::vector<std::string> const& valList);

	
    std::string getFirstParentInList(std::string element, std::vector<std::string> const& classList);

    bool isInList(std::string element);

    /**
     * @brief buildModelTreeFromList : fills model_relations and model_data with the right data to create the TreeModel.
     * @param model_relations : a map with the parent's name as the key and the list of its children's name as the value
     * @param model_data : a map with an element's name as the key and the data to set in the tree for that item.
     */
    void buildModelTreeFromList(std::map<QVariant,QList<QVariant> >* model_relations, std::map<QVariant, QList<QVariant> > *model_data);


    TreeModel *m_model;
    std::vector<std::string> m_classes;
    QList<Roles::Role> m_rolesList;
    std::vector<std::string> dataList;
};

#endif // ANATOMICTREEMODEL_H
