// Copyright (C) 2017 INRIA
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 2.1 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details. You should have received a copy of the GNU Lesser
// General Public License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Thomas Lemaire (INRIA)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
%module anatomyDB

// TODO share code with octave bindings

//%begin %{
//#ifdef _MSC_VER
//#define SWIG_PYTHON_INTERPRETER_NO_DEBUG
//#define _ALLOW_ITERATOR_DEBUG_LEVEL_MISMATCH
//#define _CRT_SECURE_INVALID_PARAMETER(expr)
//#endif
//%}

%include "std_string.i"
%include "std_vector.i"
%include "std_array.i"
%include "std_map.i"
%include "eigen.i"

%{
#include "anatomyDB/types.h"
#include "anatomyDB/query.h"
#include "anatomyDB/FrameFactory.h"
%}

%exception {
  try {
    $action
  } catch (const std::runtime_error& e) {
    SWIG_exception(SWIG_RuntimeError,const_cast<char*>(e.what()));
  }
}

%inline {
namespace anatomydb {
    typedef Eigen::Matrix<double,7,1> Vector7;
}
}
%eigen_typemaps(anatomydb::Vector7);

namespace anatomydb {
    typedef Eigen::Vector3d Vector3;
}
%eigen_typemaps(anatomydb::Vector3);

namespace std {
%template(VecStr) std::vector<std::string>;
%template(VecVecStr) std::vector< std::vector<std::string> >;
//%template(MapStrVec3) std::map< std::string, anatomydb::Vector3 >; // eigen types in container are not wrapped automagically
}

namespace anatomydb {

void init(std::string const& databaseFile="");

unsigned int getEntityId(std::string const& name);
std::string getReferenceName(std::string const& name);
bool isSynonymOf(std::string const& name1, std::string const& name2);
std::string getReferenceNameNoThrow(std::string const& name);
std::string getEntityDescription(std::string const& name);

bool exists(std::string const& name);
bool isBone(std::string const& name);
bool isSkin(std::string const& name);
bool isLandmark(std::string const& name);
bool isEntitySubClassOf(std::string const& name, std::string const& className);
bool isEntityPartOf(std::string const& entity, std::string const& parent, bool recursive=false);
bool isEntityFromBibliography(std::string const& entity, std::string const& bibliography);

std::vector<std::string> getSubClassOfList(std::string const& className);
std::vector<std::string> getParentClassList(std::string const& name);
std::vector<std::string> getPartOfList(std::string const& name);
std::vector<std::string> getPartOfSubClassList(std::string const& name, std::string const& className, bool recursive=false);
std::vector<std::string> getSubPartOfList(std::string const& name, std::string const& className, bool recursive=false);
std::vector<std::string> getSynonymList(std::string const& name, bool omitReferenceName=false);
std::string getSynonymFromBibliography(std::string const& name, std::string const& fromBibliography);
std::vector<std::string> getEntityBibliographyList(std::string const& name);
std::vector<std::string> getLandmarkBoneList(std::string const& name);
std::vector< std::vector<std::string> > getInsertOnList(std::string const& name, std::string const& className);
std::vector<std::string> getSubClassOfFromBibliographyList(std::string const& className, std::string const& bibliography);

%nodefaultctor LandmarkCont;
%nodefaultdtor LandmarkCont;
class LandmarkCont { // : public std::map<std::string, anatomydb::Vector3> // eigen types in container are not wrapped automagically
public:
    void add(std::string const& name, anatomydb::Vector3 const& coord);
    anatomydb::Vector3 const& get(std::string const& name) const;
    bool contains(std::string const& name) const;
    void clear();

};
%extend LandmarkCont {

    std::vector<std::string> keys() const {
        std::vector<std::string> keys;
        for (auto const& name_vec: *self)
            keys.push_back(name_vec.first);
        return keys;
    }


//    std::map< std::string, std::array<double,3> > toMap()
//    {
//        std::map< std::string, std::array<double,3> > map;
//        for (auto const& name_vec: *self) {
//            std::array<double,3> array;
//            array[0]=name_vec.second[0];array[1]=name_vec.second[1];array[2]=name_vec.second[2]; // more elegant ?
//            map.insert(std::make_pair(name_vec.first, array));
//        }

//        return map;
//    }
}

%nodefaultctor Frame;
class Frame {};

%extend Frame {
    anatomydb::Vector7 toVec() const // TODO change for a better name: toSofaRigid() ? toTransQuat() ?
    {
        anatomydb::Vector7 vec;
        vec.head<3>() = self->translation();
        vec.tail<4>() = anatomydb::Quaternion(self->rotation()).coeffs();
        return vec;
    }
}

%nodefaultctor FrameFactory;
%nodefaultdtor FrameFactory;
class FrameFactory {
public:
    static FrameFactory &instance();

    LandmarkCont const& landmark() const;
    LandmarkCont & landmark();

    bool isFrameRegistered(std::string const& name);
    std::vector<std::string> registeredFrameList() const;

    std::vector< std::vector<std::string> > getFrameRequiredLandmarks(std::string const& name);
};
%extend FrameFactory {
    anatomydb::Frame computeFrame(std::string const& name)
    {
        anatomydb::Frame frame;
        self->computeFrame(name, frame);
        return frame;
    }
}

} // anatomydb


%inline %{
std::vector<std::string> getAnatomicalEntityList() {
    std::vector<std::string> entityList;
    anatomydb::getAnatomicalEntityList(entityList);
    return entityList;
}
std::vector<std::string> getLandmarkList() {
    std::vector<std::string> landmarkList;
    anatomydb::getLandmarkList(landmarkList);
    return landmarkList;
}
std::vector<std::string> getJointList() {
    std::vector<std::string> jointList;
    anatomydb::getJointList(jointList);
    return jointList;
}
%}

