// Copyright (C) 2017 INRIA
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 2.1 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details. You should have received a copy of the GNU Lesser
// General Public License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Thomas Lemaire (INRIA)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.2

/**
  SearchTree component
  * Property to set :
            - treeModel, uses the model as the reference for the search. It is an AnatomicTreeModel.
            - treeView, the TreeView that goes along with the model above.
  * Functions that can be used :
            - matchIndex(index) : Function to use to check if an item of the model list
                                matches the value of the text field.
            - showResults(show) : Set wether you wants to show or not the results (show is a boolean).
            - toCurrent() : returns currentSearchIndex if correct, 0 otherwise, and scrolls the treeView to
                                currentSearchIndex
            - next and previous : actions that set the current item to the next (resp. previous) item
                                        and scroll treeView to this item
            - getCurrent() : returns the index of the current result (or 0 if not valid). Use getCurrent().name to have its value.
  * Signals :
            - gotoCurrent : this signal is emitted when the arrows are pressed or
                            the search bar has finish editing.
  * Other properties :
            - the colors can be set as the component is used.
            - indexList and currentSearchIndex can be access outside of this component but shouldn't be.
  */

// TODO: Remove Rectangle to have the native background color and natural item size

Item {
    id: searchTree
    objectName: "searchTree"
    height: searchField.height

    property var treeModel
    property var treeView
    property var indexList: []
    property int currentSearchIndex: 0

    property bool displayNavigationArrows: true
    property bool useButtonForSearching: false

    property string searchText : ""

    signal gotoCurrent()

    onTreeModelChanged:{
        indexList = []
        indexList = getMatchesModelTree(search.text,synSearch.checked)
    }

    onIndexListChanged: {
        modelSearch.insertList(indexList)
    }

    //
    onDisplayNavigationArrowsChanged: {
        navigation.visible = displayNavigationArrows
    }
    onUseButtonForSearchingChanged: {
        searchOnClick.visible = useButtonForSearching
    }


    Component {
        id: hightlightMatchedItemDelegate
        Label {
            font.bold: searchTree.matchIndex(styleData.index)
            text: styleData.value
        }
    }

//    Component.onCompleted: {
//        treeView.itemDelegate = hightlightMatchedItemDelegate.createObject(treeView);
//    }

    //Function to use to check if an item of the model list matches the value of the text field
    function matchIndex(index){
        for(var i = 0; i < this.indexList.length; i++){
            if(index === indexList[i].index) return true
        }
        return false
    }

    function showResults(show){
        results.visible = show
    }

    function getCurrent(){
        if((indexList.length > currentSearchIndex)&&(0 <= currentSearchIndex))
            return indexList[currentSearchIndex]
        else return 0
    }

    function toCurrent(){        //scrollTo of view
        navigation.forceActiveFocus()
        if((indexList.length > currentSearchIndex)&&(0 <= currentSearchIndex)){
            gotoCurrent()
            var item = indexList[currentSearchIndex]
            scrollTo(item.index)
        }else{
            //console.log("IndexList access out of bounds : ", currentSearchIndex, " for list of size ", indexList.length)
            return 0
        }
    }

    Action {
        id: next
        text: ">"
        tooltip: "Goto next matched item"

        onTriggered: {
            currentSearchIndex ++
            if(currentSearchIndex === indexList.length)
                currentSearchIndex = 0
            toCurrent()
        }
    }
    Action {
        id: previous
        text: "<"
        tooltip: "Goto previous matched item"
        onTriggered: {
            currentSearchIndex --
            if(currentSearchIndex === -1)
                currentSearchIndex = indexList.length -1
            toCurrent()
        }
    }

    function setCurrentSearchIndex(){
        currentSearchIndex = listSuggest.currentIndex
    }

    Keys.onPressed: {
        if(results.focus){

            if(event.key === Qt.Key_Down){
//                console.log("current index of list : ", listSuggest.currentIndex, " current index of search : ", currentSearchIndex)
                if(listSuggest.currentIndex !== listSuggest.model.rowCount()-1){
                    listSuggest.incrementCurrentIndex()
                    setCurrentSearchIndex()
                }else{
                    search.forceActiveFocus()
                    currentSearchIndex = 0
                }
            }

            if(event.key === Qt.Key_Up){
//                console.log("current index of list : ", listSuggest.currentIndex)
                if(listSuggest.currentIndex !== 0){
                    listSuggest.decrementCurrentIndex()
                    setCurrentSearchIndex()
                }else{
                    search.forceActiveFocus()
                    currentSearchIndex = 0
                }
//                console.log("new current index of list : ", listSuggest.currentIndex)
            }

            if((event.key === Qt.Key_Left)||(event.key === Qt.Key_Right)){
                search.forceActiveFocus()
                currentSearchIndex = 0
            }


            if(event.key === Qt.Key_Return){
//                    console.log("Return key pressed on results! ")
                search.forceActiveFocus()
                search.text = listSuggest.model.get(listSuggest.currentIndex).name
            }

        }else{
            if(search.focus){
                if(event.key === Qt.Key_Down){
                    results.forceActiveFocus()
                    listSuggest.currentIndex = 0;
//                        console.log("Down key pressed on searchField! current index is", listSuggest.currentIndex)
                }

                if(event.key === Qt.Key_Up){
                    results.forceActiveFocus()
                    listSuggest.positionViewAtEnd()
                    listSuggest.currentIndex = listSuggest.model.rowCount() -1
//                        console.log("Up key pressed on searchField! current index is", listSuggest.currentIndex)
                }

                if(event.key === Qt.Key_Return){
//                    console.log("Return key pressed on searchField! current index is", listSuggest.currentIndex)
                    navigation.forceActiveFocus()
                }
            }else{
                if(navigation.focus){
                    if(event.key === Qt.Key_Left){
                        //Trigger action previous
                        previous.trigger()
                    }

                    if(event.key === Qt.Key_Right){
                        //Trigger action next
                        next.trigger()
                    }

                    if(event.key === Qt.Key_S){
                        resetCurrentSearchIndex()
                    }
                }
            }
        }
    }

    RowLayout {
        id: searchField
        width: parent.width

        TextField {
            id: search
            Layout.fillWidth: true

            placeholderText: qsTr("Search field")

            onTextChanged: {
                searchTree.searchText = this.text
                if(searchTree.useButtonForSearching==false){
                    listSuggest.positionViewAtBeginning()
                    if(search.focus) indexList = getMatchesModelTree(this.text,synSearch.checked)
                }
//                console.log(indexList.length, " indexes for ", text)
            }

            onFocusChanged: {
                listSuggest.positionViewAtBeginning()
                currentSearchIndex = 0
            }
            onEditingFinished: {
                navigation.forceActiveFocus()
                toCurrent()
            }
            onVisibleChanged:
			{
				if(!visible)
					search.text = ""
			}
        }

        RowLayout {
            id: navigation

            Button {
                id: previousArrow
                implicitWidth: 20
                action: previous
                onClicked: navigation.forceActiveFocus()
            }

            Button {
                id: nextArrow
                implicitWidth: 20
                action: next
                onClicked: navigation.forceActiveFocus()
            }


        }

        Button{
            id:searchOnClick
            text: " Search "
            visible: false
            onClicked: {
                listSuggest.positionViewAtBeginning()
                if(search.focus) indexList = getMatchesModelTree(searchTree.searchText,synSearch.checked)
            }
        }

        CheckBox {
            id: synSearch
            text: qsTr("Synonym")
            checked: true
            visible: true
            onCheckedChanged: {
                indexList = getMatchesModelTree(search.text,synSearch.checked)
            }
        }
    }

    Rectangle {
        id: results

        anchors.top : searchField.bottom
        anchors.left: searchField.left
        width: search.width
        z:{
            treeView.z = -1
            return 1
        }

        visible: (this.focus || search.focus || searchField.focus) && (search.text !== "")

        function setHeight(){
            var nbRow = 0;
            var nbMax = 5;
            if(search.text !== ""){
                nbRow = listSuggest.model.rowCount();
    //                    console.log("nb rows :", nbRow)
                if(nbRow>nbMax) nbRow = nbMax;

                nbRow = nbRow*itemHeight;
            }
            this.height = nbRow
        }

        property int itemHeight : 20

        border.width: 1
        color: "white"

        Component {
            id: suggestDelegate
            Rectangle {
                id: rectDelegate
                height: 20
                width: listSuggest.width
                focus: false
                color: ((index === listSuggest.currentIndex)&&this.focus) ? "steelblue" : "transparent"

                Row{
                    spacing: 10
                    Text{
                        text: name
                        verticalAlignment: Text.AlignVCenter
//                        font.pointSize: 10
                       }
                    Text{
                        text: synonym
                        verticalAlignment: Text.AlignVCenter
//                        font.pointSize: 10
                        visible: synSearch.checked
                        font.italic: true
                       }
                    }
                MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            results.focus = true
                            if(listSuggest) listSuggest.currentIndex = index
                        }
                    }
            }
        }

        ListModel {
             id: modelSearch
            objectName: "modelSearch"

            function getListValue(index){
                return this.get(index) ? this.get(index).name : "";
            }

            function insertList(list){
                this.clear()
                for( var i=0; i < list.length ; ++i ) {
                    this.append({"name" : list[i].name,"synonym" : list[i].synonym, "refIndex" : list[i].index})
                            }
                results.setHeight()
            }
        }

        ListView {
            id: listSuggest

            anchors.fill: parent
            anchors.margins: 1

            currentIndex: -1

            model: modelSearch

            delegate: suggestDelegate

            clip : true

            onModelChanged: {
                parent.setHeight()
            }
        }
    }

    Component.onCompleted: {
        search.forceActiveFocus();
    }

    function getMatchesModelTree(word, doSyn){
        if(word === "") return []
        var indexes = treeModel.model.getAllIndexes()
        var returnList = []
        for(var i = 0; i < indexes.length; i ++){
            var index = indexes[i]
            var data = getModelValue(index)

            var match = data.toLowerCase().indexOf(word.toLowerCase())
            var matchSyn = match
            var syn =""
            if(doSyn){
//                    console.log("syn search activated")
                syn = treeModel.getDataRole("Synonyms",data)
                matchSyn = syn.toLowerCase().indexOf(word.toLowerCase())
//                    if(matchSyn >= 0) console.log("syn search, matchsyn is positive")
            }
            if((match>=0)||(matchSyn >= 0)) {
//                    console.log("index ", index, "; data : ", data, "; synonym : ",syn)
                var values = {"index" : index, "name" : data, "synonym" : syn}
                returnList.push(values);
            }
        }
        return returnList;
    }

    function getModelValue(index){
        return treeModel.model.data(index,0) ? treeModel.model.data(index,0) : ""
    }

    function scrollTo(index){
            expandAncestors(index)

            var child = index
            var parent = treeView.model.parent(child)
            var i = 0

            while((parent) && (getModelValue(parent) !== "")){
                child = parent
                parent = treeView.model.parent(child)
                i ++
            }
            var row = treeView.model.rowInParent(child)
            treeView.__listView.positionViewAtIndex(row, ListView.Beginning)
    }

    function expandAncestors(index) {
        var parent = treeView.model.parent(index)
//                    console.log("item ", anatomicTree.getListValue(index), " has parent ", anatomicTree.getListValue(parent) )
        if((parent) && (getModelValue(parent) !== "")){ //if index has a parent
            expandAncestors(parent) //expansion of the parents
            treeView.expand(parent) //expansion of the direct parent
        }
    }


}
