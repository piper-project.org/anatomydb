// Copyright (C) 2017 INRIA
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 2.1 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details. You should have received a copy of the GNU Lesser
// General Public License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Thomas Lemaire (INRIA)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.2

/** SearchComponent
  *  refModel : a model of type QAbstractItemModel or ListModel
  *     -> must contain a getMatches(text) function, that returns the list of indexes of the ros that matches the text
  *  indexList : the list of indexes that are filled with the function mentionned above
  *  matchIndex(index) : the index has the same type as the refModel indexes.
  *     This function returns true if the index is in indexList (ie : the index matches the text)
  */


TextField{
    id: searchComponent
    objectName: "searchComponent"

    //list model to refer to for the search
    property var refModel // must have a function getMatches(text) that returns a list of indexList form
    //text to display at the beggining, default value "Search..."
    property string defaultText : "Search ..."

    property var indexList : [] //indexList[i] has attributes index, data and synonym
    property int currentIndex : 0
//    property var textTemp : ""

    cursorPosition: 4

    placeholderText: defaultText

    style: TextFieldStyle{
        textColor: "black"
        background: Rectangle {
            color: "white"
            radius: 2
        }
    }
    onRefModelChanged: {
        indexList = []
        indexList = refModel.getMatches(this.text)
    }
    onTextChanged: if(focus) indexList = refModel.getMatches(this.text)
    onFocusChanged:{
        currentIndex = 0
        if(focus) indexList = refModel.getMatches(this.text)
//        if(focus && (textTemp !== "")) text = textTemp //if textTemp used
    }

    //Function to use to check if an item of the model list matches the value of the text field
    function matchIndex(index){
        for(var i = 0; i < this.indexList.length; i++){
//            console.log(i, " is not index no ", index, ", list = ", indexList.length, ", refIndex = ", indexList[index].index)
            if(index === indexList[i].index) return true
        }
        return false
    }

    function current(){
//        console.log(" current is ", currentIndex)
        if((indexList.length > currentIndex)&&(0 <= currentIndex)){
//            if(textTemp === "")           //if textTemp used
//                textTemp = this.text

//            console.log(" current is OK, indexList is  ", indexList.length, indexList[currentIndex], indexList[currentIndex].index)
            return indexList[currentIndex]
        }
        console.log("IndexList access out of bounds !")
        return 0
    }

    function next(){
        currentIndex ++
        if(currentIndex === indexList.length)
            currentIndex = 0
        return current()
    }
    function previous(){
        currentIndex --
        if(currentIndex === -1)
            currentIndex = indexList.length -1
        return current()
    }
}
