# Copyright (C) 2017 INRIA
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 2.1 of the License, or (at your
# option) any later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
# for more details. You should have received a copy of the GNU Lesser General
# Public License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Thomas Lemaire (INRIA)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 
# export PYTHONPATH="/local2/build/piper-release/lib/python2.7/site-packages" # your mileage may vary here

import piper.anatomyDB
import numpy as np

piper.anatomyDB.init("/local2/build/piper-release/bin/anatomyDB.sqlite") # your mileage may vary here

#print piper.anatomyDB.getSubClassOfList("Landmark")

ff = piper.anatomyDB.FrameFactory.instance()

## add landmarks
ff.landmark().clear()
ff.landmark().add("Right_acetabular_center", np.array([0.1, -0.3, 0]))
ff.landmark().add("Right_anterior_superior_iliac_spine",[0.2, -0.4, 0.1])
ff.landmark().add("LASIS",[0.2, 0.4, 0.1])
ff.landmark().add("RPSIS",[-0.6, -0.4, 0.1])
ff.landmark().add("LPSIS",[-0.6, 0.4, 0.1])

print "RASIS", ff.landmark().get("RASIS")

frame = ff.computeFrame("Pelvis_right_hip")
frameUnknown = ff.computeFrame("lksjhdfjsdf")
print "Pelvis_right_hip:", frame, " - unknown frame: ", frameUnknown
print "Pelvis_right_hip", frame.toVec().flatten().tolist()

print "Available anatomical frames (db):",piper.anatomyDB.getSubClassOfList("Frame")
print "Available anatomical frames (factory):",ff.registeredFrameList()
for (l,pos) in ff.landmark().toMap().iteritems():
    print l, pos
