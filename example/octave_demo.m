% Copyright (C) 2017 INRIA
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU Lesser General Public License as
% published by
% the Free Software Foundation, either version 2.1 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
% General Public License for more details. You should have received a copy
% of the GNU Lesser General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Thomas Lemaire (INRIA)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
# copy bin/anatomyDB.sqlite and lib/anatomyDB.oct from the build tree
# start with octave octave_demo.m

anatomyDB
anatomyDB.init(".") # anatomyDB install directory
printf "C1 synonyms:\n"
anatomyDB.getSynonymList("C1")
printf "Bony parts of pelvis:\n"
anatomyDB.getSubPartOfList("Pelvic_skeleton","Bone")
printf "Right_anterior_superior_iliac_spine in Kepple1997:\n"
anatomyDB.getSynonymFromBibliography("Right_anterior_superior_iliac_spine", "Kepple1997")
printf "Landmarks defined in Kepple1997:\n"
anatomyDB.getSubClassOfFromBibliographyList("Landmark","Kepple1997")

printf "Bone frames defined in ISB:\n"
anatomyDB.getSubClassOfFromBibliographyList("Frame","ISB_BCS")

printf "Fill landmarks coordinates:\n"
ff = anatomyDB.FrameFactory_instance()
printf "Available frames in factory:\n"
ff.registeredFrameList()
ff.landmark().add("Head_center_of_left_humerus", [1,2,3])
printf "Left_scapula_glenohumeral frame:\n"
ff.computeFrame("Left_scapula_glenohumeral_frame")
printf "Left_femur_hip frame cannot be computed (missing landmarks):\n"
ff.computeFrame("Left_femur_hip_frame")

