/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU Lesser General Public License as published by  *
* the Free Software Foundation, either version 2.1 of the License, or (at      *
* your option) any later version.                                              *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public      *
* License for more details. You should have received a copy of the GNU Lesser  *
* General Public License along with the PIPER Framework.                       *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "FrameFactory.h"

#include <exception>

#include "query.h"

namespace anatomydb {

void LandmarkCont::add(std::string const& name, Vector3 const& coord)
{
    std::string refName = name;
    if (exists(name))
        refName = getReferenceName(name);
    if (find(refName) != end())
        throw std::runtime_error("Landmark already existing: "+name);
    insert(std::make_pair(refName, coord));
}

Vector3 const& LandmarkCont::get(std::string const& name) const
{
    if (m_doInspect) {
        static Vector3 v;
        m_inspectList.push_back(name);
        return v;
    }
    std::string refName = name;
    if (exists(name))
        refName = getReferenceName(name);
    const_iterator it = find(refName);
    if (it == end())
        throw std::runtime_error("Unknown landmark: "+name+" ("+refName+")");
    return it->second;
}

bool LandmarkCont::contains(const std::string &name) const
{
    return find(name)!=end();
}

FrameFactory::FrameFactory()
{}

FrameFactory& FrameFactory::instance()
{
    static FrameFactory f;
    return f;
}

bool FrameFactory::registerFrame(std::string const& name, ComputeFrame computeFrame)
{
    m_computeFrame.insert(std::make_pair(name, computeFrame));
    return true;
}

bool FrameFactory::unregisterFrame(std::string const& name)
{
    return (m_computeFrame.erase(name)>0);
}

bool FrameFactory::isFrameRegistered(std::string const& name)
{
    return m_computeFrame.find(name) != m_computeFrame.end();
}

std::size_t FrameFactory::countFrameRegistered(std::string const& name)
{
    return m_computeFrame.count(name);
}

std::vector<std::string> FrameFactory::registeredFrameList() const
{
    std::vector<std::string> list;
    for (auto const& name_compute: m_computeFrame) {
        if (!list.empty() && name_compute.first == list.back())
            continue;
         list.push_back(name_compute.first);
    }
    return list;
}

bool FrameFactory::canComputeFrame(std::string const& name, std::string & message)
{
    Frame frame;
    try {
        computeFrame(name, frame);
    }
    catch(std::exception const& e) {
        message = e.what();
        return false;
    }
    return true;
}

void FrameFactory::computeFrame(std::string const& name, Frame& frame) const
{
    auto range = m_computeFrame.equal_range(name);
    if (range.first == m_computeFrame.end())
        throw std::runtime_error("Unknown frame: "+name);
    frame.setIdentity();
    std::string errorMsg;
    for (ComputeFrameCont::const_iterator it = range.first; it != range.second; ++it) {
        try {
            it->second(frame, m_landmark);
            return;
        }
        catch(std::runtime_error &e) {
            errorMsg += e.what();
            errorMsg += "\n";
        }
    }
    throw std::runtime_error("Error while computating frame "+name+"\n"+errorMsg);
}

bool FrameFactory::computeFrameNoThrow(std::string const& name, Frame& frame) const
{
    try {
        computeFrame(name, frame);
    }
    catch(...) {
        return false;
    }
    return true;
}

std::vector< std::vector<std::string> > FrameFactory::getFrameRequiredLandmarks(std::string const& name)
{
    auto range = m_computeFrame.equal_range(name);
    if (range.first == m_computeFrame.end())
        throw std::runtime_error("Unknown frame: "+name);

    std::vector< std::vector<std::string> > landmarksList;
    Frame frame;
    m_landmark.m_doInspect = true;// setDoInspect(true);
    for (ComputeFrameCont::const_iterator it = range.first; it != range.second; ++it) {
        m_landmark.m_inspectList.clear();// clearInspectList();
        try {
            it->second(frame, m_landmark);
        }
        catch(std::runtime_error) { }
        landmarksList.push_back(m_landmark.m_inspectList);
    }
    m_landmark.m_doInspect = false;//m_landmark.setDoInspect(false);
    return landmarksList;

}

}
